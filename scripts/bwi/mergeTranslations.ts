// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const path = require("path");
const promises = require("fs/promises");
const lodash = require("lodash");

const [overrideSource] = process.argv.slice(2);

if (!overrideSource) throw new Error("Expected path to override folder as fist argument");

(async () => {
    const translationOverrides = await promises.readdir(overrideSource);

    await Promise.all(
        translationOverrides.map(async (translationName: string) => {
            const targetFile = path.join("src/i18n/strings", translationName);
            const overrideFile = path.join(overrideSource, translationName);
            console.log(`merging ${overrideFile} -> ${targetFile}`);

            const [targetTranslations, overrideTranslation] = await Promise.all([
                readTranslationFile(targetFile),
                readTranslationFile(overrideFile),
            ]);

            const mergedTranslation = lodash.merge(targetTranslations, overrideTranslation);
            await promises.writeFile(targetFile, JSON.stringify(mergedTranslation, null, 2) + "\n");
        }),
    );
})();

async function readTranslationFile(filename: string): Promise<any> {
    const raw = (await promises.readFile(filename)).toString();
    return JSON.parse(raw);
}
