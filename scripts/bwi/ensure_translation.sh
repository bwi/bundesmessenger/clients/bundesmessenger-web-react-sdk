#!/bin/bash

BWI_TRANSLATION_DIR=./src/i18n/bwi_strings
VECTOR_TRANSLATION_DIR=./src/i18n/strings

FILES=$(find $BWI_TRANSLATION_DIR -maxdepth 1 -name '*.json' -type f | xargs -L1 -I{} basename "{}")

orphan_translations=()
for FILE in $FILES
do
    mapfile -t KEYS < <(jq -r 'keys[]' $BWI_TRANSLATION_DIR/$FILE)

    for KEY in "${KEYS[@]}"
    do
        # clean plural from existing key
        CLEAN_KEY=$(sed -E 's/\|(\w*)$//' <<< $KEY)
        if [ "$(jq --arg currKey "$CLEAN_KEY" 'has($currKey) or has($currKey+"|zero") or has($currKey+"|one") or has($currKey+"|other")' $VECTOR_TRANSLATION_DIR/en_EN.json)" == "false" ]; then
            orphan_translations+=("$FILE -> '$KEY'")
        fi
    done
done

if (( ${#orphan_translations[@]} )); then
    echo "Found orphaned translations:"

    for orphan in "${orphan_translations[@]}"
    do
        echo $orphan
    done

    exit 1
else
    echo "Success: No orphaned translations found"
fi
