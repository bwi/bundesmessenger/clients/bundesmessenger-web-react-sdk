#!/bin/bash

set -x 

# yarn config set cache-folder "$(pwd)/.yarn/cache"
yarn config set yarn-offline-mirror "$(pwd)/.yarn/offline"
# Explicitly disable this setting, because different branches can have different dependencies.
# We have to manually clear the cache in gitlab under 'Project/Piplines' and top right 'Clear Runner Caches'
yarn config set yarn-offline-mirror-pruning false
