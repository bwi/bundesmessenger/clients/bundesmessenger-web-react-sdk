#!/bin/bash

deforg="$1"
defrepo="$2"
outputDir="$3"
defbranch="$4"

# use develop as default branch if explicitly set
[ -z "$defbranch" ] && defbranch="develop"

if [ -n "$CI_COMMIT_TAG" ]; then
    defbranch="master"

    cleanTag=`$(dirname $0)/normalize-version.sh ${CI_COMMIT_TAG}`
    if [[ $cleanTag =~ -rc.[0-9]{1,3}$ ]]; then
        releasebranch=$(cut -f1 -d"-" <<< $cleanTag)
        defbranch="release/$releasebranch"
    fi
fi

rm -r "$defrepo" || true

# Try cloning a repository, will exit after success
clone() {
    org=$1
    repo=$2
    branch=$3
    out=$outputDir

    [ -z "$out" ] && out="$repo"

    if [ -n "$branch" ]
    then
        echo "Trying to clone $org/$repo#$branch into $out"
        git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@dl-gitlab.example.com/$org/$repo.git $out --branch "$branch" --depth 1 && exit 0
    fi
}

clone $deforg $defrepo $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
clone $deforg $defrepo $CI_COMMIT_BRANCH
clone $deforg $defrepo $defbranch
