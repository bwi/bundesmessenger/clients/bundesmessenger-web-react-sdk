#!/bin/bash

set -x

mv ./matrix-react-sdk/coverage coverage
COVERAGE=./coverage

# extract version number from package.json (ignore RCs)
APP_VERSION=`sed -n 's/.*"version":.*"\([0-9]*\.[0-9]*\.[0-9]*\)[-rc0-9\.]*".*/\1/p' package.json`

DEV_VERSION=$2


if [ "$1" = "all" ]; then
    sonar-scanner \
        -Dsonar.projectKey=$CI_PROJECT_NAME:latest \
        -Dsonar.sources=src,res \
        -Dsonar.host.url=$SONAR_URL \
        -Dsonar.token=$DSONAR_LOGIN \
        -Dsonar.projectVersion=$APP_VERSION$DEV_VERSION \
        -Dsonar.python.version=3.10 \
        -Dsonar.qualitygate.wait=true \
        -Dsonar.javascript.lcov.reportPaths="$COVERAGE/lcov.info" \
        -Dsonar.testExecutionReportPaths="$COVERAGE/jest-sonar-report.xml" \
        -Dsonar.sonar.coverage.exclusions="test/**/*,cypress/**/*,test_bwi/**/*" \
        -Dsonar.typescript.tsconfigPath=./tsconfig.json || exit 0
        # The global report is allowed to fail.
        # Return 0 to indicate a success in the pipeline even if the scan fails.
        # The allow_failure setting on the job will do the same but marks the job result with a warning
elif [ "$1" = "bwi" ]; then
    sonar-scanner \
        -Dsonar.projectKey=$CI_PROJECT_NAME-BWI-Only:latest \
        -Dsonar.sources=src/bwi \
        -Dsonar.host.url=$SONAR_URL \
        -Dsonar.token=$DSONAR_LOGIN \
        -Dsonar.projectVersion=$APP_VERSION$DEV_VERSION \
        -Dsonar.python.version=3.10 \
        -Dsonar.qualitygate.wait=true \
        -Dsonar.javascript.lcov.reportPaths="$COVERAGE/lcov.info" \
        -Dsonar.testExecutionReportPaths="$COVERAGE/jest-sonar-report.xml" \
        -Dsonar.sonar.coverage.exclusions="test/**/*,cypress/**/*,test_bwi/**/*" \
        -Dsonar.typescript.tsconfigPath=./tsconfig.json && exit 0
else
    echo "Unknown sonar target $1. Allowed values are (all, bwi)"
fi;
