#!/bin/bash

set -x

mkdir matrix-react-sdk
# Filter out:
#   - scripts (Just for convenience so we can execute scripts from the project root)
#   - .yarn (Do not move the mounted .yarn cache)
#   - matrix-react-sdk (Created folder and destination for repo files)
#   - element-web (Can be prepopulated by the cache element-web/node_modules)
#   - matrix-js-sdk (Same as element-web)

ls -A | grep -v -e scripts -e .yarn -e matrix-react-sdk -e element-web -e matrix-js-sdk | xargs mv -t matrix-react-sdk

"$(dirname $0)/setup_yarn.sh"

YARN_LINK=../.yarn/link

echo "installing..."

# js-sdk
"$(dirname $0)/fetchdep.sh" $CI_PROJECT_NAMESPACE $JS_REPO matrix-js-sdk
pushd matrix-js-sdk
yarn install --network-timeout=100000 --pure-lockfile
popd

# matrix-react-sdk (local repo)
pushd matrix-react-sdk
yarn install --network-timeout=100000 --pure-lockfile
popd

# element-web
"$(dirname $0)/fetchdep.sh" $CI_PROJECT_NAMESPACE $WEB_REPO element-web
pushd element-web
yarn install --network-timeout=100000 --pure-lockfile
popd

"$(dirname $0)/fetchdep.sh" $CI_PROJECT_NAMESPACE $WEB_CONFIG_REPO config-and-assets
