/*
Copyright 2021 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { MatrixEvent } from "matrix-js-sdk/src/matrix";
import React from "react";
import classNames from "classnames";

import { Icon as DownloadIcon } from "../../../../res/img/download.svg";
import { MediaEventHelper } from "../../../utils/MediaEventHelper";
import { RovingAccessibleButton } from "../../../accessibility/RovingTabIndex";
import Spinner from "../elements/Spinner";
import { _t, _td, TranslationKey } from "../../../languageHandler";
import { FileDownloader } from "../../../utils/FileDownloader";

// BWI
import ContentScanner, { ContentScanError } from "../../../bwi/content/ContentScanner";
import { getScanErrorMessage } from "../../../bwi/content/ContentError";

interface IProps {
    mxEvent: MatrixEvent;

    // XXX: It can take a cycle or two for the MessageActionBar to have all the props/setup
    // required to get us a MediaEventHelper, so we use a getter function instead to prod for
    // one.
    mediaEventHelperGet: () => MediaEventHelper | undefined;
}

interface IState {
    loading: boolean;
    blob?: Blob;
    tooltip: TranslationKey;
    isScanning: boolean;
    error?: unknown;
}

export default class DownloadActionButton extends React.PureComponent<IProps, IState> {
    private downloader = new FileDownloader();

    public constructor(props: IProps) {
        super(props);

        this.state = {
            loading: false,
            tooltip: _td("timeline|download_action_downloading"),
            // actual scanning takes place when trying to download,
            isScanning: false,
        };
    }

    public async componentDidMount(): Promise<void> {
        const mediaEventHelper = this.props.mediaEventHelperGet();
        if (ContentScanner.instance.canEnable && mediaEventHelper?.media) {
            this.setState({ isScanning: true });
            const scanResult = await mediaEventHelper.media.scanSource();
            if (scanResult !== "safe") {
                this.setState({ isScanning: false, error: new ContentScanError(scanResult) });
            } else {
                this.setState({ isScanning: false });
            }
        }
    }

    private onDownloadClick = async (): Promise<void> => {
        if (this.state.error) return;

        const mediaEventHelper = this.props.mediaEventHelperGet();
        if (this.state.loading || !mediaEventHelper) return;

        if (mediaEventHelper.media.isEncrypted) {
            this.setState({ tooltip: _td("timeline|download_action_decrypting") });
        }

        this.setState({ loading: true });

        const blob = await mediaEventHelper.sourceBlob.value;
        this.setState({ blob });
        await this.doDownload(blob);
    };

    private async doDownload(blob: Blob): Promise<void> {
        await this.downloader.download({
            blob,
            name: this.props.mediaEventHelperGet()!.fileName,
        });
        this.setState({ loading: false });
    }

    public render(): React.ReactNode {
        let disabled: boolean;
        let tooltip: string;
        let spinner: JSX.Element | undefined;

        if (this.state.loading || this.state.isScanning) {
            spinner = <Spinner w={18} h={18} />;
        }

        if (this.state.error instanceof ContentScanError) {
            disabled = true;
            tooltip = getScanErrorMessage(this.state.error.reason);
        } else if (this.state.isScanning) {
            tooltip = _t("Scanning...");
        } else if (this.state.loading) {
            tooltip = _t("Decrypting");
        } else {
            tooltip = _t("action|download");
        }

        disabled ??= !!spinner;

        const classes = classNames({
            mx_MessageActionBar_iconButton: true,
            mx_MessageActionBar_downloadButton: true,
            mx_MessageActionBar_downloadSpinnerButton: !!spinner,
        });

        return (
            <RovingAccessibleButton
                className={classes}
                title={tooltip}
                onClick={this.onDownloadClick}
                disabled={disabled}
                placement="left"
            >
                <DownloadIcon />
                {spinner}
            </RovingAccessibleButton>
        );
    }
}
