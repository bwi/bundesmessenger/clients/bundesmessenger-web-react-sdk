// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixEvent, EventType as MxEventType } from "matrix-js-sdk/src/matrix";
import { M_POLL_RESPONSE } from "matrix-events-sdk";
import SdkConfig from "../../SdkConfig";

type EvClassifierFn = (ev: MatrixEvent, type: EventType) => boolean;

export const EventType = {
    Edit: "MESSAGE_EDIT",
    PollResponse: M_POLL_RESPONSE.name,
    Redact: MxEventType.RoomRedaction,
    Reaction: MxEventType.Reaction,
} as const;

export type EventType = (typeof EventType)[keyof typeof EventType];

const shouldSuppressEventType = (type: EventType): boolean => !!SdkConfig.get("suppressNotification")?.includes(type);

const isMessageEdit: EvClassifierFn = (ev) => !!ev.getContent()["m.new_content"];
const isMxMessageType: EvClassifierFn = (ev, type) => ev.getType() === type;

const evMap = new Map<EventType, EvClassifierFn>([
    [EventType.Edit, isMessageEdit],
    [EventType.Reaction, isMxMessageType],
    [EventType.Redact, isMxMessageType],
    [EventType.PollResponse, isMxMessageType],
]);

export const shouldSuppressNotification = (ev: MatrixEvent): boolean => {
    return Array.from(evMap.entries()).some(([type, isEventOfType]) => {
        if (shouldSuppressEventType(type) && isEventOfType(ev, type)) {
            return true;
        }

        return false;
    });
};
