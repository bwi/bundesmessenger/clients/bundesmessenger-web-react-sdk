// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { EventType, ISendEventResponse, MatrixEvent, Room, RoomMember } from "matrix-js-sdk/src/matrix";
import { MatrixClientPeg } from "../../../MatrixClientPeg";
import { _t } from "../../../languageHandler";
import SettingsStore from "../../../settings/SettingsStore";
import { BwiFeature } from "../../settings/BwiFeature";
import { getSenderName } from "../../../utils/event/getSenderName";
import { RoomPowerLevelsEventContent } from "matrix-js-sdk/src/types";

export interface BwiFunctionLabelEvent {
    [key: string]: string;
}

export const getUserFunctionLabelEvent = (room: Room): BwiFunctionLabelEvent => {
    const event = room.currentState.getStateEvents(EventType.BwiUserFunctionLabels, "");
    let eventContent = event ? event.getContent() || {} : {};
    eventContent = Object.assign({}, eventContent);
    return eventContent;
};

const sendUserFunctionLabelEvent = async (room: Room, content: BwiFunctionLabelEvent): Promise<ISendEventResponse> => {
    const client = MatrixClientPeg.safeGet();

    /**
     * when we create the first function label, we should also
     * patch the powerlevels automatically
     */
    const powerLevelEvent = room.currentState.getStateEvents("m.room.power_levels", "");
    const plContent: RoomPowerLevelsEventContent = powerLevelEvent?.getContent() || {};
    if (
        plContent.events &&
        (!plContent.events[EventType.BwiUserFunctionLabels] || plContent.events[EventType.BwiUserFunctionLabels] < 100) // sanity check because of state_default
    ) {
        plContent["events"][EventType.BwiUserFunctionLabels] = 100;
        client.sendStateEvent(room.roomId, EventType.RoomPowerLevels, plContent);
    }
    return client.sendStateEvent(room.roomId, EventType.BwiUserFunctionLabels, content);
};

export const setUserFunctionLabel = (
    room: Room,
    member: RoomMember | RoomMember[],
    label: string,
): Promise<ISendEventResponse> => {
    const event = getUserFunctionLabelEvent(room);

    if (Array.isArray(member)) {
        member.forEach((m) => {
            event[m.userId] = label;
        });
    } else {
        event[member.userId] = label;
    }

    return sendUserFunctionLabelEvent(room, event);
};

export const unsetUserFunctionLabel = (room: Room, member: RoomMember | RoomMember[]): Promise<ISendEventResponse> => {
    const event = getUserFunctionLabelEvent(room);

    if (Array.isArray(member)) {
        member.forEach((m) => {
            delete event[m.userId];
        });
    } else {
        delete event[member.userId];
    }

    return sendUserFunctionLabelEvent(room, event);
};

export const canChangeFunctionLabel = (room: Room): boolean => {
    // only an admin should be able to change the function labels if not defined in powerlevels
    const powerLevelEvent = room.currentState.getStateEvents("m.room.power_levels", "");
    const powerLevels = powerLevelEvent?.getContent() || {};
    if (powerLevels.events && powerLevels.events[EventType.BwiUserFunctionLabels]) {
        return room.currentState.maySendStateEvent(EventType.BwiUserFunctionLabels, room.myUserId);
    } else {
        const me = room.getMember(room.myUserId);
        if ((me?.powerLevel || 0) >= 100) {
            return true;
        }
    }

    return false;
};

export function textForUserFunctionLabelEvent(event: MatrixEvent): (() => string) | null {
    if (!SettingsStore.getValue(BwiFeature.UserFunctionLabels)) return null;

    const current = event.getContent();
    const previous = event.getPrevContent();

    const client = MatrixClientPeg.safeGet();
    const room = client.getRoom(event.getRoomId());

    const senderName = getSenderName(event);

    const addedUsers: string[] = [];
    Object.keys(current).forEach((user) => {
        if (previous[user] !== current[user]) {
            addedUsers.push(user);
        }
    });

    /**
     * we only support either removing a user per event
     * or multiple users per new label so this is probably fine
     */
    if (addedUsers.length) {
        return () =>
            _t("%(senderName)s appointed %(user)s to function: %(functionLabel)s", {
                senderName,
                user: addedUsers.map((uid) => room?.getMember(uid)?.rawDisplayName || uid).join(", "),
                functionLabel: current[addedUsers[0]],
            });
    }

    const removedUsers: string[] = [];
    Object.keys(previous).forEach((user) => {
        if (current[user] !== previous[user]) {
            removedUsers.push(user);
        }
    });

    /**
     * we only support either removing a user per event
     * or multiple users per new label so this is probably fine
     */
    if (removedUsers.length) {
        return () =>
            _t("%(senderName)s has removed the function %(functionLabel)s for %(user)s", {
                senderName,
                user: removedUsers.map((uid) => room?.getMember(uid)?.rawDisplayName || uid).join(", "),
                functionLabel: previous[removedUsers[0]],
            });
    }

    return null;
}
