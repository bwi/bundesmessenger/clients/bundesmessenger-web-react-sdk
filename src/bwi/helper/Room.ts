// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import SettingsStore from "../../settings/SettingsStore";
import { BwiFeature } from "../settings/BwiFeature";
import { Room, HistoryVisibility } from "matrix-js-sdk/src/matrix";
import { DEFAULT_EVENT_POWER_LEVELS, IOpts as RoomCreateOpts } from "../../createRoom";
import { ElementCall } from "../../models/Call";

export const canLeaveRoom = (room: Room): boolean => {
    const members = room.getMembers().filter((m) => m.membership === "join" || m.membership === "invite");
    const allAdmins = members.filter((m) => m.powerLevel === 100);

    const isAdmin = !!allAdmins.filter((a) => a.userId === room.myUserId).length;

    if (isAdmin && allAdmins.length === 1 && members.length > 1) {
        return false;
    }

    return true;
};

export const adjustRoomCreateOpts = (opts: RoomCreateOpts): RoomCreateOpts => {
    if (SettingsStore.getValue(BwiFeature.DisableHistorySharing)) {
        /*
         * MESSENGER-3147
         * we want to enforce HistoryVisibility.Invited on any room
         */
        opts.historyVisibility = HistoryVisibility.Invited;
    }

    if (SettingsStore.getValue(BwiFeature.RoomAutoPublish)) {
        /*
         * MESSENGER-3290
         * derive room alias from roomname
         */
        if (opts.createOpts?.name) {
            const alias = opts.createOpts.name.replace(/[^a-z0-9]/gi, "");
            if (alias) {
                opts.createOpts.room_alias_name = `${alias}${Date.now()}`;
            }
        }
    }

    /*
     * MESSENGER-4924
     * we want that everybody is able to set up a call not just admins
     */
    const huddleForAll = SettingsStore.getValue(BwiFeature.HuddleCreationForAll);
    if (typeof opts.createOpts === "undefined") opts.createOpts = {};
    if (huddleForAll) {
        opts.createOpts.power_level_content_override = {
            events: {
                ...DEFAULT_EVENT_POWER_LEVELS,
                [ElementCall.MEMBER_EVENT_TYPE.name]: 0,
                [ElementCall.CALL_EVENT_TYPE.name]: 0,
                ["m.call"]: 0,
                ["m.call.member"]: 0,
            },
        };
    }

    return opts;
};
