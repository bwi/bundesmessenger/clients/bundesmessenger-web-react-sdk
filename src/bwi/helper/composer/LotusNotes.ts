// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const SNIPPET_REGEX = /<NDL><REPLICA(.*)><\/NDL>/;
const pathMatchers = [
    /<REPLICA ([\w]*):([\w]*)>/,
    /<VIEW OF([\w]*):([\w]*)-ON([\w]*):([\w]*)>/,
    /<NOTE OF([\w]*):([\w]*)-ON([\w]*):([\w]*)>/,
];

export const tryAndTransformLonoSnippetToUrl = (rawText: string): string | null => {
    const strippedText = rawText.replace(/\n|\r/g, "");

    if (SNIPPET_REGEX.test(strippedText)) {
        const parts: string[] = [];
        // query for fragments
        pathMatchers.some((matcher) => {
            const matches = strippedText.match(matcher);
            if (matches?.length) {
                // strip full match
                matches.shift();
                parts.push(matches.join(""));
            }
            // while we have matches continue
            return !matches;
        });

        if (parts.length) {
            return `notes:///${parts.join("/")}`;
        }
    }
    return null;
};
