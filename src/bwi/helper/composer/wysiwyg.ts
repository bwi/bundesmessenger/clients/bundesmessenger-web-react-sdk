// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixClientPeg } from "../../../MatrixClientPeg";
import { makeRoomPermalink, makeUserPermalink, parsePermalink } from "../../../utils/permalinks/Permalinks";

export const wysiwyg = {
    // BWI: Temp fix until elements wysiwyg composer can handle local permalinks
    validateMessageContent(content: string) {
        const client = MatrixClientPeg.safeGet();

        let message = content;

        // Replace matrix.to permalinks with local permalinks
        message = message.replace(/href="(https:\/\/matrix\.to.*?)"/, (match, matrixToUrl): string => {
            if (!matrixToUrl) return match;

            const parts = parsePermalink(matrixToUrl);
            let permalink = "";
            if (parts?.roomIdOrAlias) {
                permalink = makeRoomPermalink(client, parts.roomIdOrAlias);
            } else if (parts?.userId) {
                permalink = makeUserPermalink(parts.userId);
            }

            if (!permalink) return match;

            return match.replace(matrixToUrl, permalink);
        });

        return message;
    },
};
