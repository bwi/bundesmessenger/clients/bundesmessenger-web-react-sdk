// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

export type AppClient = "bwi" | "element";
export let APP_CLIENT: AppClient = "bwi";

export function isBwiClient(): boolean {
    return APP_CLIENT === "bwi";
}

export function isElementClient(): boolean {
    return APP_CLIENT === "element";
}

export function setClient(client: AppClient | ({} & string) = APP_CLIENT): void {
    APP_CLIENT = client as AppClient;
}
