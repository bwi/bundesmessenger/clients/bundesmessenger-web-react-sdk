// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { Visibility, Preset, MatrixError, Method, EventType } from "matrix-js-sdk/src/matrix";
import { resolve } from "path";
import { _t } from "../../languageHandler";
import { MatrixClientPeg } from "../../MatrixClientPeg";
import { DefaultTagID } from "../../stores/room-list/models";
import createRoom, { IOpts } from "../../createRoom";
import { NOTES_DATA } from "../functions/PersonalNotes";
import { encodeUri } from "matrix-js-sdk/src/utils";
import dis from "../../dispatcher/dispatcher";
import RoomListActions from "../../actions/RoomListActions";

export type PersonalNotesEvent = {
    room_id?: string;
    reset?: boolean;
};

export const getPersonalRoomConfig = (): IOpts => {
    return {
        createOpts: {
            visibility: Visibility.Private,
            preset: Preset.TrustedPrivateChat,
            name: _t("My notes"),
            topic: _t(
                "This is your private note-taking room. You are the only one who can access the content in this room.",
            ),
            creation_content: {
                "m.federate": false,
            },
        },
        guestAccess: false,
        encryption: true,
        andView: false,
        spinner: false,
    };
};

export class PersonalNotesRoomManager {
    private static _instance?: PersonalNotesRoomManager;
    private isInitialized = false;
    private initializing = false;

    public static get instance(): PersonalNotesRoomManager {
        if (!PersonalNotesRoomManager._instance) {
            PersonalNotesRoomManager._instance = new PersonalNotesRoomManager();
        }
        return PersonalNotesRoomManager._instance;
    }

    public async managePersonalNotes(): Promise<void> {
        if (!this.isInitialized && !this.initializing) {
            this.initializing = true;
            const cli = MatrixClientPeg.safeGet();

            const path = encodeUri(`/user/$userId/account_data/$type`, {
                $userId: cli.credentials.userId,
                $type: NOTES_DATA,
            });
            try {
                await cli.http
                    .authedRequest<PersonalNotesEvent>(Method.Get, path, undefined)
                    .then(async (notesData) => {
                        if (notesData && notesData.hasOwnProperty("reset") && notesData["reset"] === true) {
                            const removeReset = { ...notesData };
                            removeReset["reset"] = undefined;
                            await this.createPersonalNotesRoom(removeReset);
                        }

                        // personal notes room account data is there but no user id
                        if (notesData && notesData.hasOwnProperty("room_id")) {
                            // personal notes room account data has a room id - cleanup of rooms that could be duplicates
                            setTimeout(() => {
                                this.checkExistingRooms(notesData);
                            }, 2000);
                        }

                        this.isInitialized = true;
                        this.initializing = false;

                        resolve();
                    });
            } catch (e) {
                if (e instanceof MatrixError && e.data && e.data.errcode === "M_NOT_FOUND") {
                    // personal notes room account data is not initialized yet
                    // create a store
                    await this.createPersonalNotesRoom({});
                    this.isInitialized = true;
                    this.initializing = false;
                }
            }
        }
    }

    public async createPersonalNotesRoom(notesData: PersonalNotesEvent): Promise<void> {
        const cli = MatrixClientPeg.safeGet();

        await createRoom(cli, getPersonalRoomConfig()).then(async (roomId: string | null) => {
            if (!roomId) return;
            await Promise.all([
                cli.setRoomTag(roomId, DefaultTagID.Favourite, { order: 1 }),
                cli.setAccountData(NOTES_DATA, { ...notesData, room_id: roomId }),
            ]);
        });
    }

    public async checkExistingRooms(notesData: PersonalNotesEvent): Promise<void> {
        const cli = MatrixClientPeg.safeGet();
        const rooms = cli.getRooms();
        for (const room of rooms) {
            // room migration from personal notes tag to favourite tag
            if (notesData.room_id === room.roomId && !room.tags.hasOwnProperty(DefaultTagID.Favourite)) {
                await cli.deleteRoomTag(room.roomId, NOTES_DATA);
                dis.dispatch(RoomListActions.tagRoom(cli, room, null, DefaultTagID.Favourite, 0));
            }

            // duplicate personal notes room
            if (room.tags && room.tags.hasOwnProperty(NOTES_DATA)) {
                // delete tag and treat it as a regular room from now on
                if (notesData.room_id !== room.roomId) {
                    await cli.deleteRoomTag(room.roomId, NOTES_DATA);
                }

                // clean up any room avatar from rooms with personal notes room tag
                if (room.getMxcAvatarUrl()) {
                    await cli.sendStateEvent(room.roomId, EventType.RoomAvatar, {}, "");
                }
            }
        }
    }

    /**
     * For testing purposes to restore initial state
     */
    public static resetManager = (): void => {
        if (PersonalNotesRoomManager._instance) {
            delete PersonalNotesRoomManager._instance;
        }
    };
}
