// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { BwiFeature } from "../settings/BwiFeature";
import SettingsStore from "../../settings/SettingsStore";
import bwi from "..";

const hiddenUserPreferenceSettings: string[] = [
    "ctrlFForSearch",
    "showTwelveHourTimestamps",
    "MessageComposerInput.surroundWith",
    "Pill.shouldShowPillAvatar",
    "showTypingNotifications",
    "MessageComposerInput.showStickersButton",
    "urlPreviewsEnabled",
];

const hiddenVectorPushRules: string[] = [
    ".m.rule.contains_user_name",
    ".m.rule.call",
    ".m.rule.suppress_notices",
    ".m.rule.tombstone",
    "_keywords",
    ".m.rule.message",
    ".m.rule.room_one_to_one",
    ".m.rule.roomnotif",
];

export const isUserSettingDisabled = (setting: string): boolean => {
    // BWI - if our flag is disabled then show all nerdy settings
    if (!SettingsStore.getValue(BwiFeature.HideNerdyUserSettings)) {
        return false;
    }

    // BWI - the setting for personal notes should not be shown when it is not set in config or set to true
    if (setting === BwiFeature.ShowPersonalNotes) {
        return !bwi.isPersonalNotesConfigured();
    }

    return hiddenUserPreferenceSettings.includes(setting);
};

export const isVectorPushRuleEnabled = (rule: string): boolean => {
    if (!SettingsStore.getValue(BwiFeature.HideNerdyUserSettings)) {
        return true;
    }

    return !hiddenVectorPushRules.includes(rule);
};
