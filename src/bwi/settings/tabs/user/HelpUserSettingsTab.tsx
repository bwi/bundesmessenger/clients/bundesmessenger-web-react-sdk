// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { useState, useEffect } from "react";

import { _t } from "../../../../languageHandler";
import SdkConfig from "../../../../SdkConfig";
import PlatformPeg from "../../../../PlatformPeg";
import UpdateCheckButton from "../../../../components/views/settings/UpdateCheckButton";
import BwiPlatform from "../../../BwiPlatform";
import { SettingsSection } from "../../../../components/views/settings/shared/SettingsSection";
import SettingsSubsection, {
    SettingsSubsectionText,
} from "../../../../components/views/settings/shared/SettingsSubsection";
import CopyableText from "../../../../components/views/elements/CopyableText";

export const BwiHelpUserSettingsTab: React.FC = () => {
    const [canUpdate, setCanUpdate] = useState<boolean>(false);
    const [appVersion, setAppVersion] = useState<string>("");
    const [elementVersion, setElementVersion] = useState<string>("");

    const config = SdkConfig.get();

    const telTypeTranslation = new Map([
        ["internal", _t("internal")],
        ["external", _t("external")],
    ]);

    useEffect(() => {
        PlatformPeg.get()
            ?.getAppVersion()
            .then((ver) => setAppVersion(ver))
            .catch((e) => {
                console.error("Error getting vector version: ", e);
            });
        PlatformPeg.get()
            ?.canSelfUpdate()
            .then((v) => setCanUpdate(v))
            .catch((e) => {
                console.error("Error getting self updatability: ", e);
            });
        BwiPlatform.getElementVersion()
            .then((v) => setElementVersion(v))
            .catch((e) => {
                console.error("Error getting element version");
            });
    }, []);

    const getVersionTextToCopy = (): string => {
        return `${appVersion}\n${elementVersion}`;
    };

    let updateButton: JSX.Element | undefined;
    if (canUpdate) {
        updateButton = <UpdateCheckButton />;
    }

    return (
        <SettingsSection data-testid="bwi_legalUserSettingsTab" heading={_t("setting|help_about|title")}>
            <SettingsSubsection heading={_t("setting|help_about|help_title")}>
                {_t(
                    "You are looking for help in using %(brand)s, have a problem or need more information?<br />You have the following options:",
                    {
                        brand: config.brand,
                    },
                    {
                        br: () => <br />,
                    },
                )}

                <ul
                    style={{
                        listStyle: "none",
                        paddingLeft: "20px",
                        margin: "30px 0px",
                    }}
                >
                    {config.helpdesk && (
                        <li className="mx_SettingsTab_helpdesk">
                            <b>{config.helpdesk.name}</b>
                            <br />
                            {_t("Just call the user helpdesk at")} <br />
                            {config.helpdesk.telephone.map((tel) => (
                                <span key={tel.number}>
                                    <br /> {telTypeTranslation.get(tel.type)}:{" "}
                                    <a href={`tel:${tel.number}`} rel="noreferrer noopener">
                                        {tel.number}
                                    </a>
                                </span>
                            ))}
                            <br /> <br />
                            {_t("for further assistance.")}
                            <br />
                            <br />
                        </li>
                    )}
                    {config.faq_url && (
                        <li className="mx_SettingsTab_faq">
                            <b>FAQs</b>
                            <br />
                            {_t(
                                "On <a>%(faqUrl)s</a> you can find several FAQs.<br />Maybe your question has already been answered here...",
                                {
                                    faqUrl: config.faq_url,
                                },
                                {
                                    a: (sub) => (
                                        <a href={config.faq_url} rel="noreferrer noopener" target="_blank">
                                            {sub}
                                        </a>
                                    ),
                                    br: () => <br />,
                                },
                            )}
                            <br />
                            <br />
                        </li>
                    )}
                    {config.wiki_url && (
                        <li className="mx_SettingsTab_wiki">
                            <b>{_t("Wiki")}</b>
                            <br />
                            {_t(
                                "In the <a>Wiki</a> you will find tutorials and further information for %(brand)s",
                                { brand: config.brand },
                                {
                                    a: (sub) => (
                                        <a href={config.wiki_url} rel="noreferrer noopener" target="_blank">
                                            {sub}
                                        </a>
                                    ),
                                },
                            )}
                            <br />
                            <br />
                        </li>
                    )}
                    {config.community_support && (
                        <li data-test-id="community-support-hint">
                            <b>Community Support</b>
                            <br />
                            {_t(
                                "In the public room <b>Community Support<b> users can help each other.<br />Just take a look and ask your question.",
                                {},
                                {
                                    b: (sub) => <b>{sub}</b>,
                                    br: () => <br />,
                                },
                            )}
                            <br />
                            <br />
                        </li>
                    )}
                </ul>
            </SettingsSubsection>

            <SettingsSubsection heading={_t("setting|help_about|versions")}>
                <SettingsSubsectionText>
                    <CopyableText getTextToCopy={getVersionTextToCopy}>
                        {_t("%(brand)s version:", { brand: config.brand })} {appVersion || _t("unknown")}
                        <br />
                        {_t("Element version:")} {elementVersion || _t("unknown")}
                        <br />
                    </CopyableText>
                    {updateButton}
                </SettingsSubsectionText>
            </SettingsSubsection>
        </SettingsSection>
    );
};
