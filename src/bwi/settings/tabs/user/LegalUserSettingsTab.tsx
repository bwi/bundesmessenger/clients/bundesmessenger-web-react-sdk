// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactNode } from "react";
import { _t } from "../../../../languageHandler";
import SdkConfig from "../../../../SdkConfig";
import { MatrixClientPeg } from "../../../../MatrixClientPeg";
import { BwiA11yDeclaration } from "../../../components/A11yDeclaration";
import SettingsSubsection from "../../../../components/views/settings/shared/SettingsSubsection";
import { SettingsSection } from "../../../../components/views/settings/shared/SettingsSection";
import { BwiFeature } from "../../BwiFeature";
import SettingsStore from "../../../../settings/SettingsStore";

interface LinkConfig {
    url: string;
    text: string;
}

const tocTranslation = new Map([
    ["Privacy Policy", _t("Privacy Policy")],
    ["Copyright", _t("Copyright")],
    ["Imprint", _t("Imprint")],
]);

export const LegalUserSettingsTab: React.FC = () => {
    let links: LinkConfig[] = [];

    const wellKnown = MatrixClientPeg.safeGet().getClientWellKnown();
    const wellKnownLinks: { data_privacy_url?: string; imprint_url?: string } | undefined = wellKnown?.["de.bwi"];
    if (wellKnownLinks?.data_privacy_url) {
        links.push({
            text: "Privacy Policy",
            url: wellKnownLinks?.data_privacy_url,
        });
    }

    if (wellKnownLinks?.imprint_url) {
        links.push({
            text: "Imprint",
            url: wellKnownLinks.imprint_url,
        });
    }

    const configLinks = SdkConfig.get().terms_and_conditions_links;
    if (configLinks) {
        links.push(...configLinks);
    }

    links = links.filter((link, idx, allLinks) => allLinks.findIndex((v) => v.text === link.text) === idx);

    const subSection: ReactNode[] = [];

    const dataVNumber = SettingsStore.getValue<string | null>(BwiFeature.DataVNumber);
    if (dataVNumber) {
        subSection.push(<div key="dataVnumber">{_t("bwi|legal|data_v_number", { num: dataVNumber })}</div>);
    }

    return (
        <SettingsSection data-testid="bwi_legalUserSettingsTab" heading={_t("common|legal")}>
            <SettingsSubsection>
                <div className="mx_SettingsTab_subsectionText">
                    {links.map((tocEntry, index) => (
                        <div key={index}>
                            <a href={tocEntry.url} rel="noreferrer noopener" target="_blank">
                                {tocTranslation.get(tocEntry.text) || tocEntry.text}
                            </a>
                        </div>
                    ))}
                    <div>
                        <BwiA11yDeclaration className="bwi_a11yDeclaration_link" />
                    </div>
                </div>
            </SettingsSubsection>
            {subSection.length > 0 && (
                <SettingsSubsection className="mx_SettingsTab_subsectionText">{subSection}</SettingsSubsection>
            )}
        </SettingsSection>
    );
};
