// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { Room, RoomMember, MatrixEvent, RoomStateEvent, EventType } from "matrix-js-sdk/src/matrix";
import { useCallback, useContext, useEffect, useState } from "react";
import { getUserFunctionLabelEvent } from "../../bwi/helper/events/UserFunctionLabel";
import { BwiFeature } from "../../bwi/settings/BwiFeature";
import RoomContext from "../../contexts/RoomContext";
import SettingsStore from "../../settings/SettingsStore";
import { MatrixClientPeg } from "../../MatrixClientPeg";

export const useUserFunctionLabel = (mxEvent: MatrixEvent): string => {
    const [userFunctionLabel, setUserFunctionLabel] = useState<string>("");
    const roomContext = useContext(RoomContext);
    const client = MatrixClientPeg.safeGet();

    const updateUserFunctionLabel = useCallback(() => {
        if (!SettingsStore.getValue(BwiFeature.UserFunctionLabels)) {
            return "";
        }

        if (!roomContext.room) {
            return "";
        }

        setUserFunctionLabel(getUserFunctionLabelEvent(roomContext.room)[mxEvent.getSender() || ""] || "");
    }, [roomContext.room, mxEvent]);

    useEffect(() => {
        updateUserFunctionLabel();

        const onRoomStateEvent = (event: MatrixEvent): void => {
            if (event.getType() === EventType.BwiUserFunctionLabels) {
                updateUserFunctionLabel();
            }
        };

        client.on(RoomStateEvent.Events, onRoomStateEvent);

        return () => {
            client.removeListener(RoomStateEvent.Events, onRoomStateEvent);
        };
    }, [client, updateUserFunctionLabel]);

    return userFunctionLabel;
};

export const compareByUserFunctionLabel = (memberA: RoomMember, memberB: RoomMember, room: Room | null): number => {
    if (room === null) {
        return 0;
    }

    const functionLabelEvent = getUserFunctionLabelEvent(room);

    if (functionLabelEvent[memberA.userId]) return -1;
    if (functionLabelEvent[memberB.userId]) return 1;
    return 0;
};
