// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { EventTimeline, EventType, MatrixClient, MatrixEvent, Room } from "matrix-js-sdk/src/matrix";
import { MatrixClientPeg } from "../../MatrixClientPeg";
import { IOpts as RoomCreateOpts } from "../../createRoom";
import { _t } from "../../languageHandler";
import SettingsStore from "../../settings/SettingsStore";
import DMRoomMap from "../../utils/DMRoomMap";
import { Member } from "../../utils/direct-messages";
import { isRoomDm } from "../components/views/rooms/SingleParticipantDms";
import { BwiFeature } from "../settings/BwiFeature";
import { isPersonalNotesRoom } from "./PersonalNotes";

/**
 * enabled = m.federated !== false & server acl allows other server
 *
 * disabled = m.federated !== false & server acl does not allow other server OR
 *            m.federated === false
 *
 * pending  = m.federated !== false & server acl not set
 */
type RoomFederationState = "enabled" | "disabled" | "pending";

export function bwiIsRoomFederated(room: Room): boolean {
    const state = getRoomFederationState(room);
    return state !== "disabled";
}

export function getFederationStateFromACLEvent(event: MatrixEvent): RoomFederationState {
    const aclEvent = event.getContent() as ServerACLs | undefined; //NOSONAR cast is actually needed here
    const isACLFederated = isFederatedByACLs(aclEvent);

    // allow federation if:
    // -> m.federated != false and no acl event
    // -> m.federated != false and acl allows federation
    if (aclEvent === undefined) return "pending";
    if (isACLFederated) return "enabled";

    // default to false
    return "disabled";
}

function getRoomFederationState(room: Room): RoomFederationState {
    // Code optimized for readability

    // If room create event carries m.federated = false in its content we can not allow federation
    const state = room.getLiveTimeline().getState(EventTimeline.FORWARDS);
    const federationEnabled = state?.getStateEvents(EventType.RoomCreate, "")?.getContent()["m.federated"] !== false;

    // early out if room can never be federated
    if (federationEnabled === false) return "disabled";

    // do not allow/show federation for the personal notes room
    if (isPersonalNotesRoom(room)) return "disabled";

    // if room is a DM, show federation if dm is federated
    const theirId = DMRoomMap.shared().getUserIdForRoomId(room.roomId);
    if (theirId) {
        return bwiIsUserFederated(MatrixClientPeg.get()?.getUserId() || "", theirId || "") ? "enabled" : "disabled";
    }

    const aclEvent = state?.getStateEvents(EventType.RoomServerAcl, "")?.getContent() as ServerACLs | undefined; //NOSONAR cast is actually needed here
    const isACLFederated = isFederatedByACLs(aclEvent);

    // allow federation if:
    // -> m.federated != false and no acl event
    // -> m.federated != false and acl allows federation
    if (aclEvent === undefined) return "pending";
    if (isACLFederated) return "enabled";

    // default to false
    return "disabled";
}

export interface ServerACLs {
    allow?: string[];
    deny?: string[];
    allow_ip_literals?: boolean;
}

/**
 * Checks if a second server (besides the home server) is listed in the allow list
 */
export function isFederatedByACLs(acls?: ServerACLs): boolean {
    const firstAllowPattern = acls?.allow?.[0];
    if (firstAllowPattern) {
        // the first iteration of federation will only use wildcards. So we can safely the for a wildcard and be good.
        return firstAllowPattern.startsWith("*");
    }
    return false;
}

export function bwiGetUserIdServerPart(id: string): string | undefined {
    return id.split(":").filter((s) => !!s)[1];
}
export function formatServerName(serverName: string | undefined): string {
    const [domain, topDomain] = serverName?.split(".").slice(-2) || [];
    if (domain && topDomain) {
        return `${domain}.${topDomain}`;
    } else {
        return serverName || "";
    }
}

export function bwiIsUserFederated(myId: string, theirId: string): boolean {
    const myServer = bwiGetUserIdServerPart(myId);
    const theirServer = bwiGetUserIdServerPart(theirId);

    // Invalid values, gracefully return false.
    if (!myServer || !theirServer) return false;

    return myServer !== theirServer;
}

function getAllowRule(isFederated: boolean): [string] {
    return isFederated ? ["*"] : [bwiGetUserIdServerPart(MatrixClientPeg.safeGet().getUserId()!)!];
}

/**
 * Add initial m.room.server_acl event
 * @param opts
 * @param initialState
 */
export function bwiSetFederationRoomCreateOpts(
    opts: RoomCreateOpts,
    initialState: Required<NonNullable<RoomCreateOpts["createOpts"]>>["initial_state"],
): void {
    // do not set ACL event for DMs
    if (!opts.dmUserId) {
        initialState.push({
            type: EventType.RoomServerAcl,
            content: {
                allow: getAllowRule(opts.bwiEnableFederation ?? false),
                allow_ip_literals: false,
            },
        });
    }
}

export async function updateRoomAcl(room: Room, isFederated: boolean): Promise<void> {
    await room.client.sendStateEvent(room.roomId, EventType.RoomServerAcl, {
        allow: getAllowRule(isFederated),
        allow_ip_literals: false,
    });
}

export function shouldShowFederationDecisionDialog(room: Room, justCreatedOpts?: RoomCreateOpts): boolean {
    const admins = room.getMembers().filter((m) => m.powerLevel >= 100);
    const userIsAdmin = !!admins.filter((a) => a.userId === room.myUserId).length;
    const state = room.getLiveTimeline().getState(EventTimeline.FORWARDS);

    if (!SettingsStore.getValue(BwiFeature.Federation)) return false;
    if (!userIsAdmin) return false;
    if (isPersonalNotesRoom(room)) return false;
    if (isRoomDm(room.roomId)) return false;

    const federationEnabled = state?.getStateEvents(EventType.RoomCreate, "")?.getContent()["m.federated"] !== false;
    if (federationEnabled === false) return false;

    // odd behaviour in production: after the room is created and viewRoom in MatrixChat is called
    // you might just receive an incomplete room state where no acl event is yet synced
    // in that case check if the room was just created and see if a federation decision was taken
    const isBwiFederationChosenOnCreation = justCreatedOpts?.bwiEnableFederation;
    if (typeof isBwiFederationChosenOnCreation !== "undefined") return false;

    const aclEvent = state?.getStateEvents(EventType.RoomServerAcl, "")?.getContent() as ServerACLs | undefined; //NOSONAR cast is actually needed here
    return aclEvent === undefined;
}

export function roomNameToAlias(name: string, client: MatrixClient): string {
    const cleanedName = name.replace(/[^a-z0-9]/gi, "");
    const tsAlias = `${cleanedName}${Date.now()}`;
    return `#${tsAlias}:${client.getDomain()}`;
}

/**
 *
 * @param members Members to invite
 * @param room Room
 * @returns An error message or null if the the validation succeeded
 */
export function validateFederationInvites(members: Member[], room: Room, powerLevel?: number): string | null {
    const allLocal = !members.some((member) =>
        bwiIsUserFederated(MatrixClientPeg.safeGet().getUserId() || "", member.userId),
    );

    // do nothing if every user is local
    if (allLocal) return null;

    const state = getRoomFederationState(room);
    // return pending if admin has not yet decided about the federation state
    if (state === "pending") return _t("bwi|invite_dialog|federation_pending");

    // do not allow invites
    if (state === "disabled") return _t("bwi|invite_dialog|federation_disabled");

    // check if user is allowed
    // TODO: Check if the users home server is allowed by the room acls.

    // do not allow admins
    if (powerLevel === 100) {
        return _t("bwi|federation|admin_power_level_forbidden");
    }

    return null;
}
