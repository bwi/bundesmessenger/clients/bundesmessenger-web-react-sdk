// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import BwiAnalyticsManager from "../../bwi/analytics/BwiAnalyticsManager";
import SettingsStore from "../../settings/SettingsStore";
import { getBwiAnalyticsDialog } from "../components/notifications/BwiAnalyticsDialog";
import { BwiNotificationDialog } from "../components/notifications/BwiNotificationRenderer";
// import { getBwiReleaseNotes } from "../components/notifications/BwiReleaseNotes";
import { MatrixClientPeg } from "../../MatrixClientPeg";
import { getFederationAnnouncement } from "../components/notifications/BwiFederationAnnouncement";
import { openBwiFederationOnboarding } from "../components/notifications/BwiFederationOnboarding";
import { BwiNotificationTypes } from "../components/notifications/BwiNotifications";
import { getBwiReleaseNotes } from "../components/notifications/BwiReleaseNotes";
import { BwiFeature } from "../settings/BwiFeature";
import { getWellKnownConfig } from "./WellKnown";

export const BWI_NOTIFICATION_SETTINGS = {
    /**
     * Flag to enable release notes. Defaults to true
     */
    showReleaseNotes: true,
};

export async function getBwiDialogs(): Promise<BwiNotificationDialog[]> {
    if (!SettingsStore.getValue(BwiFeature.Celebration)) return [];

    const notifications = await Promise.all([
        Promise.resolve(BwiAnalyticsManager.canSeeOption() ? getBwiAnalyticsDialog() : null),
        getFederationNotification(),
        getReleaseNotes(),
    ]);

    return notifications.filter((n) => n !== null) as BwiNotificationDialog[];
}

/**
 * Get current onboarding dialog
 */
export async function getBwiOnboardingDialog(): Promise<BwiNotificationDialog | null> {
    if (SettingsStore.getValue(BwiFeature.Federation)) {
        const wellKnownConfig = await getWellKnownConfig(MatrixClientPeg.safeGet().getHomeserverUrl());
        if (wellKnownConfig?.federation?.show_introduction) {
            return {
                notificationType: BwiNotificationTypes.HINT_FEDERATION_INTRODUCTION,
                createModal: openBwiFederationOnboarding,
            };
        }
    }

    return null;
}

async function getReleaseNotes(): Promise<BwiNotificationDialog | null> {
    if (BWI_NOTIFICATION_SETTINGS.showReleaseNotes === false) return null;

    try {
        return await getBwiReleaseNotes("2.21.0");
    } catch {
        return null;
    }
}

async function getFederationNotification(): Promise<BwiNotificationDialog | null> {
    const wellKnownConfig = await getWellKnownConfig(MatrixClientPeg.safeGet().getHomeserverUrl());
    if (wellKnownConfig?.federation?.show_introduction) return getBwiOnboardingDialog();
    else if (wellKnownConfig?.federation?.show_announcement) return getFederationAnnouncement();
    return null;
}
