// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { JoinRule, Room } from "matrix-js-sdk/src/matrix";
import { isPersonalNotesRoom } from "./PersonalNotes";

export function isVideoCallEnabledForRoom(room: Room): boolean {
    // BWI(MESSENGER-4574) Huddle. Disable calls for public rooms
    const isPublic = room.getJoinRule() === JoinRule.Public;

    // Disable for personal notes room
    const isPersonalNotes = isPersonalNotesRoom(room);

    return !isPublic && !isPersonalNotes;
}
