// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { SettingLevel } from "../../settings/SettingLevel";
import SettingsStore from "../../settings/SettingsStore";
import { BwiFeature } from "../settings/BwiFeature";
import { Room } from "matrix-js-sdk/src/matrix";
import { MatrixClientPeg } from "../../MatrixClientPeg";

export const getPersonalNotesConfig = (): boolean | undefined => {
    return SettingsStore.getValueAt(SettingLevel.CONFIG, BwiFeature.ShowPersonalNotes, null, true, true);
};

export const isPersonalNotesConfigured = (): boolean => {
    const personalNotesConfig = getPersonalNotesConfig();
    return typeof personalNotesConfig === "boolean" && personalNotesConfig === true;
};

export const NOTES_DATA = "de.bwi.personal_notes_room";

export const isPersonalNotesRoom = (room?: Room | null): boolean => {
    if (!room?.client) return false;
    const personalNotesEvent = room.client.getAccountData(NOTES_DATA);
    if (personalNotesEvent?.getContent().room_id === room.roomId) return true;
    return false;
};

export const getPersonalNotesRoom = (): Room | undefined => {
    const cli = MatrixClientPeg.get();
    return cli?.getRooms().find((room) => isPersonalNotesRoom(room));
};
