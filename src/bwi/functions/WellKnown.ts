// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import EventEmitter from "events";
import { AutoDiscovery, IClientWellKnown } from "matrix-js-sdk/src/matrix";
import { useEffect, useState } from "react";
import { MatrixClientPeg } from "../../MatrixClientPeg";

export interface BwiWellKnownConfig {
    dataPrivacyUrl?: string;
    imprintUrl?: string;
    federation?: {
        enable?: boolean;
        show_announcement?: boolean;
        show_introduction?: boolean;
    };
}

let wellKnownCache: Record<string, Promise<IClientWellKnown>> = {};
/**
 * Clear the well known cache. Mostly used for testing
 */
export function purgeWellKnownCache(): void {
    wellKnownCache = {};
}

export function bwiExtractWellKnownConfig(wellKnown?: IClientWellKnown | null): BwiWellKnownConfig | null {
    const bwiConfig = wellKnown?.["de.bwi"] as
        | {
              data_privacy_url?: string;
              imprint_url?: string;
              federation?: {
                  enable?: boolean;
                  show_announcement?: boolean;
                  show_introduction?: boolean;
              };
          }
        | undefined
        | null;
    if (!bwiConfig) return null;
    return {
        dataPrivacyUrl: bwiConfig.data_privacy_url || "",
        imprintUrl: bwiConfig.imprint_url || "",
        federation: bwiConfig.federation || {},
    };
}

export async function getWellKnownConfig(hsUrl: string): Promise<BwiWellKnownConfig | null> {
    if (!hsUrl) return null;
    if (!wellKnownCache[hsUrl]) {
        wellKnownCache[hsUrl] = AutoDiscovery.getRawClientConfig(new URL(hsUrl).host);
    }

    let wellKnown: IClientWellKnown | null = null;
    try {
        wellKnown = await wellKnownCache[hsUrl];
    } catch {
        // delete the cached version if something went wrong
        delete wellKnownCache[hsUrl];
        return null;
    }

    return bwiExtractWellKnownConfig(wellKnown);
}

// It would also be a good idea to just pass the current home server to the components so they can use it in the hook.
// Or provide a custom context with the homes server. But all of these changes can not be decoupled from the Element code very well.
// With this approach we just need to call bwi.setCurrentHomeServer(home server) and therefore can minimize code conflicts.
let currentHomeServer = "";
const eventEmitter = new EventEmitter();
export function setCurrentHomeServer(hsUrl: string): void {
    currentHomeServer = hsUrl;
    eventEmitter.emit("onNewHomeServer", hsUrl);
}

/**
 * Use the wellknown from the current homeserver (set by`bwi.setCurrentHomeServer()`)
 * TODO: Test if multi server is supported again
 */
export function useBwiWellKnownConfig(): BwiWellKnownConfig | null {
    const [homeServer, setHomeServer] = useState(MatrixClientPeg.get()?.getHomeserverUrl() || currentHomeServer);
    const [config, setConfig] = useState<BwiWellKnownConfig | null>(null);

    useEffect(() => {
        getWellKnownConfig(homeServer).then(setConfig);
    }, [homeServer]);

    useEffect(() => {
        setHomeServer(MatrixClientPeg.get()?.getHomeserverUrl() || currentHomeServer);
        eventEmitter.on("onNewHomeServer", setHomeServer);
        return () => {
            eventEmitter.off("onNewHomeServer", setHomeServer);
        };
    }, []);

    return config;
}
