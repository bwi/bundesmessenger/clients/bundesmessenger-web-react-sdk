// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { isNumber } from "matrix-js-sdk/src/utils";
import { IVariables, _td } from "../../languageHandler";
import PlatformPeg from "../../PlatformPeg";
import SdkConfig from "../../SdkConfig";
import SettingsStore from "../../settings/SettingsStore";
import { BwiFeature } from "../settings/BwiFeature";
export const BWI_ANALYTICS_MATRIX_EVENT_TYPE = "de.bwi.analytics";

const UID_KEY = "mx_Bwi_Analytics_uid";
const CREATION_TS_KEY = "mx_Bwi_Analytics_cts";
const VISIT_COUNT_KEY = "mx_Bwi_Analytics_vc";
const LAST_VISIT_TS_KEY = "mx_Bwi_Analytics_lvts";
const HEARTBEAT_INTERVAL = 30 * 1000;

export type DimensionKey = "RoomDevicesCount" | "RoomMembersCount";

export type ConfiguredDimensions = {
    [x: string]: number;
};

export interface Dimension {
    name?: DimensionKey;
    id: number;
    value: string;
}

interface IData {
    /* eslint-disable camelcase */
    gt_ms?: string;
    e_c?: string;
    e_a?: string;
    e_n?: string;
    e_v?: string;
    ping?: string;
    new_visit?: number;
    /* eslint-enable camelcase */
}

interface IVariable {
    id: number;
    expl: string; // explanation
    example: string; // example value
    getTextVariables?(): IVariables; // object to pass as 2nd argument to `_t`
}

const customVariables: Record<string, IVariable> = {
    // The Matomo installation at https://matomo.riot.im is currently configured
    // with a limit of 10 custom variables.
    "App Platform": {
        id: 1,
        expl: _td("The platform you're on"),
        example: "Electron Platform",
    },
    "App Version": {
        id: 2,
        expl: _td("The version of %(brand)s"),
        getTextVariables: () => ({
            brand: SdkConfig.get().brand,
        }),
        example: "15.0.0",
    },
};

// Note: we keep the analytics redaction on groups in case people have old links.
const hashRegex = /#\/(groups?|room|user|settings|register|login|forgot_password|home|directory)/;
const hashVarRegex = /#\/(group|room|user)\/.*$/;

// Remove all but the first item in the hash path. Redact unexpected hashes.
function getRedactedHash(hash: string): string {
    // Don't leak URLs we aren't expecting - they could contain tokens/PII
    const match = hashRegex.exec(hash);
    if (!match) {
        return "#/<unexpected hash location>";
    }

    if (hashVarRegex.test(hash)) {
        return hash.replace(hashVarRegex, "#/$1/<redacted>");
    }

    return hash.replace(hashRegex, "#/$1");
}

// Return the current origin, path and hash separated with a `/`. This does
// not include query parameters.
export function getRedactedUrl(): string {
    const { origin, hash } = window.location;
    let { pathname } = window.location;

    // Redact paths which could contain unexpected PII
    if (origin.startsWith("file://")) {
        pathname = "/<redacted>/";
    }

    return origin + pathname + getRedactedHash(hash);
}

function createUid(): string {
    return [...Array(16)].map(() => Math.random().toString(16)[2]).join("");
}

function getUid(): string {
    try {
        let data = localStorage?.getItem(UID_KEY);
        if (!data) {
            data = createUid();
            localStorage?.setItem(UID_KEY, data);
        }
        return data;
    } catch (e) {
        console.error("Analytics error: ", e);
        return "";
    }
}

export type AnalyticsSessionInfo = {
    time: number;
    platform: string;
    app_version: string;
    consent: boolean;
};

export type AnalyticsSessionsAccountData = {
    [key: string]: AnalyticsSessionInfo;
};

export async function getAnalyticsMetaData(newValue: boolean): Promise<AnalyticsSessionInfo> {
    const timestamp = Date.now();
    const version = (await PlatformPeg.get()?.getAppVersion()) || "";
    const sessionInfo: AnalyticsSessionInfo = {
        consent: newValue,
        platform: "web",
        time: timestamp,
        app_version: version,
    };
    return sessionInfo;
}

export class BwiAnalyticsClass {
    /** Used for testing */
    public static sharedInstance: BwiAnalyticsClass;
    private baseUrl: URL | null = null;
    private visitVariables: Record<number, [string, string]> = {}; // {[id: number]: [name: string, value: string]}
    private heartbeatIntervalID: number | null = null;
    private readonly creationTs: string | null;
    private readonly lastVisitTs: string | null;
    private readonly visitCount: string;
    private uid: string | null;

    public configuredDimensions: ConfiguredDimensions | null = null;

    public constructor() {
        this.creationTs = localStorage && localStorage.getItem(CREATION_TS_KEY);
        if (!this.creationTs && localStorage) {
            localStorage.setItem(CREATION_TS_KEY, (this.creationTs = String(new Date().getTime())));
        }

        this.uid = localStorage && localStorage.getItem(UID_KEY);
        if (!this.uid && localStorage) {
            this.uid = getUid();
            localStorage.setItem(UID_KEY, this.uid);
        }

        this.lastVisitTs = localStorage && localStorage.getItem(LAST_VISIT_TS_KEY);
        this.visitCount = (localStorage && localStorage.getItem(VISIT_COUNT_KEY)) || "0";
        this.visitCount = String(parseInt(this.visitCount, 10) + 1); // increment
        if (localStorage) {
            localStorage.setItem(VISIT_COUNT_KEY, this.visitCount);
        }
    }

    private async track(data: IData, dimensions?: Dimension[]): Promise<void> {
        if (this.disabled) return;

        const dims: Record<string, string> = {};
        if (dimensions?.length) {
            dimensions.forEach((dim) => (dims["dimension" + dim.id] = dim.value));
        }

        const now = new Date();
        const params = {
            ...data,
            ...dims,
            url: getRedactedUrl(),

            _cvar: JSON.stringify(this.visitVariables), // user custom vars
            res: `${window.screen.width}x${window.screen.height}`, // resolution as WWWWxHHHH
            rand: String(Math.random()).slice(2, 8), // random nonce to cache-bust
            h: now.getHours(),
            m: now.getMinutes(),
            s: now.getSeconds(),
            _id: this.uid,
        } as const;

        const url = new URL(this.baseUrl!.toString()); // copy
        Object.entries(params).forEach(([key, value]) => {
            url.searchParams.set(key, "" + value);
        });

        try {
            await window.fetch(url.toString(), {
                method: "GET",
                mode: "no-cors",
                cache: "no-cache",
                redirect: "follow",
            });
        } catch (e) {
            console.error("Analytics error: ", e);
        }
    }

    private setVisitVariable(key: keyof typeof customVariables, value: string): void {
        if (this.disabled) return;
        this.visitVariables[customVariables[key].id] = [key, value];
    }

    public isDimensionConfigured(dimension: DimensionKey): boolean {
        return !!this.configuredDimensions && isNumber(this.configuredDimensions[dimension]);
    }

    public ping(): void {
        this.track({
            ping: "1",
        });
        localStorage.setItem(LAST_VISIT_TS_KEY, String(new Date().getTime())); // update last visit ts
    }

    public get disabled(): boolean {
        return !this.baseUrl;
    }

    // Takes into account the users choice in the account data
    public canEnable(): boolean {
        const config = SdkConfig.get();
        if (!SettingsStore.getValue(BwiFeature.Analytics)) return false;
        if (!config?.matomo?.url || !config?.matomo?.siteId) return false;
        return true;
    }

    // Checks if the client would support error tracking and if user has the option to turn it on / off
    public canSeeOption(): boolean {
        const config = SdkConfig.get();

        if (!config?.matomo?.url || !config?.matomo?.siteId) return false;
        return true;
    }

    /**
     * Enable Analytics if initialized but disabled
     * otherwise try and initalize, no-op if piwik config missing
     */
    public async enable(): Promise<void> {
        if (!this.disabled) return;
        if (!this.canEnable()) return;
        const config = SdkConfig.get();
        const matomo = config.matomo!;
        this.baseUrl = new URL("piwik.php", matomo.url);
        // set constants
        this.baseUrl.searchParams.set("rec", "1"); // rec is required for tracking
        this.baseUrl.searchParams.set("idsite", matomo.siteId); // rec is required for tracking
        this.baseUrl.searchParams.set("apiv", "1"); // API version to use
        this.baseUrl.searchParams.set("send_image", "0"); // we want a 204, not a tiny GIF
        // set user parameters
        this.baseUrl.searchParams.set("_id", this.uid!); // visitor id
        this.baseUrl.searchParams.set("_idts", this.creationTs!); // first ts
        this.baseUrl.searchParams.set("_idvc", this.visitCount); // visit count
        if (this.lastVisitTs) {
            this.baseUrl.searchParams.set("_viewts", this.lastVisitTs); // last visit ts
        }
        if (matomo.dimensions) this.configuredDimensions = matomo.dimensions ?? null;

        const platform = PlatformPeg.get();
        this.setVisitVariable("App Platform", platform!.getHumanReadableName());
        try {
            this.setVisitVariable("App Version", await platform!.getAppVersion());
        } catch (e) {
            this.setVisitVariable("App Version", "unknown");
        }

        // start heartbeat
        this.heartbeatIntervalID = window.setInterval(this.ping.bind(this), HEARTBEAT_INTERVAL);
    }

    /**
     * Disable Analytics, stop the heartbeat and clear identifiers from localStorage
     */
    public disable(): void {
        if (this.disabled) return;
        window.clearInterval(this.heartbeatIntervalID!);
        this.baseUrl = null;
        this.visitVariables = {};
        localStorage.removeItem(UID_KEY);
        localStorage.removeItem(CREATION_TS_KEY);
        localStorage.removeItem(VISIT_COUNT_KEY);
        localStorage.removeItem(LAST_VISIT_TS_KEY);
        this.uid = createUid();
    }

    public async trackEvent(
        category: string,
        action: string,
        name?: string,
        value?: number,
        dimensions?: Dimension[],
        // eslint-disable-next-line @typescript-eslint/naming-convention, camelcase
        new_visit?: boolean,
    ): Promise<void> {
        if (this.disabled) return;

        await this.track(
            {
                e_c: category,
                e_a: action,
                e_n: name,
                e_v: "" + value,
                // eslint-disable-next-line camelcase
                new_visit: new_visit ? 1 : 0,
            },
            dimensions,
        );
    }

    public updateBwiAnalytics = (checked: boolean): void => {
        checked ? this.enable() : this.disable();
    };
}

if (!window.bwiAnalyticsManager) {
    BwiAnalyticsClass.sharedInstance = window.bwiAnalyticsManager = new BwiAnalyticsClass();
}
export default window.bwiAnalyticsManager;
