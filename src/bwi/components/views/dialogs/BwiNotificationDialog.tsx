// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactNode } from "react";
import classNames from "classnames";

import { _t } from "../../../../languageHandler";
import BaseDialog from "../../../../components/views/dialogs/BaseDialog";
import DialogButtons from "../../../../components/views/elements/DialogButtons";

interface IProps {
    headerImage?: string;
    customHeaderImage?: ReactNode;
    title?: string;
    description?: ReactNode;
    footer?: ReactNode;
    className?: string;
    confirmButton?: JSX.Element;
    cancelButton?: JSX.Element;
    hasCloseButton?: boolean;
    fixedWidth?: boolean;
    onKeyDown?(event: KeyboardEvent | React.KeyboardEvent): void;
    onFinished(): void;
}

export default class BwiNotificationDialog extends React.Component<IProps> {
    public static defaultProps = {
        title: "",
        description: "",
        hasCloseButton: false,
        onFinished: () => {},
    };

    private onFinished = (): void => {
        this.props.onFinished?.();
    };

    public render(): JSX.Element {
        return (
            <BaseDialog
                className="mx_InfoDialog mx_BwiNotificationDialog"
                onFinished={this.props.onFinished}
                title={this.props.title}
                contentId="mx_Dialog_content"
                hasCancel={this.props.hasCloseButton}
                onKeyDown={this.props.onKeyDown}
                fixedWidth={this.props.fixedWidth}
                headerImage={this.props.headerImage}
                customHeaderImage={this.props.customHeaderImage}
            >
                <div className={classNames("mx_Dialog_content", this.props.className)} id="mx_Dialog_content">
                    {this.props.description}
                </div>
                <div>
                    {this.props.confirmButton && (
                        <DialogButtons
                            primaryButton={this.props.confirmButton || _t("OK")}
                            cancelButton={this.props.cancelButton || null}
                            additive={
                                this.props.footer ? (
                                    <div className="mx_BwiNotificationDialog_footer">{this.props.footer}</div>
                                ) : null
                            }
                            onPrimaryButtonClick={this.onFinished}
                            onCancel={this.onFinished}
                            hasCancel={!!this.props.cancelButton}
                        />
                    )}
                </div>
            </BaseDialog>
        );
    }
}
