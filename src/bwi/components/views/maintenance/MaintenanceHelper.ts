// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

export const isDateSummerTime = (date: Date): number => {
    const jan = new Date(date.getFullYear(), 0, 1);
    const jul = new Date(date.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
};

export const convertEstToUtc = (date: Date): void => {
    const isSummerTime = date.getTimezoneOffset() < isDateSummerTime(date);
    if (isSummerTime) {
        date.setHours(date.getHours() - 2);
    } else {
        date.setHours(date.getHours() - 1);
    }
};

export const convertUtcToEst = (date: Date): void => {
    const isSummerTime = date.getTimezoneOffset() < isDateSummerTime(date);
    if (isSummerTime) {
        date.setHours(date.getHours() + 2);
    } else {
        date.setHours(date.getHours() + 1);
    }
};

export const timeStampToISO = (ts: number): string => {
    const date = new Date(ts);
    return date.toISOString();
};
