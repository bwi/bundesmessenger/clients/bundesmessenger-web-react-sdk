// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*
    This is in the src folder to ease doing runtime tests for the maintenance messages.
    It can be directly imported in the MaintenanceManager without circular dependencies.
 */

import { timeStampToISO } from "./MaintenanceHelper";
import {
    DowntimeInfo,
    MaintenanceInfo,
    ServerDowntimeInfo,
    ServerMaintenanceInfo,
} from "../../../managers/MaintenanceManager";

const today = Date.now();

const dayMultiplier = 24 * 60 * 60 * 1000;

export const DowntimeClosestMock: ServerDowntimeInfo = {
    start_time: timeStampToISO(today + 2 * dayMultiplier),
    end_time: timeStampToISO(today + 12 * dayMultiplier),
    warning_start_time: timeStampToISO(today - 1 * dayMultiplier),
    type: "MAINTENANCE_STANDARD",
};

export const DowntimeWarningMock: ServerDowntimeInfo = {
    start_time: timeStampToISO(today + 9 * dayMultiplier),
    end_time: timeStampToISO(today + 12 * dayMultiplier),
    warning_start_time: timeStampToISO(today - 1 * dayMultiplier),
    type: "MAINTENANCE_STANDARD",
    description: "This is a message on top of the message.",
};

export const DowntimeWarningParsedMock: DowntimeInfo = {
    start_time: today + 9 * dayMultiplier,
    end_time: today + 12 * dayMultiplier,
    warning_start_time: today - 1 * dayMultiplier,
    type: "MAINTENANCE_STANDARD",
    description: "This is a message on top of the message.",
};

export const DowntimeEndedMock: ServerDowntimeInfo = {
    start_time: timeStampToISO(today - 2 * dayMultiplier),
    end_time: timeStampToISO(today - 1 * dayMultiplier),
    warning_start_time: timeStampToISO(today - 12 * dayMultiplier),
    type: "MAINTENANCE_STANDARD",
};

export const DowntimeWarningAfterTodayMock: ServerDowntimeInfo = {
    start_time: timeStampToISO(today + 9 * dayMultiplier),
    end_time: timeStampToISO(today + 12 * dayMultiplier),
    warning_start_time: timeStampToISO(today + 1 * dayMultiplier),
    type: "MAINTENANCE_STANDARD",
};

export const DowntimeOngoingMock: ServerDowntimeInfo = {
    start_time: timeStampToISO(today - 1 * dayMultiplier),
    end_time: timeStampToISO(today + 4 * dayMultiplier),
    warning_start_time: timeStampToISO(today - 5 * dayMultiplier),
    type: "MAINTENANCE_STANDARD",
};

export const InvalidDowntimeMock: ServerDowntimeInfo = {
    start_time: "24.12.2021 16:00",
    end_time: timeStampToISO(today + 4 * dayMultiplier),
    warning_start_time: timeStampToISO(today - 5 * dayMultiplier),
    type: "MAINTENANCE_STANDARD",
};

export const MaintenanceOngoingWithDescriptionMock: ServerDowntimeInfo = {
    start_time: timeStampToISO(today - 1 * dayMultiplier),
    end_time: timeStampToISO(today + 4 * dayMultiplier),
    warning_start_time: timeStampToISO(today - 5 * dayMultiplier),
    type: "MAINTENANCE",
    description: "This is a message on top of the message.",
};

export const AdHocMock: ServerDowntimeInfo = {
    start_time: timeStampToISO(today - 1 * dayMultiplier),
    end_time: timeStampToISO(today + 4 * dayMultiplier),
    warning_start_time: timeStampToISO(today - 5 * dayMultiplier),
    type: "ADHOC_MESSAGE",
    description: "This is a special singular message.",
};

export const BlockingMock: ServerDowntimeInfo = {
    start_time: timeStampToISO(today - 1 * dayMultiplier),
    end_time: timeStampToISO(today + 4 * dayMultiplier),
    warning_start_time: timeStampToISO(today - 5 * dayMultiplier),
    type: "MAINTENANCE",
    description: "This is a message on top of the message.",
    blocking: true,
};

export const BlockingNotActiveMock: ServerDowntimeInfo = {
    start_time: timeStampToISO(today + 1 * dayMultiplier),
    end_time: timeStampToISO(today + 4 * dayMultiplier),
    warning_start_time: timeStampToISO(today - 5 * dayMultiplier),
    type: "MAINTENANCE",
    description: "This is a message on top of the message.",
    blocking: true,
};

export const InvalidWarningGTStartMock: ServerDowntimeInfo = {
    ...DowntimeOngoingMock,
    warning_start_time: timeStampToISO(today - 1 * dayMultiplier),
    start_time: timeStampToISO(today - 2 * dayMultiplier),
    end_time: timeStampToISO(today + 2 * dayMultiplier),
};
export const InvalidWarningGTEndingMock: ServerDowntimeInfo = {
    ...DowntimeOngoingMock,
    warning_start_time: timeStampToISO(today + 3 * dayMultiplier),
    start_time: timeStampToISO(today + 1 * dayMultiplier),
    end_time: timeStampToISO(today + 2 * dayMultiplier),
};
export const InvalidStartingGTEnding: ServerDowntimeInfo = {
    ...DowntimeOngoingMock,
    warning_start_time: timeStampToISO(today - 1 * dayMultiplier),
    start_time: timeStampToISO(today + 3 * dayMultiplier),
    end_time: timeStampToISO(today + 2 * dayMultiplier),
};

const versionMock = {
    versions: {
        android: {
            update_before: "mock",
            warn_before: "mock",
        },
        ios: {
            update_before: "mock",
            warn_before: "mock",
        },
    },
};

export const OngoingMaintenanceMock: ServerMaintenanceInfo = {
    downtime: [DowntimeOngoingMock],
    ...versionMock,
};

export const RegularMaintenanceMock: ServerMaintenanceInfo = {
    downtime: [DowntimeWarningMock],
    ...versionMock,
};

export const RegularMaintenanceParsedMock: MaintenanceInfo = {
    downtime: [DowntimeWarningParsedMock],
    ...versionMock,
};

export const InvalidDateMaintenanceMock: ServerMaintenanceInfo = {
    downtime: [InvalidDowntimeMock],
    ...versionMock,
};

export const InvalidDateMaintenanceParsedMock: MaintenanceInfo = {
    downtime: [],
    ...versionMock,
};

export const OngoingWithDescriptionMaintenanceMock: ServerMaintenanceInfo = {
    downtime: [MaintenanceOngoingWithDescriptionMock],
    ...versionMock,
};

export const AdhocMaintenanceMock: ServerMaintenanceInfo = {
    downtime: [AdHocMock],
    ...versionMock,
};

export const BlockingMaintenanceMock: ServerMaintenanceInfo = {
    downtime: [BlockingMock],
    ...versionMock,
};
