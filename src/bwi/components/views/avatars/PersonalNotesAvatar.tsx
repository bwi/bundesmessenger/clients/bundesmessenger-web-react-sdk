// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import classNames from "classnames";
import React from "react";
import { IProps as BaseAvatarProps } from "../../../../components/views/avatars/BaseAvatar";
import AccessibleButton from "../../../../components/views/elements/AccessibleButton";
import { _t } from "../../../../languageHandler";

export const PersonalNotesAvatarUrl = "bwi/img/Notizen-Avatar.svg";

export const getPersonalNotesAvatar = (props: BaseAvatarProps): JSX.Element => {
    // TODO: Props are spread all over the place, we should only spread the props that are actually allowed for the element.
    // otherwise we end up with nodes like <img idName="..." resizeMethod="..." />
    const {
        title,
        size,
        onClick,
        className,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        idName,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        customLocalAvatar,
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        urls,
        ...otherProps
    } = props;

    if (onClick) {
        return (
            <AccessibleButton
                className={classNames("mx_BaseAvatar mx_BaseAvatar_image", className)}
                element="img"
                src={PersonalNotesAvatarUrl}
                onClick={onClick}
                style={{
                    width: size,
                    height: size,
                }}
                title={title}
                alt={_t("Avatar")}
                {...otherProps}
            />
        );
    } else {
        return (
            <img
                className={classNames("mx_BaseAvatar mx_BaseAvatar_image", className)}
                src={PersonalNotesAvatarUrl}
                style={{
                    width: size,
                    height: size,
                }}
                title={title}
                alt=""
                {...otherProps}
            />
        );
    }
};
