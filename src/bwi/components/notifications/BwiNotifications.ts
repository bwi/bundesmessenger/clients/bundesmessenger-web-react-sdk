// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

export const NOTIFICATION_DATA = "de.bwi.notifications";

export const BwiNotificationTypes = {
    HINT_NOTES_ROOM: "should_show_feature_hint_notes_room",
    HINT_RELEASE_NOTES: "should_show_web_release_notes",
    HINT_BIRTHDAY_NOTIFICATION: "should_show_web_birthday_notification",
    HINT_ANALYTICS_NOTIFICATION: "should_show_analytics_consent_notification",
    HINT_FEDERATION_ANNOUNCEMENT: "should_show_federation_announcement",
    HINT_FEDERATION_INTRODUCTION: "should_show_federation_introduction",
} as const;

export type BwiNotificationValue = (typeof BwiNotificationTypes)[keyof typeof BwiNotificationTypes];
export type BwiNotification = keyof typeof BwiNotificationTypes;

export type BwiNotifications = {
    [key in BwiNotificationValue]?: boolean | { [key: string]: boolean };
};
