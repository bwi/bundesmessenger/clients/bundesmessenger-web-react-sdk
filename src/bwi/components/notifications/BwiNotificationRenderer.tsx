// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactNode, useContext, useEffect, useRef, useState } from "react";

import Modal from "../../../Modal";
import MatrixClientContext from "../../../contexts/MatrixClientContext";
import ICanvasEffect from "../../../effects/ICanvasEffect";
import { _t } from "../../../languageHandler";
import UIStore from "../../../stores/UIStore";
import { getBwiDialogs } from "../../functions/NotificationConfig";
import { BwiNotificationManager } from "../../managers/BwiNotificationManager";
import NotificationDialog from "../views/dialogs/BwiNotificationDialog";
import { BwiNotificationValue } from "./BwiNotifications";

export type DialogEffect = "confetti" | "fireworks" | "snowfall" | "spaceinvaders";

export type BwiNotificationBuilder = {
    notificationType: BwiNotificationValue;
    version?: string;
    headerImage?: string;
    customHeaderImage?: ReactNode;
    title: string;
    content: JSX.Element;
    triggerEffect?: DialogEffect;
    confirmButton?: JSX.Element;
    cancelButton?: JSX.Element;
    customFilterFunction?: () => Promise<boolean>;
    customFinishFunction?: () => Promise<void>;
    className?: string;
    fixedWidth?: boolean;
};

export type BwiNotificationCustom = {
    notificationType: BwiNotificationValue;
    createModal(onFinished: () => void): void;
    customFinishFunction?: () => Promise<void>;
    customFilterFunction?: () => Promise<boolean>;
    triggerEffect?: DialogEffect;
    version?: string;
};

export type BwiNotificationDialog = BwiNotificationBuilder | BwiNotificationCustom;

/**
 * Component to render BwiNotificationDialogs and effects
 * Triggered only after Passphrase is set
 */
export function BwiNotifications(): JSX.Element {
    const cli = useContext(MatrixClientContext);

    const [isKeyBackupEnabled, setKeyBackupEnabled] = useState(false);
    const [initialized, setInitialized] = useState(false);
    const canvasRef = useRef<HTMLCanvasElement>(null);
    const [hasEffect, setHasEffect] = useState(false);

    useEffect(() => {
        if (!initialized && isKeyBackupEnabled) {
            iniNotifications((effect) => {
                setHasEffect(true);
                if (canvasRef.current) {
                    effect.start(canvasRef.current);
                }
            }).then(() => {
                setInitialized(true);
            });
        }
    }, [initialized, isKeyBackupEnabled]);

    useEffect(() => {
        async function queryCrypto(): Promise<void> {
            const crypto = cli.getCrypto();
            if (crypto) {
                const isEnabled = (await crypto.getActiveSessionBackupVersion()) !== null;
                setKeyBackupEnabled(isEnabled);
            }
        }

        queryCrypto();
    }, [cli]);

    return (
        <>
            {hasEffect && isKeyBackupEnabled && (
                <canvas
                    ref={canvasRef}
                    width={UIStore.instance.windowWidth}
                    height={UIStore.instance.windowHeight}
                    style={{
                        display: "block",
                        zIndex: 100,
                        pointerEvents: "none",
                        position: "absolute",
                        top: 0,
                        right: 0,
                    }}
                />
            )}
        </>
    );
}

async function iniNotifications(onEffect: (effect: ICanvasEffect) => void): Promise<void> {
    // load dialogs
    const dialogs = await getBwiDialogs();

    const notificationsFiltered = await BwiNotificationManager.instance.filterAlreadySeen(dialogs);
    const notificationsToShow = [...notificationsFiltered].reverse();

    if (notificationsFiltered.length === 0) return;

    async function runEffectMaybe(dialog?: BwiNotificationDialog): Promise<void> {
        clearAllEffects();
        const effect = dialog?.triggerEffect;
        if (!effect) return;
        const effectModule = await lazyLoadEffect(effect);
        if (effectModule) {
            onEffect(effectModule);
        }
    }

    await runEffectMaybe(notificationsFiltered[0]);

    for (const notification of notificationsToShow) {
        const index = notificationsToShow.indexOf(notification);

        // eslint-disable-next-line no-inner-declarations
        async function onFinished(): Promise<void> {
            BwiNotificationManager.instance.hasSeenNotification(notification);
            await runEffectMaybe(notificationsToShow[index - 1]);
        }

        if ("createModal" in notification) {
            notification.createModal(() => {
                onFinished();
            });
        } else {
            Modal.createDialog(NotificationDialog, {
                headerImage: notification.headerImage,
                customHeaderImage: notification.customHeaderImage,
                title: notification.title,
                description: <div>{notification.content}</div>,
                confirmButton: notification.confirmButton,
                cancelButton: notification.cancelButton,
                footer: `${_t("Notification")} ${notificationsToShow.length - index} ${_t("of")} ${notificationsToShow.length}`,
                onFinished,
                className: notification.className,
                fixedWidth: notification.fixedWidth,
            });
        }
    }
}

const effectsCache = new Map<DialogEffect, ICanvasEffect>();

async function lazyLoadEffect(dialogEffect: DialogEffect): Promise<ICanvasEffect | null> {
    let effect = effectsCache.get(dialogEffect) || null;
    if (effect === null) {
        const options = {
            maxCount: 150,
            speed: 3,
            frameInterval: 15,
            alpha: 1.0,
            gradient: false,
        };
        try {
            const { default: Effect } = await import(`../../../effects/${dialogEffect}`);
            effect = new (Effect as new (options?: any) => ICanvasEffect)(options);
            effectsCache.set(dialogEffect, effect);
        } catch (err) {
            console.warn(`Unable to load effect module at '../../effects/${dialogEffect}.`, err);
        }
    }
    return effect;
}

/**
 * Stop and clear all effects
 */
function clearAllEffects(): void {
    effectsCache.forEach((effect) => {
        window.cancelAnimationFrame(effect.animationFrameId || -1);
    });
    effectsCache.clear();
}
