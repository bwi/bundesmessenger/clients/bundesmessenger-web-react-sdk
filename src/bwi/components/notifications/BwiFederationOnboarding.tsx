// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { Button } from "@vector-im/compound-web";
import React from "react";
import BuMLogo from "../../../../res/img/bwi/BuMLogo.svg";
import { Icon as UserIcon } from "../../../../res/img/compound/user.svg";
import Modal from "../../../Modal";
import BaseDialog from "../../../components/views/dialogs/BaseDialog";
import { _t } from "../../../languageHandler";
import { BwiDecoratedAvatar } from "../federation/DecoratedAvatar";
import { BwiAvatarStack } from "../structures/BwiAvatarStack";
import { PhasedModal, usePhasedModel } from "../structures/BwiPhasedModal";

export function BwiFederationOnboarding({ onFinished }: { onFinished(): void }): JSX.Element {
    const modal = usePhasedModel({
        initialPhase: "introduction",
        phaseOrder: ["introduction", "avatar", "settings"],
    });

    return (
        <BaseDialog hasCancel={false} fixedWidth={false}>
            <PhasedModal modal={modal} className="bwi_FederationOnboarding_dialog">
                <PhasedModal.Phase phase="introduction" className="bwi_FederationOnboarding_content">
                    <BwiAvatarStack
                        size="160px"
                        images={[
                            {
                                source: BuMLogo,
                                alt: "BuM Logo",
                            },
                            "placeholder",
                        ]}
                    />

                    <div className="bwi_FederationOnboarding_text">
                        <h2 className="bwi_FederationOnboarding_title">
                            {_t("bwi|federation|onboarding|phase_one_title")}
                        </h2>
                        <p className="bwi_FederationOnboarding_description">
                            {_t("bwi|federation|onboarding|phase_one_info")}
                        </p>
                    </div>
                </PhasedModal.Phase>
                <PhasedModal.Phase phase="avatar" className="bwi_FederationOnboarding_content">
                    <BwiDecoratedAvatar size="60px" type="user-federation" userId="@awd213:1209cx1m2.org">
                        <UserIcon className="bwi_FederationOnboarding_avatarImage" width={160} height={160} />
                    </BwiDecoratedAvatar>

                    <div className="bwi_FederationOnboarding_text">
                        <h2 className="bwi_FederationOnboarding_title">
                            {_t("bwi|federation|onboarding|phase_two_title")}
                        </h2>
                        <p className="bwi_FederationOnboarding_description">
                            {_t("bwi|federation|onboarding|phase_two_info")}
                        </p>
                    </div>
                </PhasedModal.Phase>
                <PhasedModal.Phase phase="settings" className="bwi_FederationOnboarding_content">
                    <img
                        src={require("../../../../res/img/bwi/federationOnboardingChat.png")}
                        width="100%"
                        alt="a chat window"
                        className="bwi_FederationOnboarding_chatImage"
                    />

                    <div className="bwi_FederationOnboarding_text">
                        <h2 className="bwi_FederationOnboarding_title">
                            {_t("bwi|federation|onboarding|phase_three_title")}
                        </h2>
                        <p className="bwi_FederationOnboarding_description">
                            {_t("bwi|federation|onboarding|phase_three_info")}
                        </p>
                        <Button
                            className="mx_AccessibleButton mx_NotOverrideButton bwi_FederationOnboarding_confirmButton"
                            size="sm"
                            kind="primary"
                            onClick={onFinished}
                        >
                            {_t("action|got_it")}
                        </Button>
                    </div>
                </PhasedModal.Phase>
            </PhasedModal>
        </BaseDialog>
    );
}

export function openBwiFederationOnboarding(onFinished?: () => void): void {
    Modal.createDialog(
        BwiFederationOnboarding,
        {
            onFinished() {
                onFinished?.();
            },
        },
        undefined,
        false,
        false,
        {
            async onBeforeClose(reason) {
                console.log("ON BEFORE CLOSE", reason);
                if (reason === "backgroundClick") return false;
                return true;
            },
        },
    );
}
