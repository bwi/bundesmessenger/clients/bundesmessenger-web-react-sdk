// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import { EventType, Room } from "matrix-js-sdk/src/matrix";
import React, { useState } from "react";
import Modal from "../../../Modal";
import BaseDialog from "../../../components/views/dialogs/BaseDialog";
import LabelledToggleSwitch from "../../../components/views/elements/LabelledToggleSwitch";
import { SettingsSection } from "../../../components/views/settings/shared/SettingsSection";
import { useRoomState } from "../../../hooks/useRoomState";
import { _t } from "../../../languageHandler";
import { updateRoomAcl } from "../../functions/federation";
import { useRoomFederationState } from "../../hooks/useRoomFederationState";

export function BwiEnableFederationSwitch({
    value,
    onChange,
    disabled,
    tooltip,
}: {
    value: boolean;
    onChange(value: boolean): void;
    disabled?: boolean;
    tooltip?: string;
}): JSX.Element {
    return (
        <LabelledToggleSwitch
            label={_t("Allow federation")}
            caption={_t("External organizations can join the room")}
            tooltip={tooltip}
            value={value}
            onChange={onChange}
            disabled={disabled}
        />
    );
}

export function BwiRoomFederationSettings({ room }: { room: Room }): JSX.Element {
    const isFederated = useRoomFederationState(room);
    const [busy, setBusy] = useState(false);
    const state = useRoomState(room);
    const canEdit = state.mayClientSendStateEvent(EventType.RoomServerAcl, room.client);

    const disabled = busy || !canEdit;

    return (
        <SettingsSection heading={_t("Federation")}>
            <BwiEnableFederationSwitch
                value={isFederated}
                onChange={async (value) => {
                    if (value === isFederated) return;

                    if (value === false) {
                        const modal = Modal.createDialog(ConfirmDisableFederationDialog);
                        const [proceed] = await modal.finished;
                        if (!proceed) return;
                    }

                    setBusy(true);
                    await updateRoomAcl(room, value);
                    setBusy(false);
                }}
                disabled={disabled}
                tooltip={!canEdit ? _t("forward|no_perms_title") : ""}
            />
        </SettingsSection>
    );
}

// Create custom ConfirmDialog because we have a reversed button style (primary left, secondary right) and QuestionDialog only supports one layout.
export function ConfirmDisableFederationDialog({ onFinished }: { onFinished(proceed?: boolean): void }): JSX.Element {
    return (
        <BaseDialog
            title={_t("Warning!")}
            className="mx_QuestionDialog"
            contentId="mx_Dialog_content"
            onFinished={onFinished}
            hasCancel={true}
        >
            <div className="mx_Dialog_content" id="mx_Dialog_content">
                <p>{_t("Withdrawing the federation removes all users from the other organization permanently.")}</p>
                <p>{_t("Proceed withdrawing federation?")}</p>
            </div>
            <div className="mx_Dialog_buttons">
                <span className="mx_Dialog_buttons_row">
                    <button
                        type="button"
                        className="mx_Dialog_primary"
                        onClick={() => {
                            onFinished(false);
                        }}
                    >
                        {_t("action|cancel")}
                    </button>
                    <button
                        type="button"
                        onClick={() => {
                            onFinished(true);
                        }}
                    >
                        {_t("Yes, withdraw")}
                    </button>
                </span>
            </div>
        </BaseDialog>
    );
}
