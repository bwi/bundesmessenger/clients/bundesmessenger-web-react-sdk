// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import classNames from "classnames";
import React, { ComponentPropsWithoutRef } from "react";

export function BwiFederationIcon({ className }: Pick<ComponentPropsWithoutRef<"div">, "className">): JSX.Element {
    return (
        <div className={classNames("bwi_FederationIcon", className)}>
            <svg viewBox="0 0 15 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="5.44" cy="5.44" r="5.44" className="bwi_FederationIcon_background" />
                <circle cx="8.85577" cy="5.44" r="5.44" className="bwi_FederationIcon_background" />
                <circle cx="8.85577" cy="5.4415" r="3.79535" className="bwi_FederationIcon_foreground" />
                <circle cx="5.43996" cy="5.43999" r="4.68093" className="bwi_FederationIcon_background" />
                <circle cx="5.44" cy="5.4415" r="3.79535" className="bwi_FederationIcon_foreground" />
            </svg>
        </div>
    );
}
