// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactNode, useCallback, useState } from "react";
import { EventType, Room } from "matrix-js-sdk/src/matrix";
import BaseDialog from "../../../components/views/dialogs/BaseDialog";
import RoomAvatar from "../../../components/views/avatars/RoomAvatar";
import classNames from "classnames";
import { Button } from "@vector-im/compound-web";
import { roomNameToAlias, updateRoomAcl } from "../../functions/federation";
import { _t } from "../../../languageHandler";
import { Icon as CheckmarkIcon } from "../../../../res/img/bwi/success-web.svg";
import { federationTracker } from "../../analytics/BwiAnalyticsSender";

interface IProps {
    room: Room;
    onFinished: (success: boolean) => void;
}

type ModalPhase = "decide" | "success" | "error";

export const FederationDecisionDialog: React.FC<IProps> = ({ room, onFinished }) => {
    const closeDialog = useCallback(() => onFinished(false), [onFinished]);

    const [phase, setPhase] = useState<ModalPhase>("decide");

    // check if no alias exists, create alias from name and timestamp and add as canonical alias
    const setAlias = useCallback(async () => {
        const client = room.client;
        const response = await client.getLocalAliases(room.roomId);
        const localAliases = response.aliases;
        const hasAlias = Array.isArray(localAliases) && localAliases.length !== 0;
        if (!hasAlias) {
            const alias = roomNameToAlias(room.name, client);
            await room.client.createAlias(alias, room.roomId);
            await client.sendStateEvent(room.roomId, EventType.RoomCanonicalAlias, { alias: alias });
        }
    }, [room]);

    const allowFederation = useCallback(async () => {
        try {
            await updateRoomAcl(room, true);
            await setAlias();
            if (room.getAltAliases()) {
                setPhase("success");
            }
            trackDecision(room, true);
        } catch {
            setPhase("error");
        }
    }, [room, setAlias]);

    const forbidFederation = useCallback(async () => {
        try {
            await updateRoomAcl(room, false);
            await setAlias();
            onFinished(true);
            trackDecision(room, false);
        } catch {
            setPhase("error");
        }
    }, [room, onFinished, setAlias]);

    let title: JSX.Element | undefined;
    let content: ReactNode = null;
    switch (phase) {
        case "decide":
            content = (
                <>
                    <RoomAvatar room={room} size="128px" />
                    <h2 className="mx_FederationDecisionDialog_title" data-testid="FederationDecisionDialog_title">
                        {_t("bwi|federation|decision_title", { roomName: room.name })}
                    </h2>
                    <div className="mx_FederationDecisionDialog_content">
                        {_t("bwi|federation|decision_description")}
                    </div>
                    <div className="mx_FederationDecisionDialog_buttons" data-testid="FederationDecisionDialog_buttons">
                        <Button
                            className="mx_AccessibleButton mx_NotOverrideButton"
                            size="sm"
                            kind="secondary"
                            onClick={allowFederation}
                        >
                            {_t("bwi|federation|decision_allow")}
                        </Button>
                        <Button
                            className="mx_AccessibleButton mx_NotOverrideButton"
                            size="sm"
                            kind="primary"
                            onClick={forbidFederation}
                        >
                            {_t("bwi|federation|decision_forbid")}
                        </Button>
                    </div>
                </>
            );
            break;
        case "success":
            content = (
                <>
                    <div className="mx_FederationDecisionDialog_success">
                        <CheckmarkIcon className="bwi_Icon_primary_accent mx_FederationDecisionDialog_successIcon" />
                        {_t("bwi|federation|decision_success")}
                    </div>
                    <div className="mx_FederationDecisionDialog_buttons" data-testid="FederationDecisionDialog_buttons">
                        <Button
                            className="mx_AccessibleButton mx_NotOverrideButton"
                            size="sm"
                            kind="primary"
                            onClick={() => {
                                onFinished(true);
                            }}
                        >
                            {_t("action|ok")}
                        </Button>
                    </div>
                </>
            );
            break;
        case "error":
            title = <>{_t("bwi|federation|decision_failed_title")}</>;
            content = (
                <>
                    {_t("bwi|federation|decision_failed_message")}
                    <div className="mx_FederationDecisionDialog_buttons" data-testid="FederationDecisionDialog_buttons">
                        <Button
                            className="mx_AccessibleButton mx_NotOverrideButton"
                            size="sm"
                            kind="primary"
                            onClick={() => {
                                onFinished(true);
                            }}
                        >
                            {_t("action|ok")}
                        </Button>
                    </div>
                </>
            );
            break;
        default:
            break;
    }

    return (
        <BaseDialog fixedWidth={false} hasCancel={false} onFinished={closeDialog} title={title}>
            <div className={classNames("mx_FederationDecisionDialog", `mx_FederationDecisionDialog_phase_${phase}`)}>
                {content}
            </div>
        </BaseDialog>
    );
};

async function trackDecision(room: Room, decision: boolean): Promise<void> {
    await federationTracker.sendFederationDecision(
        decision,
        room.getMembers().filter((member) => member.powerLevel >= 100).length,
    );
}
