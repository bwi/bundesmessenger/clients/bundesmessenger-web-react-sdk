// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import QuestionDialog from "../../../components/views/dialogs/QuestionDialog";
import Modal from "../../../Modal";
import { _t } from "../../../languageHandler";

export async function openFederatedAdminsNotAllowedModal(): Promise<void> {
    await Modal.createDialog(QuestionDialog, {
        title: _t("common|notice"),
        description: <div>{_t("bwi|federation|admin_power_level_forbidden")}</div>,
        button: _t("action|got_it"),
        hasCancelButton: false,
    }).finished;
}
