// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { PollResponseEvent } from "matrix-js-sdk/src/extensible_events_v1/PollResponseEvent";
import { PollAnswerSubevent } from "matrix-js-sdk/src/extensible_events_v1/PollStartEvent";
import { MatrixEvent, PollEvent, Relations, RoomMember } from "matrix-js-sdk/src/matrix";
import React, { useEffect, useMemo, useState } from "react";
import { Icon as TrophyIcon } from "../../../../res/img/element-icons/trophy.svg";
import { formatFullDate, formatTime, isDateToday } from "../../../DateUtils";
import { MatrixClientPeg } from "../../../MatrixClientPeg";
import MemberAvatar from "../../../components/views/avatars/MemberAvatar";
import AccessibleButton from "../../../components/views/elements/AccessibleButton";
import DisambiguatedProfile from "../../../components/views/messages/DisambiguatedProfile";
import BaseCard from "../../../components/views/right_panel/BaseCard";
import EntityTile from "../../../components/views/rooms/EntityTile";
import { Action } from "../../../dispatcher/actions";
import dis from "../../../dispatcher/dispatcher";
import { _t } from "../../../languageHandler";
import RightPanelStore from "../../../stores/right-panel/RightPanelStore";
import { getMemberDisplayName } from "../../functions/User";

interface GroupedAnswer {
    answer: {
        id: string;
        text: string;
    };
    votes: {
        ts: number;
        userId: string;
    }[];
}

function mapAnswers(
    relationEvents: MatrixEvent[],
    answers: PollAnswerSubevent[],
): { answers: GroupedAnswer[]; winnerCount: number } {
    const distinctVotes = relationEvents
        .flatMap((event) => {
            const response = new PollResponseEvent(event.getEffectiveEvent() as any);
            // map event
            return response.answerIds.map((answerId) => ({
                answerId,
                userId: event.getSender(),
                ts: event.getTs(),
            }));
        })
        .sort((a, b) => b.ts - a.ts) // sort by newest first
        .filter(
            // only take the latest answer
            // TODO: This only supports max_selection of 1, if the user is able to have multiple votes, this logic will fail.
            // But currently Elements logic `MPollBody.collectUserVotes` will also fail.
            (vote, index, votes) => votes.findIndex((v) => v.userId === vote.userId) === index,
        );

    const groupedAnswers = distinctVotes.reduce(
        (groups, vote) => {
            const group = groups.find((g) => g.answer.id === vote.answerId);

            if (group) {
                group.votes.push({
                    userId: vote.userId!,
                    ts: vote.ts,
                });
            }

            return groups;
        },
        answers.map((answer) => ({ answer: { id: answer.id, text: answer.text }, votes: [] })) as GroupedAnswer[],
    );

    // sort by vote count (many -> few)
    groupedAnswers.sort((a, b) => b.votes.length - a.votes.length);

    return {
        answers: groupedAnswers,
        winnerCount: Math.max(...groupedAnswers.map((a) => a.votes.length)),
    };
}

export default function BwiPollVotesRightPanel({ event }: { event: MatrixEvent }): JSX.Element {
    const client = MatrixClientPeg.safeGet();
    const room = client.getRoom(event.getRoomId())!;
    const poll = room.polls.get(event.getId()!)!;
    const question = poll.pollEvent.question.text;

    const [relations, setRelations] = useState<MatrixEvent[] | null>(null);
    const [expanded, setExpanded] = useState<string[]>([]);

    useEffect(() => {
        setRelations(null);
        setExpanded([]);

        const onChange = (rel: Relations): void => {
            setRelations(rel.getRelations());
        };

        poll.getResponses().then(onChange);
        poll.on(PollEvent.Responses, onChange);
        return () => {
            poll.off(PollEvent.Responses, onChange);
        };
    }, [poll]);

    const result = useMemo(() => {
        if (relations === null) return null;
        return mapAnswers(relations, poll.pollEvent.answers);
    }, [relations, poll]);

    const onClose = (): void => {
        RightPanelStore.instance.setCards([]);
    };

    const onMemberClicked = (member?: RoomMember | null): void => {
        if (!member) return;
        dis.dispatch({
            action: Action.ViewUser,
            member: member,
            push: true,
        });
    };

    return (
        <BaseCard
            header={
                <div className="bwi_PollVotesPanel_header">
                    <h3>{_t("Poll details")}</h3>
                </div>
            }
            onClose={onClose}
            className="bwi_PollVotesPanel"
        >
            <div className="bwi_PollVotesPanel_question">
                <span className="bwi_PollVotesPanel_questionText">{question}</span>
            </div>
            {result?.answers.map(({ answer, votes }) => {
                const isExpanded = expanded.includes(answer.id);
                const isWinner = poll.isEnded && votes.length === result.winnerCount;
                return (
                    <div key={answer.id} className="bwi_PollVotesPanel_answer">
                        <div className="bwi_PollVotesPanel_answerHeader">
                            <span className="bwi_PollVotesPanel_answerText">{answer.text}</span>
                            <div className="bwi_PollVotesPanel_answerVoteBadge">
                                {isWinner ? (
                                    <span className="bwi_PollVotesPanel_favoriteAnswer">
                                        <TrophyIcon className="mx_PollOption_winnerIcon" />
                                    </span>
                                ) : null}
                                <span className="bwi_PollVotesPanel_answerVoteBadgeText">
                                    {_t("%(count)s votes", { count: votes.length })}
                                </span>
                            </div>
                        </div>
                        <div className="bwi_PollVotesPanel_voteList">
                            {(isExpanded ? votes : votes.slice(0, 5)).map((vote) => {
                                const member = room.getMember(vote.userId);
                                const name = member ? getMemberDisplayName(member) : vote.userId;
                                const date = new Date(vote.ts);
                                const timeStamp = isDateToday(date)
                                    ? `${_t("Today")}, ${formatTime(date)}`
                                    : formatFullDate(date, false, false);
                                return (
                                    <EntityTile
                                        key={vote.userId}
                                        avatarJsx={
                                            <MemberAvatar member={member} size="36px" fallbackUserId={vote.userId} />
                                        }
                                        name={name}
                                        nameJSX={<DisambiguatedProfile member={member} fallbackName={name || ""} />}
                                        subtextLabel={timeStamp}
                                        showPresence={false}
                                        onClick={() => onMemberClicked(member)}
                                    />
                                );
                            })}
                            {!isExpanded && votes.length > 5 ? (
                                <AccessibleButton
                                    className="bwi_PollVotesPanel_loadMore"
                                    onClick={() => {
                                        setExpanded((s) => [...s, answer.id]);
                                    }}
                                >
                                    {_t("View all (%(count)s more)", {
                                        count: votes.length - 5,
                                    })}
                                </AccessibleButton>
                            ) : null}
                        </div>
                    </div>
                );
            })}
        </BaseCard>
    );
}
