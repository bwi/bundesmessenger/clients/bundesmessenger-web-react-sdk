// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";

import BaseAvatar from "../../../src/components/views/avatars/BaseAvatar";
import { mediaFromMxc } from "../../../src/customisations/Media";
import { RoomMember } from "matrix-js-sdk/src/matrix";
import { toPx } from "../../utils/units";

interface BwiUserTileProps {
    member: RoomMember;
    description?: boolean;
}

export const BwiUserTile: React.FC<BwiUserTileProps> = ({ member, description = false }) => {
    const avatarSize = 20;
    const avatar = (
        <BaseAvatar
            className="mx_InviteDialog_userTile_avatar"
            url={
                member.getMxcAvatarUrl()
                    ? mediaFromMxc(member.getMxcAvatarUrl()).getSquareThumbnailHttp(avatarSize)
                    : null
            }
            name={member.name}
            idName={member.userId}
            size={toPx(avatarSize)}
        />
    );

    return (
        <>
            {avatar}
            <span className="bwi_autocomplete_suggestion_title">{member.name}</span>
            {description && <span className="bwi_autocomplete_suggestion_description">{member.userId}</span>}
        </>
    );
};
