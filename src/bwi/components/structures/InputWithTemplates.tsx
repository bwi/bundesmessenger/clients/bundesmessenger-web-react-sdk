// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactElement } from "react";
import Field from "../../../components/views/elements/Field";
import Dropdown from "../../../components/views/elements/Dropdown";
import AccessibleButton from "../../../components/views/elements/AccessibleButton";
import { _t } from "../../../languageHandler";
import { NonEmptyArray } from "../../../@types/common";

export type InputTemplate = {
    key: string;
    label: string;
    description: string;
};

export type InputWithTemplatesProps = {
    label: string;
    placeholder: string;
    templates: InputTemplate[];
    showCounter?: boolean;
    initialValue?: string;
    maxChars?: number;
    saveCallback?: (result: string) => void;
    cancelCallback?: () => void;
};

const createOption = (key: string, label: string): React.ReactElement => (
    <div className="mx_CountryDropdown_option" key={key}>
        {label}
    </div>
);

export const InputWithTemplates: React.FC<InputWithTemplatesProps> = ({
    label,
    placeholder,
    initialValue,
    templates,
    showCounter,
    maxChars,
    saveCallback,
    cancelCallback,
}) => {
    const [template, setTemplate] = React.useState<string>("template");
    const [value, setValue] = React.useState(initialValue ? initialValue : "");

    const onChange = (inputEvent: React.FormEvent<HTMLInputElement>): void => {
        setValue(inputEvent.currentTarget.value);
    };

    const reset = (): void => {
        setValue("");
        setTemplate("template");
        if (cancelCallback) {
            cancelCallback();
        }
    };

    const getTemplateOptions = (): React.ReactElement[] => {
        const temps: React.ReactElement[] = [];
        temps.push(createOption("template", _t("Templates")));
        templates.forEach((template) => temps.push(createOption(template.key, template.label)));
        return temps;
    };

    const onTemplateChanged = (option: string): void => {
        if (option === "template") {
            setValue("");
        } else {
            const temp = templates.find((t) => t.key === option);
            if (temp) setValue(temp.description);
        }
        setTemplate(option);
    };

    const valueLength = value.length;

    return (
        <div>
            <Field
                type="text"
                value={value}
                onChange={onChange}
                label={label}
                placeholder={placeholder}
                autoComplete="off"
                maxLength={maxChars}
                data-testid="template-input"
                postfixComponent={
                    <Dropdown
                        id={`${label}_dropdown`}
                        className="mx_bwi_iwt_dropdown"
                        label={label}
                        children={getTemplateOptions() as NonEmptyArray<ReactElement & { key: string }>}
                        menuWidth={120}
                        value={template}
                        searchEnabled={false}
                        onOptionChange={onTemplateChanged}
                    />
                }
            />
            {showCounter && (
                <div className="mx_bwi_iwt_counter">
                    {valueLength} {maxChars ? ` / ${maxChars}` : ""} {` ${_t("Character", { count: valueLength })}`}
                </div>
            )}
            {valueLength > 0 && (
                <div className="mx_ProfileSettings_buttons">
                    <AccessibleButton onClick={reset} kind="link">
                        {_t("Cancel")}
                    </AccessibleButton>
                    <AccessibleButton onClick={() => saveCallback?.(value)} kind="primary" disabled={valueLength === 0}>
                        {_t("Save")}
                    </AccessibleButton>
                </div>
            )}
        </div>
    );
};
