// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import classNames from "classnames";
import React, { ReactNode } from "react";

type Image =
    | {
          source: string;
          alt: string;
          withoutBorder?: boolean;
      }
    | "placeholder";

export function BwiAvatarStack({
    images,
    size,
    className,
}: {
    images: Image[];
    size?: string;
    className?: string;
}): React.JSX.Element {
    const nodes = images.map((image, index) => {
        let imgNode: ReactNode;
        let classes = "bwi_AvatarStack_item";

        if (image !== "placeholder") {
            imgNode = <img src={image.source} alt={image.alt} />;
            if (image.withoutBorder) {
                classes += " --without-border";
            }
        }

        return (
            <div key={index} className={classes} style={{ zIndex: images.length - index }}>
                {imgNode}
            </div>
        );
    });

    return (
        <div
            style={
                {
                    "--__avatar-stack-size": size || "142px",
                } as any
            }
            className={classNames("bwi_AvatarStack", className)}
        >
            {nodes}
        </div>
    );
}
