// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { throttle } from "lodash";
import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import SettingsStore from "../../../settings/SettingsStore";
import { BwiFeature } from "../../settings/BwiFeature";
import { MatrixEvent } from "matrix-js-sdk/src/matrix";
import { _t, getUserLanguage } from "../../../languageHandler";

// BWI (MESSENGER-4265) Floating date

export type BwiFloatingDateConfig =
    | boolean
    | {
          enabled?: boolean;
          hideAfterMs?: number;
      };

export function BwiFloatingDate({ show, label }: { label: null | string; show: boolean }): JSX.Element {
    const ref = useRef<HTMLDivElement>(null);
    const [showLabel, setShowLabel] = useState(show);

    useLayoutEffect(() => {
        if (showLabel) {
            ref.current?.animate?.(
                [
                    { transform: "translateY(-110%)", opacity: "0" },
                    { transform: "translateY(0%)", opacity: "1" },
                ],
                {
                    duration: 200,
                },
            );
        }
    }, [showLabel]);

    useEffect(() => {
        function onFinished(): void {
            setShowLabel(false);
        }

        if (show) {
            setShowLabel(true);
        } else {
            if (!ref.current?.animate) return;
            const animation = ref.current.animate(
                [
                    { transform: "translateY(0%)", opacity: "1" },
                    { transform: "translateY(-110%)", opacity: "0" },
                ],
                {
                    duration: 200,
                },
            );
            animation.addEventListener("finish", onFinished);
            return () => {
                animation.removeEventListener("finish", onFinished);
            };
        }
    }, [show]);

    return (
        <>
            {showLabel && (
                <div className="bwi_MessagePanel_DateHighlight_container" ref={ref}>
                    <div className="bwi_MessagePanel_DateHighlight_date">{label}</div>
                </div>
            )}
        </>
    );
}

function getDateLabel(date: number, format: Intl.DateTimeFormat): string {
    const today = new Date();
    if (date > +new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, -1)) {
        return _t("Today");
    }
    if (date > +new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1, 0, 0, -1)) {
        return _t("Yesterday");
    }
    return format.format(date);
}

export function bwiCreateFloatingDateScrollHandler({
    getEvents,
    setState,
}: {
    getEvents: () => MatrixEvent[];
    setState: (state: { label?: string | null; show?: boolean }) => void;
}): (event: Event) => void {
    const config = SettingsStore.getValue<BwiFloatingDateConfig | undefined>(BwiFeature.FloatingDateIndicator);
    let enabled = false;
    let hideAfterMs = 1000;

    if (typeof config === "object") {
        enabled = config.enabled ?? false;
        hideAfterMs = config.hideAfterMs ?? hideAfterMs;
    } else {
        enabled = config ?? false;
    }

    if (!enabled) return () => {};

    let hideDateHighlightTimout: any;

    // en'Apr 6, 2023', de'6. Apr. 2023'
    const format = new Intl.DateTimeFormat(getUserLanguage(), { day: "numeric", month: "short", year: "numeric" });

    return throttle(
        (scrollEvent: Event) => {
            const scrollContainer = scrollEvent.target as HTMLDivElement;
            const tiles = scrollContainer.querySelectorAll<HTMLDivElement>(".mx_EventTile");
            const containerOffset = scrollContainer.getBoundingClientRect().top;
            const heights = Array.from(tiles).map((tile) => {
                return {
                    tile,
                    eventId: tile.dataset.eventId,
                    offsetTop: tile.getBoundingClientRect().top + tile.clientHeight - containerOffset,
                };
            });
            const positivHeights = heights.filter(({ offsetTop }) => offsetTop >= 0);
            const currentEvent = positivHeights[0];
            const isLast = positivHeights.length === heights.length;

            if (!isLast && currentEvent) {
                const event = getEvents().find((e) => e.getId() === currentEvent.eventId);
                if (!event) {
                    return;
                }
                const date = +new Date(event.localTimestamp);

                setState({
                    label: getDateLabel(date, format),
                    show: true,
                });
                clearTimeout(hideDateHighlightTimout);
                hideDateHighlightTimout = setTimeout(() => {
                    setState({
                        show: false,
                    });
                }, hideAfterMs);
            } else {
                setState({
                    show: false,
                });
            }
        },
        150,
        {
            leading: true,
        },
    );
}
