// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactNode } from "react";
import { BwiContentContainer, BwiContentContainerProps } from "./ContentContainer";
import { Spinner } from "@matrix-org/react-sdk-module-api/lib/components/Spinner";

type ContentLoaderProps = {
    label: ReactNode;
    /**
     * Optional children to render instead of the default spinner
     */
    children?: ReactNode;
} & Pick<BwiContentContainerProps, "width" | "height" | "type">;
export function BwiContentLoader({ label, children, ...containerProps }: ContentLoaderProps): React.JSX.Element {
    return (
        <BwiContentContainer label={label} {...containerProps}>
            {children !== undefined ? children : <Spinner />}
        </BwiContentContainer>
    );
}
