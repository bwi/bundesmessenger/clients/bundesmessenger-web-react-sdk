// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactNode } from "react";

export interface BwiContentContainerProps {
    children: ReactNode;
    label: ReactNode;
    type: "image" | "video" | "file";
    width?: number;
    height?: number;
}
export function BwiContentContainer({
    children,
    label,
    type,
    width = 300,
    height = 300,
}: BwiContentContainerProps): React.JSX.Element {
    const styles = {
        "--_bwi-content_width": width + "px",
        "--_bwi-content_height": height + "px",
    } as any;

    if (type === "image" || type === "video")
        return (
            <div className="bwi_Content" style={styles} data-bwi_content_type={type}>
                <span className="bwi_Content_placeholder">{children}</span>
                <span className="bwi_Content_label">{label}</span>
            </div>
        );

    if (type === "file") {
        return (
            <div className="bwi_Content" data-bwi_content_type={type}>
                {children && <span className="mx_MediaBody bwi_Content_placeholder">{children}</span>}
                <span className="bwi_Content_label">{label}</span>
            </div>
        );
    }

    return <></>;
}
