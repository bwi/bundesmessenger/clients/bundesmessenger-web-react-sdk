// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { ReactNode } from "react";
import { ContentScanError, ScanResult } from "./ContentScanner";
import { _t } from "../../languageHandler";
import { BwiContentContainer, BwiContentContainerProps } from "./ContentContainer";
import { BwiContentErrorIcon } from "./ContentErrorIcon";

type ContentErrorProps = {
    error: ContentScanError;
    /**
     * Optional children to render instead of the default icon
     */
    children?: ReactNode;
} & Pick<BwiContentContainerProps, "width" | "height" | "type">;

export function BwiContentError({ error, children, ...containerProps }: ContentErrorProps): React.JSX.Element {
    const { reason } = error;

    const message = getScanErrorMessage(reason);

    return (
        <BwiContentContainer label={message} {...containerProps}>
            {children !== undefined ? children : <BwiContentErrorIcon type={reason} />}
        </BwiContentContainer>
    );
}

export function getScanErrorMessage(reason: ScanResult): string {
    if (reason === "not-found") {
        return _t("bwi|timeline|content|not_found");
    } else if (reason === "unsafe") {
        return _t("bwi|timeline|content|unsafe");
    } else if (reason === "request-failed") {
        return _t("bwi|timeline|content|request-failed");
    } else {
        return _t("bwi|timeline|content|unknown");
    }
}
