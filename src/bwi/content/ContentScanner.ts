// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import SdkConfig from "../../SdkConfig";
import { getHttpUriForMxc, ResizeMethod } from "matrix-js-sdk/src/matrix";
import { EncryptedFile } from "matrix-js-sdk/src/types";
import { logger } from "matrix-js-sdk/src/logger";
import { MatrixClientPeg } from "../../MatrixClientPeg";

export interface IScanError {
    info: string;
    reason: ScanError;
}

interface ScanResponse {
    reason?: ScanError;
    clean?: boolean;
    info: string;
}

export type ScanResult = "safe" | "unsafe" | "not-found" | "request-failed" | "unknown";

export enum ScanError {
    RequestFailed = "MCS_MEDIA_REQUEST_FAILED",
    DecryptFailed = "MCS_MEDIA_FAILED_TO_DECRYPT",
    NotClean = "MCS_MEDIA_NOT_CLEAN",
    BadDecryption = "MCS_BAD_DECRYPTION",
    Malformed = "MCS_MALFORMED_JSON",
    NotFound = "M_NOT_FOUND",
}

// CS Api: https://github.com/element-hq/matrix-content-scanner-python/blob/main/docs/api.md

// TODO: olm to be replaced
export class ContentScannerClass {
    public constructor() {}

    private key: Olm.PkEncryption | null = null;
    private keyPromise: Promise<void> | null = null;
    public cache = new Map<string, ScanResult>();
    private pendingScans = new Map<string, Promise<ScanResult>>();
    private mediaProxyPath = "/_matrix/media_proxy/unstable";

    private loadPublicKey = async (): Promise<void> => {
        return new Promise((resolve, reject) => {
            fetch(`${this.csFullPath}/public_key`)
                .then((r) => r.json())
                .then((res) => {
                    if (res && res["public_key"]) {
                        this.key = new global.Olm.PkEncryption();
                        this.key.set_recipient_key(res["public_key"]);
                        resolve();
                    } else {
                        throw new Error(ScanError.Malformed);
                    }
                })
                .catch((e) => {
                    reject(e);
                });
        });
    };

    private initKey = async (): Promise<void> => {
        await global.Olm.init();
        if (!this.keyPromise) {
            this.keyPromise = this.loadPublicKey().catch(() => {
                this.keyPromise = null;
            });
        }

        return this.keyPromise;
    };

    public urlForMxc(mxc: string, width?: number, height?: number, method?: ResizeMethod): string {
        const matrixUrl = getHttpUriForMxc(this.url, mxc, width, height, method);
        return matrixUrl.replace(/media\/r0/, "media_proxy/unstable");
    }

    private get url(): string {
        //return `http://localhost:8081`; //use local content scanner
        return MatrixClientPeg.safeGet().getHomeserverUrl();
    }

    public get canEnable(): boolean {
        return !!SdkConfig.get().content_scanner?.enabled;
    }

    private get csFullPath(): string {
        return this.url + this.mediaProxyPath;
    }

    public async download(mxc: string, file?: EncryptedFile): Promise<Response> {
        try {
            const download = await this.request("download", mxc, file);
            this.cache.set(mxc, "safe");
            return download;
        } catch (error) {
            if (error instanceof ContentScanError) {
                this.cache.set(mxc, error.reason);
            }
            throw error;
        }
    }

    public async scan(mxc: string, file?: EncryptedFile): Promise<ScanResult> {
        let pendingScan = this.pendingScans.get(mxc);
        if (!pendingScan) {
            pendingScan = this.request("scan", mxc, file).then((result) => {
                this.cache.set(mxc, result);
                return result;
            });
            this.pendingScans.set(mxc, pendingScan);
        }
        return pendingScan;
    }

    private async request(type: "scan", mxc: string, file?: EncryptedFile): Promise<ScanResult>;
    private async request(type: "download", mxc: string, file?: EncryptedFile): Promise<Response>;
    private async request(
        type: "download" | "scan",
        mxc: string,
        file?: EncryptedFile,
    ): Promise<Response | ScanResult> {
        let res: Response;

        try {
            if (file) {
                if (!this.key) {
                    await this.initKey();
                }

                res = await fetch(`${this.csFullPath}/${type}_encrypted`, {
                    method: "POST",
                    body: JSON.stringify({
                        encrypted_body: this.key!.encrypt(JSON.stringify({ file })),
                    }),
                    headers: {
                        "Content-Type": "application/json",
                    },
                });
            } else {
                res = await fetch(`${this.csFullPath}/${type}/${mxc.substring("mxc://".length)}`);
            }
        } catch (error) {
            logger.error(error);
            if (type === "scan") {
                return "unknown";
            } else {
                throw new ContentScanError("unknown");
            }
        }

        if (res.ok) {
            return this.getOkResponse(res, type);
        } else {
            const scanResponse: ScanResponse = await res.json();
            const errorResult = this.getErrorFromResponse(res.status, scanResponse.reason);
            if (type === "scan") return errorResult;
            else throw new ContentScanError(errorResult, scanResponse.info);
        }
    }

    private getOkResponse = async (res: Response, type: "scan" | "download"): Promise<ScanResult | Response> => {
        if (type === "scan") {
            const scanResponse: ScanResponse = await res.json();
            if (scanResponse.clean === true) return "safe";
            return "unsafe";
        } else {
            return res;
        }
    };

    private getErrorFromResponse = (status: number, reason: ScanError | undefined): ScanResult => {
        let errorResult: ScanResult;
        if (status === 400 && reason === ScanError.Malformed) errorResult = "request-failed";
        else if (status === 400 && reason === ScanError.DecryptFailed) errorResult = "request-failed";
        else if (status === 403 && reason === ScanError.BadDecryption) errorResult = "request-failed";
        else if (status === 403 && reason === ScanError.NotClean) errorResult = "unsafe";
        else if (status === 404 && reason === ScanError.NotFound) errorResult = "not-found";
        else if (status === 502 && reason === ScanError.RequestFailed) errorResult = "request-failed";
        else errorResult = "unknown";
        return errorResult;
    };
}

export default class ContentScanner {
    private static internalInstance: ContentScannerClass | undefined;

    public static get instance(): ContentScannerClass {
        if (!ContentScanner.internalInstance) {
            ContentScanner.internalInstance = new ContentScannerClass();
        }

        return ContentScanner.internalInstance;
    }
}

window.bwiContentScanner = ContentScanner.instance;

export class ContentScanError extends Error {
    public constructor(
        public reason: ScanResult,
        message?: string,
    ) {
        super(message ?? "Scan failed");
    }
}
