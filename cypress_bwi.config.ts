import { defineConfig } from "cypress";

export default defineConfig({
    videoUploadOnPasses: false,
    projectId: "ppvnzg",
    experimentalInteractiveRunEvents: true,
    defaultCommandTimeout: 10000,
    chromeWebSecurity: false,
    e2e: {
        setupNodeEvents(on, config) {
            return require("./cypress/plugins/index.ts").default(on, config);
        },
        baseUrl: "http://localhost:8080",
        experimentalSessionAndOrigin: true,
        specPattern: "cypress/e2e_bwi/**/*.{js,jsx,ts,tsx}",
    },
});
