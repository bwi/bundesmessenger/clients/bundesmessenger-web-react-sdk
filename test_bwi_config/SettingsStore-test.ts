// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import fs from "fs";
import path from "path";

import { IConfigOptions } from "../src/IConfigOptions";
import SettingsStore from "../src/settings/SettingsStore";

const configDir = "../config-and-assets/config/";
const baseConfigPath = "base/base.json";

const getAllConfigs = (dir: string): Map<string, IConfigOptions> => {
    if (!fs.existsSync(dir)) {
        throw new Error(`Config Dir: ${dir} does not exist`);
    }

    const configs = new Map();

    // baseconfig
    configs.set("base", getConfig(path.join(configDir, baseConfigPath)));

    const files = fs.readdirSync(dir).filter((f) => f.endsWith(".json"));

    files.map((file) => {
        configs.set(path.basename(file, ".json"), getConfig(path.join(dir, file)));
    });

    return configs;
};

const getConfig = (configPath: string): IConfigOptions => {
    if (!fs.existsSync(configPath)) {
        throw new Error(`Config: ${configPath} does not exist`);
    }

    return JSON.parse(fs.readFileSync(configPath).toString());
};

const allConfigs = getAllConfigs(configDir);

describe("Feature Flags", () => {
    const testFeatureFlag = (key: string, value: any) => {
        expect(() => {
            SettingsStore.getDefaultValue(key);
        }).not.toThrow();

        // const valueDefault = SettingsStore.getDefaultValue(key);

        // This test is not save because the default value of a possible string value can be null
        // So this test will throw expected string but received object(null)
        // expect(typeof valueDefault).toEqual(typeof value);
    };

    const testableNamespaces = ["bwi_setting_defaults", "setting_defaults"];

    describe.each(Array.from(allConfigs.entries()))("Config: %s", (name, sdkConfig) => {
        const namespaces = Array.from(Object.entries(sdkConfig));
        describe.each(namespaces.filter(([key, value]) => testableNamespaces.includes(key)))(
            "Namespace: %s",
            (_, config) => {
                it("should be an object", () => {
                    expect(config).toBeTruthy();
                    expect(typeof config).toBe("object");
                });
                it.each(Array.from(Object.entries(config)))("Flag: %s", testFeatureFlag);
            },
        );
    });
});
