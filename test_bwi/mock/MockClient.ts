// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import {
    IClientWellKnown,
    IPresenceOpts,
    MatrixClient,
    Room,
    User,
    TypedEventEmitter,
    ClientPrefix,
} from "matrix-js-sdk/src/matrix";
import Login from "../../src/Login";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import { TILE_SERVER_WK_KEY } from "../../src/utils/WellKnownUtils";
import * as testUtils from "../../test/test-utils";
import { getMockClientWithEventEmitter, mockClientMethodsUser } from "../../test/test-utils";
import { mockClientStore } from "../test-utils/client";
import { generateRoomId } from "./MockRoom";
import { mockUser, MockUser } from "./MockUser";
import { DeepPartial } from "./utils";
// eslint-disable-next-line no-restricted-imports
import { CrossSigningInfo } from "matrix-js-sdk/src/crypto/CrossSigning";
// eslint-disable-next-line no-restricted-imports
import { FetchHttpApi } from "matrix-js-sdk/src/http-api/fetch";

export const locationWellKnown: IClientWellKnown = {
    [TILE_SERVER_WK_KEY.altName]: {
        map_style_url: "mock_from_wellknown",
    },
};

export function getClientUserMocks(
    user: string | User = mockUser,
    profile?: { avatar_url?: string; displayname?: string },
) {
    const mocks = {
        ...mockClientMethodsUser(),
        getProfileInfo: jest.fn().mockResolvedValue(profile || {}),
        getCrypto: jest.fn(),
    };
    if (typeof user === "string") {
        return {
            ...mocks,
            getUserId: jest.fn().mockReturnValue(user),
            getSafeUserId: jest.fn().mockReturnValue(user),
            getUser: jest.fn().mockReturnValue(new User(user)),
        };
    } else {
        return {
            ...mocks,
            getUserId: jest.fn().mockReturnValue(user.userId),
            getSafeUserId: jest.fn().mockReturnValue(user.userId),
            getUser: jest.fn().mockReturnValue(user),
        };
    }
}

export function getClientServerMocks() {
    return {
        doesServerSupportSeparateAddAndBind: jest.fn(),
        getIdentityServerUrl: jest.fn(),
        getDomain: jest.fn(),
        getCapabilities: jest.fn().mockReturnValue({}),
        getClientWellKnown: jest.fn().mockReturnValue({}),
        doesServerSupportUnstableFeature: jest.fn().mockResolvedValue(false),
        isVersionSupported: jest.fn().mockResolvedValue(false),
        getVersions: jest.fn().mockResolvedValue({}),
        isFallbackICEServerAllowed: jest.fn(),
        waitForClientWellKnown: jest.fn().mockResolvedValue({}),
    };
}

export const getBaseClient = (props?: DeepPartial<MatrixClient>) => {
    // const emitter = new EventEmitter();
    // const client = { ...testUtils.createTestClient() };
    // client.http = {};
    // client.doesServerSupportSeparateAddAndBind = jest.fn().mockReturnValue(false);
    // client.getThreePids = jest.fn().mockReturnValue([]);
    // client.getIdentityServerUrl = jest.fn();
    const args = Object.assign(
        {},
        {
            doesServerSupportUnstableFeature: jest.fn().mockReturnValue(false),
            isSynapseAdministrator: jest.fn().mockResolvedValue(false),
            isUserIgnored: jest.fn().mockReturnValue(false),
            isCryptoEnabled: jest.fn().mockReturnValue(false),
            getIdentityServerUrl: jest.fn(),
            getClientWellKnown: jest.fn().mockResolvedValue(undefined),
        },
        props,
    );
    const client = getMockClientWithEventEmitter(args);
    return client;
};

export const extendSpyAuthRequest = (client: MatrixClient, resolveValue: unknown) => {
    client.http.authedRequest = jest.fn().mockResolvedValue(resolveValue);
    return client;
};

export const getAuthRequestClientSpy = (resolveValue: unknown) => {
    const client = getBaseClient({
        ...getClientUserMocks(),
        http: {
            authedRequest: jest.fn().mockResolvedValue(resolveValue),
        },
    });

    jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
    jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValue(client);

    return client;
};

export const getRequestClientSpy = (resolveValue: unknown) => {
    const client = getBaseClient({
        ...getClientUserMocks(),
        ...getClientServerMocks(),
        getClientWellKnown: jest.fn().mockResolvedValue(undefined),
        http: {
            request: jest.fn().mockResolvedValue(resolveValue),
            disableBlocking: jest.fn(),
            enableBlocking: jest.fn(),
        },
        getIdentityServerUrl: jest.fn().mockReturnValue(false),
    });

    jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
    jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValue(client);

    return client;
};

export const getTempClientSpy = (resolveValue: unknown) => {
    const client = getBaseClient({
        http: {
            request: jest.fn().mockResolvedValue(resolveValue),
            disableBlocking: jest.fn(),
        },
    });
    return jest.spyOn(Login.prototype, "createTemporaryClient").mockReturnValue(client);
};

export const getLoggedInViewClientSpy = (resolveValue: unknown) => {
    const client = getBaseClient({
        ...getClientUserMocks(),
        getMediaHandler: jest.fn().mockReturnValue({
            setAudioInput: jest.fn(),
            setVideoInput: jest.fn(),
        }),
        getSyncState: jest.fn().mockReturnValue("SYNCING"),
        getSyncStateData: jest.fn().mockReturnValue(null),
        ...{
            http: {
                request: jest.fn().mockResolvedValue(resolveValue),
                disableBlocking: jest.fn(),
            },
        },
    });
    jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
    jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValue(client);

    return client;
};

export const getRoomClientSpy = (room: Room) => {
    const client = getBaseClient({
        ...getClientUserMocks(),
        getRoom: jest.fn().mockReturnValue(room),
        isRoomEncrypted: () => false,
    });
    jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
    jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValue(client);

    return client;
};

export const getRoomCrawlSpy = (emptyResponse?: boolean) => {
    const client = getBaseClient();
    const id = generateRoomId();
    Object.assign(client, {
        getRoom: jest.fn().mockReturnValue(undefined),
        getRoomIdForAlias: jest.fn().mockReturnValue(id),
    });

    const room = testUtils.mkStubRoom(id, "room_name", client);
    room.getCanonicalAlias = jest.fn().mockReturnValue(room.roomId.replace("!", "#"));

    Object.assign(client, {
        http: {
            authedRequest: emptyResponse
                ? jest.fn().mockRejectedValue({})
                : jest.fn().mockResolvedValue({ rooms: [room] }),
        },
    });

    jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
    jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValue(client);

    return client;
};

export const extendAccountDataSpyRequest = (client: MatrixClient, account: unknown) => {
    client.setAccountData = jest.fn().mockResolvedValue(account);
    return client;
};

export const getAccountDataSpy = (account: unknown) => {
    const client = getBaseClient({
        ...getClientUserMocks(),
        http: {
            authedRequest: jest.fn().mockResolvedValue(account),
        },
        setAccountData: jest.fn().mockResolvedValue(account),
        getDeviceId: jest.fn().mockReturnValue("ABCDEFGHI"),
    });

    jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
    jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValue(client);

    return client;
};

export const getRejectedAccountDataSpy = (rejection: unknown) => {
    const client = getBaseClient({
        ...getClientUserMocks(),
        http: {
            authedRequest: jest.fn().mockRejectedValue(rejection),
        },
        setAccountData: jest.fn().mockRejectedValue(rejection),
    });

    jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
    jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValue(client);

    return client;
};

export const getEncryptionClient = (isReady: boolean, authedResponse?: any, accountDataResponse?: any) => {
    const client = getBaseClient({
        ...getClientUserMocks(),
        doesServerSupportUnstableFeature: jest.fn().mockResolvedValue(true),
        isCryptoEnabled: jest.fn().mockResolvedValue(true),
        isInitialSyncComplete: jest.fn().mockResolvedValue(true),
        isCrossSigningReady: jest.fn().mockResolvedValue(isReady),
        isSecretStorageReady: jest.fn().mockResolvedValue(isReady),
        getKeyBackupEnabled: jest.fn().mockReturnValue(isReady),
        downloadKeys: jest.fn().mockResolvedValue([]),
        getCrossSigningId: jest.fn().mockReturnValue(undefined),
        getStoredCrossSigningForUser: jest.fn().mockReturnValue(true),
        getStoredDevicesForUser: jest.fn().mockReturnValue([]),
        getVerificationRequestsToDeviceInProgress: jest.fn().mockReturnValue([]),
        isSecretStored: jest.fn().mockResolvedValue([]),
        getDehydratedDevice: jest.fn().mockResolvedValue(undefined),
        getDefaultSecretStorageKeyId: jest.fn().mockReturnValue(undefined),
        getIdentityServerUrl: jest.fn(),
        getCapabilities: () => ({}),
        getClientWellKnown: () => undefined,
        setPassword: jest.fn().mockResolvedValue({}),
        getUserIdLocalpart: jest.fn().mockReturnValue(""),
        getDevices: jest.fn().mockResolvedValue({ devices: [] }),
        isVersionSupported: () => Promise.resolve(true),
        getDeviceId: jest.fn().mockReturnValue("42"),
        secretStorage: {
            isStored: jest.fn(),
            getDefaultKeyId: jest.fn().mockResolvedValue(null),
        },
        getKeyBackupVersion: jest.fn().mockResolvedValue(null),
    });

    if (authedResponse) {
        extendSpyAuthRequest(client, authedResponse);
    }

    if (accountDataResponse) {
        extendAccountDataSpyRequest(client, accountDataResponse);
    }
    jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
    jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValue(client);

    return client;
};

export const getPresenceClient = (res: IPresenceOpts) => {
    const user = mockUser;
    user.presenceStatusMsg = "Mock";

    const client = getBaseClient({
        getPresence: jest.fn().mockResolvedValue(res),
        setPresence: jest.fn().mockResolvedValue(res),
        isUserIgnored: jest.fn().mockReturnValue(false),
        ...getClientUserMocks(user),
        isCryptoEnabled: jest.fn().mockReturnValue(false),
        doesServerSupportUnstableFeature: jest.fn().mockReturnValue(true),
        isSynapseAdministrator: jest.fn().mockResolvedValue(false),
        getProfileInfo: jest.fn().mockResolvedValue({
            displayname: "Name Example",
        }),
        ...mockClientStore(),
        getEventMapper: jest.fn().mockReturnValue({}),
    });

    return client;
};

export const getLoginSpy = () => {
    const client = getBaseClient();
    Object.assign(client, {
        login: jest.fn().mockResolvedValue({}),
        loginFlows: jest.fn().mockResolvedValue({ flows: [{ type: "m.login.password" }] }),
    });
    return client;
};

export const getVerificationSpy = () => {
    const client = getBaseClient({
        ...getClientUserMocks(),
        ...testUtils.mockClientMethodsCrypto(),
        getDevice: jest.fn(),
        requestVerification: jest.fn().mockResolvedValue({ on: jest.fn(), then: jest.fn(), off: jest.fn() }),
        getVerificationRequestsToDeviceInProgress: jest.fn().mockResolvedValue([]),
        isSecretStored: jest.fn().mockResolvedValue(true),
        getDehydratedDevice: jest.fn().mockResolvedValue(undefined),
        secretStorage: {
            hasKey: jest.fn(),
            isStored: jest.fn(),
        },
    });
    const crossSigningInfo: CrossSigningInfo = new CrossSigningInfo(MockUser.userId);
    crossSigningInfo.checkDeviceTrust = jest.fn().mockReturnValue({
        isCrossSigningVerified: jest.fn().mockReturnValue(true),
    });
    // @ts-ignore
    const device: DeviceInfo = { getIdentityKey: jest.fn().mockReturnValue("Mockdevice") };
    Object.assign(client, {
        getStoredCrossSigningForUser: jest.fn().mockReturnValue(crossSigningInfo),
        getStoredDevicesForUser: jest.fn().mockReturnValue([device]),
    });
    jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
    jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValue(client);

    return client;
};

export const getWellKnownSpy = () => {
    const client = getBaseClient({
        ...getClientUserMocks(),
        ...mockClientStore(),
        getEventMapper: jest.fn(),
        getClientWellKnown: jest.fn().mockReturnValue(locationWellKnown),
    });

    jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
    jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValue(client);

    return client;
};

export const getBlockingClientSpy = () => {
    const baseUrl = "http://baseUrl";
    const prefix = ClientPrefix.V3;

    const client = getBaseClient({
        ...getClientUserMocks(),
        ...getClientServerMocks(),
        getClientWellKnown: jest.fn().mockResolvedValue(undefined),
        http: new FetchHttpApi(new TypedEventEmitter<any, any>(), { baseUrl, prefix, onlyData: true }),
        getIdentityServerUrl: jest.fn().mockReturnValue(false),
    });
    return jest.spyOn(MatrixClientPeg, "get").mockReturnValue(client);
};
