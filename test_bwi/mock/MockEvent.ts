// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import * as testUtils from "../../test/test-utils";
import { tryTransformPermalinkToLocalHref } from "../../src/utils/permalinks/Permalinks";

export const AliasLink = "https://bwm.to/#/room/#FindMe:bwm.to";
export const AliasLinkEvent = testUtils.mkEvent({
    type: "m.room.message",
    room: "room_id",
    user: "sender",
    content: {
        body: AliasLink,
        msgtype: "m.text",
    },
    event: true,
});

export const IdLink = "https://bwm.to/#/room/!FindMe:bwm.to";
export const IdLinkEvent = testUtils.mkEvent({
    type: "m.room.message",
    room: "room_id",
    user: "sender",
    content: {
        body: IdLink,
        msgtype: "m.text",
    },
    event: true,
});

export const getMockLinkForRoom = (roomIdentifier: string) =>
    tryTransformPermalinkToLocalHref(`https://bwm.to/#/room/${roomIdentifier}`);

export const getMockLinkForMessage = (roomIdentifier: string) =>
    tryTransformPermalinkToLocalHref(`https://bwm.to/#/room/${roomIdentifier}/$12ACG63221`);

export const localIdLink = "#/room/!FindMe:bwm.to";
export const localAliasLink = "#/room/#FindMe:bwm.to";
