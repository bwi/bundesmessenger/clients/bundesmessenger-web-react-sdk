// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { PasswordPolicyRules } from "../../src/bwi/settings/PasswordPolicy";
import SdkConfig from "../../src/SdkConfig";

export const getSdkSpy = () => {
    const sdkconf = {
        validated_server_config: {
            isUrl: false,
        },
    };
    return jest.spyOn(SdkConfig, "get").mockReturnValue(sdkconf);
};

export const getPasswordPolicySpy = (policy: PasswordPolicyRules) => {
    const sdkconf = {
        validated_server_config: {
            isUrl: false,
        },
        password_policy: policy,
        default_country_code: "GB",
    };
    return jest.spyOn(SdkConfig, "get").mockReturnValue(sdkconf);
};
