// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import bwi from "../../src/bwi";
import SdkConfig from "../../src/SdkConfig";

describe("SdkConfig.map_custom_attribution", () => {
    describe("getCustomMapAttribution", () => {
        it("should use custom attribution", () => {
            const customAttribution = [
                "<a href='https://bwi.de' target='_blank'>© BWI GmbH</a>",
                "<a href='https://mock.com'>© Mock</a>",
            ];
            SdkConfig.put({ map_custom_attribution: customAttribution });
            expect(bwi.getCustomMapAttribution()?.customAttribution).toEqual(customAttribution);
        });

        it("should not use custom attribution", () => {
            SdkConfig.put({ map_custom_attribution: undefined });
            expect(bwi.getCustomMapAttribution()?.customAttribution).toBeUndefined();
        });
    });
});
