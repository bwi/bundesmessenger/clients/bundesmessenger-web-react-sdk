// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import {
    getAuthHeaderTransparency,
    getConfiguredTransparency,
    isTransparencyConfigured,
} from "../../src/bwi/functions/LoginBackground";
import { IConfigOptions } from "../../src/IConfigOptions";
import { getPlatformSpy } from "../mock/MockPlatform";
import AuthBody from "../../src/components/views/auth/AuthBody";
import { act, render, screen } from "@testing-library/react";
import React from "react";
import SdkConfig from "../../src/SdkConfig";

describe("SdkConfig.branding.welcome_background_transparency", () => {
    getPlatformSpy();

    function setupSdkConfig(transparency: number | undefined) {
        const brand: IConfigOptions["branding"] = {
            home_byline: "mock",
            home_title: "mock",
            welcome_background_url: "mock",
        };

        if (transparency) brand["welcome_background_transparency"] = transparency;

        SdkConfig.put({ branding: brand });
    }

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    it("no transparency set in config, render regular style", async () => {
        setupSdkConfig(undefined);
        await act(async () => {
            render(<AuthBody />);
        });
        expect(getAuthHeaderTransparency()).toBe(0.59);
        expect(isTransparencyConfigured()).toBe(false);
        expect(getConfiguredTransparency()).toBe(undefined);
        const style = window.getComputedStyle(screen.getByTestId("mx_AuthBody"));
        expect(style.backgroundColor).not.toBe("transparent");
    });

    it("transparency set in config, render with transparency", async () => {
        setupSdkConfig(0.95);
        await act(async () => {
            render(<AuthBody />);
        });
        expect(getAuthHeaderTransparency()).toBe(0.95);
        expect(isTransparencyConfigured()).toBe(true);
        expect(getConfiguredTransparency()).toBe(0.95);
        const style = window.getComputedStyle(screen.getByTestId("mx_AuthBody"));
        expect(style.backgroundColor).toBe("transparent");
    });
});
