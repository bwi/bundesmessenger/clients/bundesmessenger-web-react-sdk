// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import * as CreateRoom from "../../src/createRoom";
import * as TestUtils from "../../test/test-utils";
import { PersonalNotesRoomManager } from "../../src/bwi/managers/PersonalNotesRoomManager";

import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import { createRoom, generateRoomId } from "../mock/MockRoom";
import { DefaultTagID } from "../../src/stores/room-list/models";
import { waitForTimers } from "../mock/MockHelper";
import { MatrixError } from "matrix-js-sdk/src/matrix";

describe("PersonalNotesRoomManager", () => {
    afterEach(() => {
        PersonalNotesRoomManager.resetManager();
        jest.clearAllMocks();
        jest.resetModules();
    });

    beforeEach(() => {
        TestUtils.stubClient();
    });

    describe("createPersonalNotesRoom", () => {
        it("creates a room successfully, sets tag and stores id in account data", async () => {
            const roomId = generateRoomId();
            const createRoomSpy = jest.spyOn<any, any>(CreateRoom, "default").mockResolvedValue(roomId);
            const cli = MatrixClientPeg.safeGet();
            cli.setRoomTag = jest.fn().mockResolvedValue({});
            cli.setAccountData = jest.fn().mockResolvedValue({});

            await PersonalNotesRoomManager.instance.createPersonalNotesRoom({});

            expect(createRoomSpy).toHaveBeenCalledTimes(1);
            expect(cli.setRoomTag).toHaveBeenCalledTimes(1);
            expect(cli.setRoomTag).toHaveBeenCalledWith(roomId, "m.favourite", expect.anything());
            expect(cli.setAccountData).toHaveBeenCalledTimes(1);
            expect(cli.setAccountData).toHaveBeenCalledWith("de.bwi.personal_notes_room", { room_id: roomId });
        });

        it("fails room creation and doesn't save anything", async () => {
            const createRoomSpy = jest.spyOn<any, any>(CreateRoom, "default").mockRejectedValue("Mock");
            const cli = MatrixClientPeg.safeGet();
            cli.setRoomTag = jest.fn().mockResolvedValue({});
            cli.setAccountData = jest.fn().mockResolvedValue({});

            try {
                await PersonalNotesRoomManager.instance.createPersonalNotesRoom({});
            } catch {}

            expect(createRoomSpy).toHaveBeenCalledTimes(1);
            const callParams: CreateRoom.IOpts = createRoomSpy.mock.calls[0] as unknown as CreateRoom.IOpts;
            // make sure that no initial state like the previous room avatar is set
            expect(callParams.createOpts?.creation_content).toBeUndefined();
            expect(cli.setRoomTag).toHaveBeenCalledTimes(0);
            expect(cli.setAccountData).toHaveBeenCalledTimes(0);
        });
    });

    describe("checkExistingRooms", () => {
        it("tagged room, room id in account data, keep room", async () => {
            const cli = MatrixClientPeg.safeGet();
            const pnr = createRoom(cli, {
                tags: {
                    [DefaultTagID.Favourite]: {},
                },
                name: "Personal Notes Room",
            });
            const other = createRoom(cli, { tags: { "m.favourite": {} }, name: "Some other room" });
            cli.getRooms = jest.fn().mockReturnValue([pnr, other]);
            cli.deleteRoomTag = jest.fn().mockResolvedValue({});

            await PersonalNotesRoomManager.instance.checkExistingRooms({ room_id: pnr.roomId });
            expect(cli.deleteRoomTag).toHaveBeenCalledTimes(0);
        });

        it("tagged room but not in account data, remove tag", async () => {
            jest.useFakeTimers();
            const cli = MatrixClientPeg.safeGet();
            const pnr = createRoom(cli, {
                tags: {
                    [DefaultTagID.Favourite]: {},
                    ["de.bwi.personal_notes_room"]: {},
                },
                name: "Personal Notes Room",
            });
            const other = createRoom(cli, { tags: { "m.favourite": { order: 0.1 } }, name: "Some other room" });
            cli.getRooms = jest.fn().mockReturnValue([pnr, other]);
            cli.deleteRoomTag = jest.fn().mockResolvedValue({});
            cli.setRoomTag = jest.fn().mockResolvedValue({});

            await PersonalNotesRoomManager.instance.checkExistingRooms({ room_id: other.roomId });
            await waitForTimers();
            expect(cli.deleteRoomTag).toHaveBeenCalledTimes(1);
            expect(cli.deleteRoomTag).toHaveBeenCalledWith(pnr.roomId, "de.bwi.personal_notes_room");
        });

        it("tagged room, avatar set, remove avatar", async () => {
            jest.useFakeTimers();
            const cli = MatrixClientPeg.safeGet();
            const pnr = createRoom(cli, {
                tags: { [DefaultTagID.Favourite]: {}, ["de.bwi.personal_notes_room"]: {} },
                name: "Personal Notes Room",
            });
            pnr.getMxcAvatarUrl = jest.fn().mockReturnValue("mockAvatarUrl");
            const other = createRoom(cli, { tags: { "m.favourite": { order: 0.1 } }, name: "Some other room" });
            cli.getRooms = jest.fn().mockReturnValue([pnr, other]);
            cli.sendStateEvent = jest.fn().mockResolvedValue({});
            cli.deleteRoomTag = jest.fn().mockResolvedValue({});
            cli.setRoomTag = jest.fn().mockResolvedValue({});

            await PersonalNotesRoomManager.instance.checkExistingRooms({ room_id: pnr.roomId });
            await waitForTimers();
            expect(cli.sendStateEvent).toHaveBeenCalledTimes(1);
        });

        it("tagged room, no avatar set, remove avatar", async () => {
            jest.useFakeTimers();
            const cli = MatrixClientPeg.safeGet();
            const pnr = createRoom(cli, {
                tags: { [DefaultTagID.Favourite]: {} },
                name: "Personal Notes Room",
            });
            pnr.getMxcAvatarUrl = jest.fn().mockReturnValue(false);
            const other = createRoom(cli, { tags: { "m.favourite": { order: 0.1 } }, name: "Some other room" });
            cli.getRooms = jest.fn().mockReturnValue([pnr, other]);
            cli.sendStateEvent = jest.fn().mockResolvedValue({});
            cli.deleteRoomTag = jest.fn().mockResolvedValue({});
            cli.setRoomTag = jest.fn().mockResolvedValue({});

            await PersonalNotesRoomManager.instance.checkExistingRooms({ room_id: pnr.roomId });
            await waitForTimers();
            expect(cli.sendStateEvent).toHaveBeenCalledTimes(0);
        });

        it("old personal notes room, migrate to favourite", async () => {
            jest.useFakeTimers();
            const cli = MatrixClientPeg.safeGet();
            const pnr = createRoom(cli, {
                tags: { ["de.bwi.personal_notes_room"]: {} },
                name: "Personal Notes Room",
            });

            cli.getRooms = jest.fn().mockReturnValue([pnr]);
            cli.sendStateEvent = jest.fn().mockResolvedValue({});
            cli.deleteRoomTag = jest.fn().mockResolvedValue({});
            cli.setRoomTag = jest.fn().mockResolvedValue({});

            await PersonalNotesRoomManager.instance.checkExistingRooms({ room_id: pnr.roomId });
            await waitForTimers();
            expect(cli.deleteRoomTag).toHaveBeenCalledTimes(1);
            expect(cli.deleteRoomTag).toHaveBeenCalledWith(pnr.roomId, "de.bwi.personal_notes_room");
            expect(cli.setRoomTag).toHaveBeenCalledTimes(1);
            expect(cli.setRoomTag).toHaveBeenCalledWith(pnr.roomId, "m.favourite", undefined);
        });
    });

    describe("managePersonalNotes", () => {
        it(`room already exists and is stored in account data,
            call checkExistingRooms and don't create room`, async () => {
            jest.useFakeTimers();
            const cli = MatrixClientPeg.safeGet();
            // @ts-ignore
            cli.http = { authedRequest: jest.fn().mockResolvedValue({ room_id: generateRoomId() }) };
            PersonalNotesRoomManager.prototype.checkExistingRooms = jest.fn().mockResolvedValue({});
            PersonalNotesRoomManager.prototype.createPersonalNotesRoom = jest.fn().mockResolvedValue({});

            await PersonalNotesRoomManager.instance.managePersonalNotes();

            await jest.runAllTimers();
            expect(PersonalNotesRoomManager.prototype.checkExistingRooms).toHaveBeenCalledTimes(1);
            expect(PersonalNotesRoomManager.prototype.createPersonalNotesRoom).toHaveBeenCalledTimes(0);
        });

        it("no room exists yet, create room", async () => {
            const cli = MatrixClientPeg.safeGet();

            cli.http = {
                authedRequest: jest.fn().mockRejectedValue(
                    new MatrixError({
                        errcode: "M_NOT_FOUND",
                    }),
                ),
            } as any;
            PersonalNotesRoomManager.prototype.checkExistingRooms = jest.fn().mockResolvedValue({});
            PersonalNotesRoomManager.prototype.createPersonalNotesRoom = jest.fn().mockResolvedValue({});

            await PersonalNotesRoomManager.instance.managePersonalNotes();

            expect(PersonalNotesRoomManager.prototype.checkExistingRooms).toHaveBeenCalledTimes(0);
            expect(PersonalNotesRoomManager.prototype.createPersonalNotesRoom).toHaveBeenCalledTimes(1);
        });

        it("reset room set, create new room", async () => {
            const cli = MatrixClientPeg.safeGet();
            // @ts-ignore
            cli.http = { authedRequest: jest.fn().mockResolvedValue({ reset: true }) };
            PersonalNotesRoomManager.prototype.checkExistingRooms = jest.fn().mockResolvedValue({});
            PersonalNotesRoomManager.prototype.createPersonalNotesRoom = jest.fn().mockResolvedValue({});

            await PersonalNotesRoomManager.instance.managePersonalNotes();

            expect(PersonalNotesRoomManager.prototype.checkExistingRooms).toHaveBeenCalledTimes(0);
            expect(PersonalNotesRoomManager.prototype.createPersonalNotesRoom).toHaveBeenCalledTimes(1);
        });

        it("check that room creation is really just called once", async () => {
            jest.useFakeTimers();
            const cli = MatrixClientPeg.safeGet();
            // @ts-ignore
            cli.http = { authedRequest: jest.fn().mockResolvedValue({ reset: true }) };
            PersonalNotesRoomManager.prototype.checkExistingRooms = jest.fn().mockResolvedValue({});
            PersonalNotesRoomManager.prototype.createPersonalNotesRoom = jest.fn().mockResolvedValue({});

            // Three simultaneous requests to singleton
            await Promise.all([
                PersonalNotesRoomManager.instance.managePersonalNotes(),
                PersonalNotesRoomManager.instance.managePersonalNotes(),
                PersonalNotesRoomManager.instance.managePersonalNotes(),
            ]);

            await jest.runAllTimers();

            // Another request after room is created
            await PersonalNotesRoomManager.instance.managePersonalNotes();

            expect(PersonalNotesRoomManager.prototype.checkExistingRooms).toHaveBeenCalledTimes(0);
            expect(PersonalNotesRoomManager.prototype.createPersonalNotesRoom).toHaveBeenCalledTimes(1);
            expect(PersonalNotesRoomManager.instance["isInitialized"]).toBe(true);
            expect(PersonalNotesRoomManager.instance["initializing"]).toBe(false);
        });
    });
});
