// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { User } from "matrix-js-sdk/src/matrix";
import { ExternalProfile } from "../../../../src/bwi/components/views/right_panel/ExternalProfile";
import { render, screen } from "@testing-library/react";
import React from "react";
import SdkConfig from "../../../../src/SdkConfig";
import { BwiConfigOptions } from "../../../../src/bwi/settings/SdkConfigOptions";

describe("ExternalProfile", () => {
    const mockUser = new User("Mock Jagger");

    const completeConfig: BwiConfigOptions = {
        external_user_profile: {
            link: "https://mocklink.com/?mockParam=rawDisplayName",
            buttonLabel: "mockLabel",
            dynamicParam: "rawDisplayName",
        },
    };

    const justLinkConfig: BwiConfigOptions = {
        external_user_profile: {
            link: "https://mocklink.com/?mockParam=rawDisplayName",
        },
    };

    it("no url given, no external profile rendered", () => {
        const { container } = render(<ExternalProfile member={mockUser} />);
        expect(container.firstChild).toBeNull();
    });

    it("no member given but complete config make no param replacement", () => {
        SdkConfig.put({
            brand: "mock",
            ...completeConfig,
        });
        render(<ExternalProfile member={undefined!} />);
        expect(screen.getByTestId("userinfo_external_profile_container")).toBeInTheDocument();
        expect(screen.getByTestId("userinfo_external_profile_label")).toHaveTextContent(
            completeConfig.external_user_profile!.buttonLabel!,
        );
        expect(screen.getByTestId("userinfo_external_profile_link").getAttribute("href")).toBe(
            completeConfig.external_user_profile!.link!,
        );
    });

    it("only link config, make no param replacement", () => {
        SdkConfig.put({
            brand: "mock",
            ...justLinkConfig,
        });
        render(<ExternalProfile member={mockUser} />);
        expect(screen.getByTestId("userinfo_external_profile_container")).toBeInTheDocument();
        expect(screen.getByTestId("userinfo_external_profile_label")).toHaveTextContent("External Profile");
        expect(screen.getByTestId("userinfo_external_profile_link").getAttribute("href")).toBe(
            justLinkConfig.external_user_profile!.link,
        );
    });

    it("link, member, replacement and button label", () => {
        SdkConfig.put({
            brand: "mock",
            ...completeConfig,
        });
        render(<ExternalProfile member={mockUser} />);
        expect(screen.getByTestId("userinfo_external_profile_container")).toBeInTheDocument();
        expect(screen.getByTestId("userinfo_external_profile_label")).toHaveTextContent(
            completeConfig.external_user_profile!.buttonLabel!,
        );
        expect(screen.getByTestId("userinfo_external_profile_link").getAttribute("href")).toBe(
            completeConfig.external_user_profile!.link.replace("rawDisplayName", "MockJagger"),
        );
    });
});
