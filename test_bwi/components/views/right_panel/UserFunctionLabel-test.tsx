// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";

import { EventType, RoomStateEvent, User } from "matrix-js-sdk/src/matrix";
import * as TestUtils from "../../../../test/test-utils";

import { act, cleanup, fireEvent, render, screen } from "@testing-library/react";
import { UserFunctionLabel } from "../../../../src/bwi/components/UserFunctionLabel";
import * as UserFunctionLabelHelper from "../../../../src/bwi/helper/events/UserFunctionLabel";
import { getBaseClient, getClientUserMocks } from "../../../mock/MockClient";
import { generateRoomId, mockRoomWithEvents } from "../../../mock/MockRoom";
import { userToRoomMember } from "../../../test-utils/user";

describe("UserFunctionLabel", () => {
    const roomId = generateRoomId();
    const mockLabel = "testlabel";
    const member = userToRoomMember(new User("test@localhost"));
    const event = TestUtils.mkEvent({
        type: EventType.BwiUserFunctionLabels,
        event: true,
        content: {
            [member.userId]: mockLabel,
        },
        room: roomId,
        user: member.userId,
        skey: "",
    });
    const powerLevels = TestUtils.mkEvent({
        type: EventType.RoomPowerLevels,
        event: true,
        content: {
            events: {
                [EventType.BwiUserFunctionLabels]: 100,
            },
        },
        room: roomId,
        user: member.userId,
    });

    getBaseClient({
        ...getClientUserMocks(),
        sendStateEvent: () => Promise.resolve(),
    });
    const room = mockRoomWithEvents({
        partials: {
            roomId,
            name: "1231241241",
            currentState: {
                maySendStateEvent: jest.fn().mockReturnValue(true),
            },
        },
        events: [event, powerLevels],
    });

    const unsetUserFunctionLabelSpy = jest.spyOn(UserFunctionLabelHelper, "unsetUserFunctionLabel");
    const setUserFunctionLabelSpy = jest.spyOn(UserFunctionLabelHelper, "setUserFunctionLabel");

    beforeEach(() => {
        room.currentState.setStateEvents([event, powerLevels]);
        unsetUserFunctionLabelSpy.mockClear();
        setUserFunctionLabelSpy.mockClear();
        room.currentState.maySendStateEvent.mockClear();
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.restoreAllMocks();
        jest.resetModules();
    });

    function enterEditMode() {
        const editButton = screen.getByRole("button", { name: "Edit" });
        act(() => {
            fireEvent.click(editButton);
        });

        const inputEl = screen.getByRole("textbox", { name: "Function label" });

        return {
            node: inputEl,
            type(value: string) {
                act(() => {
                    fireEvent.change(inputEl, { target: { value } });
                });
            },
            async clickSave() {
                await act(async () => {
                    fireEvent.click(screen.getByRole("button", { name: "Apply" }));
                });
            },
            async clickCancel() {
                await act(async () => {
                    fireEvent.click(screen.getByRole("button", { name: "Cancel" }));
                });
            },
        };
    }

    function assertPlaceholder() {
        expect(screen.queryByText("+ Add user function label")).toBeInTheDocument();
    }

    it('should properly subscribe to "RoomState.events" and unsubscribe when unmounting', () => {
        const eventListenerSpy = jest.spyOn(room.currentState, "on");
        const removeListenerSpy = jest.spyOn(room.currentState, "off");

        render(<UserFunctionLabel room={room} member={member} />);

        expect(eventListenerSpy).toHaveBeenCalledWith("RoomState.events", expect.any(Function));
        const eventListenerFunction = eventListenerSpy.mock.calls[0][1];

        cleanup();

        expect(removeListenerSpy).toHaveBeenCalledWith("RoomState.events", eventListenerFunction);
    });

    it("label should be shown to user that isnt able to edit it", () => {
        room.currentState.maySendStateEvent.mockReturnValueOnce(false);

        render(<UserFunctionLabel room={room} member={member} />);

        expect(screen.queryByText(mockLabel)).toBeInTheDocument();
        expect(screen.queryByText("Edit")).not.toBeInTheDocument();
    });

    it("user with edit privs should be able to see and edit the current label", async () => {
        render(<UserFunctionLabel room={room} member={member} />);

        enterEditMode();

        expect(screen.queryByDisplayValue(mockLabel)).toBeInTheDocument();
    });

    it('label should default to "add function label" if none is set', () => {
        jest.spyOn(event, "getContent").mockReturnValueOnce({});

        render(<UserFunctionLabel room={room} member={member} />);

        assertPlaceholder();
    });

    it("aborting an edit should retain the previous value", async () => {
        render(<UserFunctionLabel room={room} member={member} />);

        const input = enterEditMode();
        input.type("new label");
        await input.clickCancel();

        expect(screen.queryByText(mockLabel)).toBeInTheDocument();
        expect(setUserFunctionLabelSpy).not.toHaveBeenCalled();
    });

    it("saving an edit should immediately reflect the new value and call setUserFunctionLabel", async () => {
        render(<UserFunctionLabel room={room} member={member} />);

        const input = enterEditMode();
        input.type("new label");
        await input.clickSave();

        expect(screen.queryByText("new label")).toBeInTheDocument();
        expect(setUserFunctionLabelSpy).toHaveBeenCalledTimes(1);
        expect(setUserFunctionLabelSpy).toHaveBeenCalledWith(room, member, "new label");
    });

    it("saving an edit and rejecting the save should restore the old value", async () => {
        setUserFunctionLabelSpy.mockRejectedValueOnce(false);

        render(<UserFunctionLabel room={room} member={member} />);

        const input = enterEditMode();
        input.type("new label");
        await input.clickSave();

        expect(screen.queryByText(mockLabel)).toBeInTheDocument();
    });

    it("rejecting a deletion should restore the previous value", async () => {
        unsetUserFunctionLabelSpy.mockRejectedValueOnce(false);

        render(<UserFunctionLabel room={room} member={member} />);

        await act(async () => {
            fireEvent.click(screen.getByRole("button", { name: "Delete" }));
        });

        expect(screen.queryByText(mockLabel)).toBeInTheDocument();
    });

    it("deleting a value should immediately reflect the deletion", async () => {
        render(<UserFunctionLabel room={room} member={member} />);

        await act(async () => {
            fireEvent.click(screen.getByRole("button", { name: "Delete" }));
        });

        assertPlaceholder();
    });

    it("if current roomstate changes we should update the label accordingly", async () => {
        const newLabel = "new label";
        const newEvent = TestUtils.mkEvent({
            type: EventType.BwiUserFunctionLabels,
            event: true,
            content: {
                [member.userId]: newLabel,
            },
            room: room.roomId,
            user: member.userId,
        });

        render(<UserFunctionLabel room={room} member={member} />);

        await act(async () => {
            room.currentState.emit(RoomStateEvent.Events, newEvent, undefined!, undefined!);
        });

        expect(screen.getByText(newLabel)).toBeInTheDocument();
    });

    it("if current roomstate changes and the event isnt ours we should discard it", async () => {
        const newEvent = TestUtils.mkEvent({
            type: "wrongeventtype",
            event: true,
            content: {
                [member.userId]: "testNewLabel",
            },
            room: room.roomId,
            user: member.userId,
        });

        render(<UserFunctionLabel room={room} member={member} />);

        await act(async () => {
            room.currentState.emit(RoomStateEvent.Events, newEvent, undefined!, undefined!);
        });

        expect(screen.getByText(mockLabel)).toBeInTheDocument();
    });

    it("without defined powerlevel an admin should be able to edit label", async () => {
        const newPowerLevels = TestUtils.mkEvent({
            type: EventType.RoomPowerLevels,
            event: true,
            content: {
                events: {},
            },
            room: room.roomId,
            user: member.userId,
        });

        room.currentState.setStateEvents([event, newPowerLevels]);

        const memberSpy = jest.spyOn(room, "getMember").mockReturnValue({
            powerLevel: 100,
        } as any);

        render(<UserFunctionLabel room={room} member={member} />);

        expect(screen.queryByRole("button", { name: "Edit" })).toBeInTheDocument();

        memberSpy.mockClear();
    });

    it("without defined powerlevel non admin shouldnt be able to edit label", () => {
        const newPowerLevels = TestUtils.mkEvent({
            type: EventType.RoomPowerLevels,
            event: true,
            content: {
                events: {},
            },
            room: room.roomId,
            user: member.userId,
            skey: "",
        });

        room.currentState.setStateEvents([event, newPowerLevels]);

        const memberSpy = jest.spyOn(room, "getMember").mockReturnValue({
            powerLevel: 1,
        } as any);

        render(<UserFunctionLabel room={room} member={member} />);

        expect(screen.queryByRole("button", { name: "Edit" })).not.toBeInTheDocument();

        memberSpy.mockClear();
    });

    it("saving an empty value should unset instead of saving it", async () => {
        render(<UserFunctionLabel room={room} member={member} />);

        const input = enterEditMode();
        input.type("");
        await input.clickSave();

        expect(unsetUserFunctionLabelSpy).toHaveBeenCalled();
        expect(setUserFunctionLabelSpy).not.toHaveBeenCalled();
    });

    it("should map escape / enter keys to abort / onSave", async () => {
        render(<UserFunctionLabel room={room} member={member} />);

        let input = enterEditMode();
        input.type("");

        await act(async () => {
            fireEvent.keyDown(input.node, { key: "Escape" });
        });

        expect(screen.queryByText(mockLabel)).toBeInTheDocument();
        expect(setUserFunctionLabelSpy).not.toHaveBeenCalled();

        const newLabel = "newlabeltest";

        input = enterEditMode();
        input.type(newLabel);

        await act(async () => {
            fireEvent.keyDown(input.node, { key: "Enter" });
        });

        expect(screen.queryByText(newLabel)).toBeInTheDocument();
        expect(setUserFunctionLabelSpy).toHaveBeenCalled();
    });
});
