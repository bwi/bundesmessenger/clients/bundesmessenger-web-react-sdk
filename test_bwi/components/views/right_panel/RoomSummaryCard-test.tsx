// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, render, screen } from "@testing-library/react";
import { PendingEventOrdering, Room } from "matrix-js-sdk/src/matrix";
import { describe } from "node:test";
import React from "react";
import { IRoomState } from "../../../../src/components/structures/RoomView";
import RoomSummaryCard from "../../../../src/components/views/right_panel/RoomSummaryCard";
import MatrixClientContext from "../../../../src/contexts/MatrixClientContext";
import RoomContext from "../../../../src/contexts/RoomContext";
import DMRoomMap from "../../../../src/utils/DMRoomMap";
import { E2EStatus } from "../../../../src/utils/ShieldUtils";
import { getBaseClient, getClientUserMocks } from "../../../mock/MockClient";
import { mockRoomWithEvents } from "../../../mock/MockRoom";

describe("<RoomSummaryCard />", () => {
    // eslint-disable-next-line prefer-const
    let room: Room;

    const client = getBaseClient({
        ...getClientUserMocks(),
        isRoomEncrypted() {
            return false;
        },
        getRoom() {
            return room;
        },
        store: {
            async getPendingEvents() {
                return [];
            },
        },
        getEventMapper() {
            return {};
        },
    });

    room = mockRoomWithEvents(
        {
            partials: {
                getCanonicalAlias() {
                    return "Alias schmalias";
                },
            },
            opts: {
                pendingEventOrdering: PendingEventOrdering.Detached,
            },
        },
        client,
    );

    DMRoomMap.makeShared(client);

    it("should hide alias", async () => {
        await act(async () => {
            render(
                <MatrixClientContext.Provider value={client}>
                    <RoomContext.Provider value={{ room, e2eStatus: E2EStatus.Normal } as unknown as IRoomState}>
                        <RoomSummaryCard permalinkCreator={undefined!} room={room} />
                    </RoomContext.Provider>
                </MatrixClientContext.Provider>,
            );
        });

        expect(screen.queryByText("Alias schmalias")).not.toBeInTheDocument();
    });
});
