// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import UserProfileSettings from "../../../../src/components/views/settings/UserProfileSettings";
import {
    getMockClientWithEventEmitter,
    mockClientMethodsUser,
    unmockClientPeg,
} from "../../../../test/test-utils/client";
import { OwnProfileStore } from "../../../../src/stores/OwnProfileStore";
import MatrixClientContext from "../../../../src/contexts/MatrixClientContext";

describe("UserProfileSettings", () => {
    const userId = "@user:server.org";
    const client = getMockClientWithEventEmitter({
        ...mockClientMethodsUser(userId),
        getMediaConfig: jest.fn().mockResolvedValue({}),
    });

    afterEach(() => {
        cleanup();
    });

    afterAll(() => {
        unmockClientPeg();
    });

    it("should show the share link option", () => {
        const spy = jest.spyOn(OwnProfileStore.prototype, "displayName", "get").mockReturnValue("Test user");

        render(
            <MatrixClientContext.Provider value={client}>
                <UserProfileSettings canSetAvatar={false} canSetDisplayName={false} />
            </MatrixClientContext.Provider>,
        );
        screen.debug();
        expect(screen.queryByText("Share link to profile")).toBeInTheDocument();

        spy.mockRestore();
    });
});
