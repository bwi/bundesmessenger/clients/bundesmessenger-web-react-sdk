/*
Copyright 2022 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { fireEvent, render, RenderResult, screen } from "@testing-library/react";
import { MatrixClient, EventType, IContent, Room, User } from "matrix-js-sdk/src/matrix";
import React from "react";

import RolesRoomSettingsTab from "../../../../../../src/components/views/settings/tabs/room/RolesRoomSettingsTab";
import { MatrixClientPeg } from "../../../../../../src/MatrixClientPeg";
import { ElementCall } from "../../../../../../src/models/Call";
import SettingsStore from "../../../../../../src/settings/SettingsStore";
import { mkStubRoom, stubClient } from "../../../../../../test/test-utils";
import { userToRoomMember } from "../../../../../test-utils/user";
import MatrixClientContext from "../../../../../../src/contexts/MatrixClientContext";
import { resetPowerLevels } from "../../../../../../src/bwi/functions";
import SdkConfig from "../../../../../../src/SdkConfig";

describe("RolesRoomSettingsTab", () => {
    const roomId = "!room:example.com";
    let cli: MatrixClient;
    let room: Room;

    const renderTab = (rm: Room = room): RenderResult => {
        return render(
            <MatrixClientContext.Provider value={cli}>
                <RolesRoomSettingsTab room={rm} />
            </MatrixClientContext.Provider>,
        );
    };

    beforeEach(() => {
        stubClient();
        cli = MatrixClientPeg.safeGet();
        room = mkStubRoom(roomId, "test room", cli);
    });

    // Override for matrix-react-sdk/test/components/views/settings/tabs/room/RolesRoomSettingsTab-test.tsx >
    // RolesRoomSettingsTab › Element Call
    describe("Element Call", () => {
        const setGroupCallsEnabled = (val: boolean): void => {
            jest.spyOn(SettingsStore, "getValue").mockImplementation((name: string) => {
                if (name === "feature_group_calls") return val;
            });
        };

        const getStartCallSelect = (tab: RenderResult) => {
            return tab.container.querySelector("select[label='Start Element Call calls']");
        };

        const getStartCallSelectedOption = (tab: RenderResult) => {
            return tab.container.querySelector("select[label='Start Element Call calls'] option:checked");
        };

        const getJoinCallSelect = (tab: RenderResult) => {
            return tab.container.querySelector("select[label='Join Element Call calls']");
        };

        const getJoinCallSelectedOption = (tab: RenderResult) => {
            return tab.container.querySelector("select[label='Join Element Call calls'] option:checked");
        };

        describe("Element Call enabled", () => {
            beforeAll(() => {
                SdkConfig.put({
                    element_call: {
                        url: "https://mockety.mock.io",
                        use_exclusively: true,
                        participant_limit: 8,
                        brand: "Element Call",
                    },
                });
            });

            afterAll(() => {
                SdkConfig.reset();
            });

            beforeEach(() => {
                setGroupCallsEnabled(true);
            });

            describe("Join Element calls", () => {
                it("defaults to moderator for joining calls", () => {
                    expect(getJoinCallSelectedOption(renderTab())?.textContent).toBe("Moderator");
                });

                it("can change joining calls power level", () => {
                    const tab = renderTab();

                    console.log("fired event");
                    fireEvent.change(getJoinCallSelect(tab)!, {
                        target: { value: 0 },
                    });

                    expect(getJoinCallSelectedOption(tab)?.textContent).toBe("Member");
                    expect(cli.sendStateEvent).toHaveBeenCalledWith(roomId, EventType.RoomPowerLevels, {
                        events: {
                            [ElementCall.MEMBER_EVENT_TYPE.name]: 0,
                        },
                    });
                });
            });

            describe("Start Element calls", () => {
                it("defaults to moderator for starting calls", () => {
                    expect(getStartCallSelectedOption(renderTab())?.textContent).toBe("Moderator");
                    screen.debug();
                });

                it("can change starting calls power level", () => {
                    const tab = renderTab();

                    fireEvent.change(getStartCallSelect(tab)!, {
                        target: { value: 0 },
                    });

                    expect(getStartCallSelectedOption(tab)?.textContent).toBe("Member");
                    expect(cli.sendStateEvent).toHaveBeenCalledWith(roomId, EventType.RoomPowerLevels, {
                        events: {
                            [ElementCall.CALL_EVENT_TYPE.name]: 0,
                        },
                    });
                });
            });
        });

        it("hides when group calls disabled", () => {
            setGroupCallsEnabled(false);

            const tab = renderTab();

            expect(getStartCallSelect(tab)).toBeFalsy();
            expect(getStartCallSelectedOption(tab)).toBeFalsy();

            expect(getJoinCallSelect(tab)).toBeFalsy();
            expect(getJoinCallSelectedOption(tab)).toBeFalsy();
        });
    });

    describe("Privileged Users", () => {
        it("should not be able change my powerlevel as last remaining admin", () => {
            const alice = userToRoomMember(new User("@alice:server.org"));
            alice.powerLevel = 100;
            const bob = userToRoomMember(new User("@bob:server.org"));
            bob.powerLevel = 50;

            const room = new Room("!my_room:server.org", cli, alice.userId);

            jest.spyOn(room, "getMembers").mockReturnValue([alice, bob]);

            renderTab(room);

            expect(screen.getByDisplayValue("Admin")).toHaveAttribute("disabled", "");
        });
    });

    describe("reset power levels", () => {
        it("should reset bwi override to bwi default", () => {
            const plContent = {
                events: {
                    [EventType.RoomAvatar]: 100,
                    [EventType.RoomRedaction]: 100,
                },
            } as IContent;

            resetPowerLevels(
                {
                    [EventType.RoomAvatar]: { bwiDefault: 50 },
                    [EventType.RoomRedaction]: {},
                },
                plContent,
                (key, content, value) => {
                    content.events[key.slice("event_levels_".length)] = value;
                },
            );

            expect(plContent.events).toEqual({
                [EventType.RoomAvatar]: 50,
                [EventType.RoomRedaction]: 100,
            });
        });
    });
});
