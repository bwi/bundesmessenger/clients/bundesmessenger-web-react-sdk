// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { RefObject } from "react";
import ResizeHandle from "../../../../src/components/views/elements/ResizeHandle";
import { Resizer, CollapseDistributor } from "../../../../src/resizer";
import { ICollapseConfig } from "../../../../src/resizer/distributors/collapse";
import { act, fireEvent, render, screen } from "@testing-library/react";
import UIStore from "../../../../src/stores/UIStore";

function TestContainer({
    handleRef,
    containerRef,
}: {
    handleRef: RefObject<HTMLDivElement>;
    containerRef: RefObject<HTMLDivElement>;
}) {
    return (
        <div>
            <div className="container" ref={containerRef} style={{ width: "200px" }} />
            <ResizeHandle passRef={handleRef} />
        </div>
    );
}

describe("ResizeHandle", () => {
    describe("keyboard controlled", () => {
        const handleRef = React.createRef<HTMLDivElement>();
        const containerRef = React.createRef<HTMLDivElement>();

        beforeEach(() => {
            render(<TestContainer handleRef={handleRef} containerRef={containerRef} />);

            // we need to simulate offsetWidth because jsDom does not calculate it
            jest.spyOn(containerRef.current!, "offsetWidth", "get").mockImplementation(() => {
                return +(containerRef.current?.style.width.slice(0, -2) || 0);
            });

            const resizer = new Resizer(
                containerRef.current!,
                CollapseDistributor as any,
                {
                    toggleSize: 50,
                    onCollapsed: (isCollapsed) => {
                        if (isCollapsed) {
                            containerRef.current!.setAttribute("data-collapsed", "true");
                        } else {
                            containerRef.current!.removeAttribute("data-collapsed");
                        }
                    },
                    onResized: (_size) => {},
                    onResizeStart: () => {},
                    onResizeStop: () => {},
                    isItemCollapsed: () => {
                        return containerRef.current?.dataset.collapsed === "true";
                    },
                    handler: handleRef.current,
                } as ICollapseConfig,
            );

            resizer.attach();

            act(() => {
                fireEvent.focusIn(screen.getByRole("separator"));
            });
        });

        it("should move the container with the arrow keys", () => {
            const handle = screen.getByRole("separator");

            // When: Right arrow click
            act(() => {
                fireEvent.keyDown(handle, { key: "ArrowRight" });
            });

            // Expect: Container to be bigger
            expect(containerRef.current?.style.width).toBe("300px");

            // When: Left arrow click
            act(() => {
                fireEvent.keyDown(handle, { key: "ArrowLeft" });
            });

            // Expect: Container to be smaller
            expect(containerRef.current?.style.width).toBe("200px");
        });

        it("should toggle the container from collapsed to last position and vice versa on 'Enter'", () => {
            // When: Enter pressed
            act(() => {
                fireEvent.keyDown(screen.getByRole("separator"), { key: "Enter" });
            });

            // Expect: Sidebar to be collapsed
            expect(containerRef.current).toHaveAttribute("data-collapsed", "true");

            // When: Enter pressed again
            act(() => {
                fireEvent.keyDown(screen.getByRole("separator"), { key: "Enter" });
            });

            // Expect: Siderbar not to be collapsed
            expect(containerRef.current).not.toHaveAttribute("data-collapsed");

            // info: If enter brings back the panel from the collapsed state, the container should have the same
            // width as before (200px in this case). But testing for styles is painful in jestDom so we skip it here
        });

        it("should collapse the container on 'Home", () => {
            // When: Home pressed
            act(() => {
                fireEvent.keyDown(screen.getByRole("separator"), { key: "Home" });
            });

            expect(containerRef.current).toHaveAttribute("data-collapsed", "true");
        });

        it("should set the container to the maximum size on 'End", () => {
            UIStore.instance.windowWidth = 1000;

            // When: Home pressed
            act(() => {
                fireEvent.keyDown(screen.getByRole("separator"), { key: "End" });
            });

            expect(containerRef.current?.style.width).toBe("1000px");
        });
    });

    it("should have additional a11y attributes", () => {
        render(<ResizeHandle />);
        const handle = screen.getByRole("separator");
        expect(handle).toHaveAttribute("tabIndex", "0");
        expect(handle).toHaveAttribute("role", "separator");
    });
});
