// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, fireEvent, render, screen } from "@testing-library/react";
import React from "react";
import SdkConfig from "../../../../src/SdkConfig";
import LanguageDropdown from "../../../../src/components/views/elements/LanguageDropdown";

/**
 * TIPP: Languages are mocked in /test/setup/setupLanguage.ts
 */

describe("LanguageDropdown", () => {
    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    function assertLanguageOptions(options: string[]) {
        // Open DropDown
        act(() => {
            fireEvent.click(screen.getByLabelText("Language Dropdown"));
        });

        expect(screen.queryAllByRole("option", { name: (query) => options.includes(query) })).toHaveLength(
            options.length,
        );
    }

    it("should render all available languages without config set", async () => {
        await act(async () => {
            render(<LanguageDropdown onOptionChange={jest.fn()} />);
        });

        assertLanguageOptions(["English", "Deutsch", "latviešu"]);
    });

    it("should only render languages defined in sdk config", async () => {
        SdkConfig.add({
            enabledLanguages: ["lv"],
        });

        await act(async () => {
            render(<LanguageDropdown onOptionChange={jest.fn()} />);
        });

        assertLanguageOptions(["latviešu"]);
    });
});
