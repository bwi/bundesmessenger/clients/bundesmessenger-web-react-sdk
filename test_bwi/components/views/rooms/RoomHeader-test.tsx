// Copyright 2022 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import { fireEvent, render, screen } from "@testing-library/react";
import { mocked, Mocked } from "jest-mock";
import {
    MatrixClient,
    PendingEventOrdering,
    ISendEventResponse,
    MatrixEvent,
    Room,
    RoomMember,
    RoomStateEvent,
    RoomType,
    IContent,
} from "matrix-js-sdk/src/matrix";
import React from "react";
import { IRoomState } from "../../../../src/components/structures/RoomView";
import RoomHeader, { IProps as RoomHeaderProps } from "../../../../src/components/views/rooms/LegacyRoomHeader";
import { SearchScope } from "../../../../src/Searching";
import RoomContext from "../../../../src/contexts/RoomContext";
import { MatrixClientPeg } from "../../../../src/MatrixClientPeg";
import MediaDeviceHandler, { MediaDeviceKindEnum } from "../../../../src/MediaDeviceHandler";
import SettingsStore from "../../../../src/settings/SettingsStore";
import { CallStore } from "../../../../src/stores/CallStore";
import WidgetStore from "../../../../src/stores/WidgetStore";
import DMRoomMap from "../../../../src/utils/DMRoomMap";
import { E2EStatus } from "../../../../src/utils/ShieldUtils";
import {
    mkEvent,
    mkMembership,
    mkRoomMember,
    mockPlatformPeg,
    setupAsyncStoreWithClient,
    stubClient,
} from "../../../../test/test-utils";

function mkCreationEvent(roomId: string, userId: string): MatrixEvent {
    return mkEvent({
        event: true,
        type: "m.room.create",
        room: roomId,
        user: userId,
        content: {
            creator: userId,
            room_version: "5",
            predecessor: {
                room_id: "!prevroom",
                event_id: "$someevent",
            },
        },
    });
}

describe("BWI: RoomHeader (React Testing Library)", () => {
    let client: Mocked<MatrixClient>;
    let room: Room;
    let alice: RoomMember;

    beforeEach(async () => {
        mockPlatformPeg({ supportsJitsiScreensharing: () => true });

        stubClient();
        client = mocked(MatrixClientPeg.safeGet());
        client.getUserId.mockReturnValue("@alice:example.org");
        client.credentials.userId = "@alice:example.org";

        DMRoomMap.makeShared(client);

        room = new Room("!1:example.org", client, "@alice:example.org", {
            pendingEventOrdering: PendingEventOrdering.Detached,
        });
        alice = mkRoomMember(room.roomId, "@alice:example.org");
        room.currentState.setStateEvents([
            mkCreationEvent(room.roomId, "@alice:example.org"),
            mkMembership({
                event: true,
                room: room.roomId,
                mship: "join",
                user: "@alice:example.org",
                name: "Alice",
                target: alice,
            }),
        ]);
        room.recalculate();

        client.getRoom.mockImplementation((roomId) => (roomId === room.roomId ? room : null));
        client.getRooms.mockReturnValue([room]);
        client.reEmitter.reEmit(room, [RoomStateEvent.Events]);
        client.sendStateEvent.mockImplementation(async (roomId, eventType, content, stateKey = "") => {
            if (roomId !== room.roomId) throw new Error("Unknown room");
            const event = mkEvent({
                event: true,
                type: eventType,
                room: roomId,
                user: alice.userId,
                skey: stateKey,
                content: content as IContent,
            });
            room.addLiveEvents([event]);
            return { event_id: event.getId() } as ISendEventResponse;
        });

        client.getRoom.mockImplementation((roomId) => (roomId === room.roomId ? room : null));
        client.getRooms.mockReturnValue([room]);
        client.reEmitter.reEmit(room, [RoomStateEvent.Events]);

        await Promise.all(
            [CallStore.instance, WidgetStore.instance].map((store) => setupAsyncStoreWithClient(store, client)),
        );

        jest.spyOn(MediaDeviceHandler, "getDevices").mockResolvedValue({
            [MediaDeviceKindEnum.AudioInput]: [],
            [MediaDeviceKindEnum.VideoInput]: [],
            [MediaDeviceKindEnum.AudioOutput]: [],
        });
    });

    const mockEnabledSettings = (settings: string[]) => {
        jest.spyOn(SettingsStore, "getValue").mockImplementation((settingName) => settings.includes(settingName));
    };

    const mockRoomType = (type: string) => {
        jest.spyOn(room, "getType").mockReturnValue(type);
    };

    const renderHeader = (props: Partial<RoomHeaderProps> = {}, roomContext: Partial<IRoomState> = {}) => {
        render(
            <RoomContext.Provider value={{ ...roomContext, room } as IRoomState}>
                <RoomHeader
                    room={room}
                    inRoom={true}
                    onSearchClick={() => {}}
                    onInviteClick={null}
                    onForgetClick={() => {}}
                    onAppsClick={() => {}}
                    e2eStatus={E2EStatus.Normal}
                    appsShown={true}
                    searchInfo={{
                        searchId: 1,
                        term: "",
                        scope: SearchScope.Room,
                        count: 0,
                        promise: Promise.resolve({
                            highlights: [],
                            results: [],
                        }),
                    }}
                    viewingCall={false}
                    activeCall={null}
                    {...props}
                />
            </RoomContext.Provider>,
        );
    };

    // Test override for matrix-react-sdk/test/components/views/rooms/RoomHeader-test.tsx "RoomHeader" > "shows an invite button in video rooms"
    it("shows an invite button in video rooms", () => {
        mockEnabledSettings(["feature_video_rooms", "feature_element_call_video_rooms"]);
        mockRoomType(RoomType.UnstableCall);

        const onInviteClick = jest.fn();
        renderHeader({ onInviteClick, viewingCall: true });

        fireEvent.click(screen.getByRole("button", { name: /invite/i }));
        expect(onInviteClick).toHaveBeenCalled();
    });
});
