// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { render, screen } from "@testing-library/react";
import { MatrixClient, Room } from "matrix-js-sdk/src/matrix";
import React from "react";

import { IRoomState } from "../../../../src/components/structures/RoomView";
import NewRoomIntro from "../../../../src/components/views/rooms/NewRoomIntro";
import MatrixClientContext from "../../../../src/contexts/MatrixClientContext";
import RoomContext from "../../../../src/contexts/RoomContext";
import { LocalRoom } from "../../../../src/models/LocalRoom";
import DMRoomMap from "../../../../src/utils/DMRoomMap";
import { filterConsole, stubClient } from "../../../../test/test-utils";

const renderNewRoomIntro = (client: MatrixClient, room: Room | LocalRoom) => {
    render(
        <MatrixClientContext.Provider value={client}>
            <RoomContext.Provider value={{ room, roomId: room.roomId } as unknown as IRoomState}>
                <NewRoomIntro />
            </RoomContext.Provider>
        </MatrixClientContext.Provider>,
    );
};

describe("NewRoomIntro", () => {
    let client: MatrixClient;
    const roomId = "!room:example.com";
    const userId = "@user:example.com";

    filterConsole("Room !room:example.com does not have an m.room.create event");

    beforeAll(() => {
        client = stubClient();
        DMRoomMap.makeShared(client);
    });

    describe("for a DM Room", () => {
        beforeEach(() => {
            jest.spyOn(DMRoomMap.shared(), "getUserIdForRoomId").mockReturnValue(userId);
            const room = new Room(roomId, client, client.getUserId()!);
            jest.spyOn(room, "getJoinedMemberCount").mockReturnValue(2);
            room.name = "test_room";
            renderNewRoomIntro(client, room);
        });

        // BWI(MESSENGER-4728) Remove caption for direct messages
        it("should hide caption for direct messages", () => {
            const expected = `This is the beginning of your direct message history with test_room.`;
            screen.getByText((id, element) => element?.tagName === "SPAN" && element?.textContent === expected);

            expect(
                screen.queryByText(
                    "Only the two of you are in this conversation, unless either of you invites anyone to join.",
                ),
            ).not.toBeInTheDocument();
        });
    });
});
