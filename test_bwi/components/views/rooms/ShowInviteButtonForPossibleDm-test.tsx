// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixClient, Room } from "matrix-js-sdk/src/matrix";
import { showInviteButtonForPossibleDm } from "../../../../src/bwi/components/views/rooms/SingleParticipantDms";
import { BwiFeature } from "../../../../src/bwi/settings/BwiFeature";
import { MatrixClientPeg } from "../../../../src/MatrixClientPeg";
import DMRoomMap from "../../../../src/utils/DMRoomMap";
import * as testUtils from "../../../../test/test-utils";
import { generateRoomId } from "../../../mock/MockRoom";
import { mockFeatureFlag } from "../../../mock/MockSettingsStore";

describe("showInviteButtonForPossibleDm, all possible scenarios", () => {
    let client: MatrixClient;
    let room: Room;

    beforeAll(() => {
        testUtils.stubClient();
        client = MatrixClientPeg.safeGet();
        room = client.getRoom(generateRoomId())!;
        DMRoomMap.makeShared(client);
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    describe("feature flag off, room is no Dm", () => {
        it("should return true", () => {
            const featureFlag = mockFeatureFlag(BwiFeature.SingleParticipantDms, false);
            expect(showInviteButtonForPossibleDm(room.roomId)).toBe(true);
            featureFlag.mockRestore();
        });
    });

    describe("feature flag on, room is no Dm", () => {
        it("should return true", () => {
            const featureFlag = mockFeatureFlag(BwiFeature.SingleParticipantDms, true);
            expect(showInviteButtonForPossibleDm(room.roomId)).toBe(true);
            featureFlag.mockRestore();
        });
    });

    describe("feature flag off, room is a Dm", () => {
        it("should return true", () => {
            const featureFlag = mockFeatureFlag(BwiFeature.SingleParticipantDms, false);
            const spy = jest.spyOn(DMRoomMap.prototype, "getUserIdForRoomId").mockReturnValue("YesIAmADm");
            expect(showInviteButtonForPossibleDm(room.roomId)).toBe(true);
            featureFlag.mockRestore();
            spy.mockClear();
        });
    });

    describe("feature flag on, room is a Dm", () => {
        it("should return true", () => {
            const featureFlag = mockFeatureFlag(BwiFeature.SingleParticipantDms, true);
            const spy = jest.spyOn(DMRoomMap.prototype, "getUserIdForRoomId").mockReturnValue("YesIAmADm");
            expect(showInviteButtonForPossibleDm(room.roomId)).toBe(false);
            featureFlag.mockRestore();
            spy.mockClear();
        });
    });
});
