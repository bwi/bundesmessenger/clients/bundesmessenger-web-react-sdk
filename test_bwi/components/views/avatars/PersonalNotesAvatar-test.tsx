// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { cleanup, fireEvent, render } from "@testing-library/react";
import { Mocked } from "jest-mock";
import { IEvent, MatrixClient, MatrixEvent, Room } from "matrix-js-sdk/src/matrix";
import React from "react";
import { act } from "react-dom/test-utils"; // eslint-disable-line deprecate/import
import { avatarUrlForRoom } from "../../../../src/Avatar";
import { PersonalNotesAvatarUrl } from "../../../../src/bwi/components/views/avatars/PersonalNotesAvatar";
import RoomAvatar from "../../../../src/components/views/avatars/RoomAvatar";
import RoomProfileSettings from "../../../../src/components/views/room_settings/RoomProfileSettings";
import { DefaultTagID } from "../../../../src/stores/room-list/models";
import DMRoomMap from "../../../../src/utils/DMRoomMap";
import { getMockClientWithEventEmitter, mockClientMethodsUser, unmockClientPeg } from "../../../../test/test-utils";
import { mockRoomWithEvents } from "../../../mock/MockRoom";
import { mockClientStore } from "../../../test-utils/client";
import * as TestUtils from "../../../../test/test-utils";
import { NOTES_DATA } from "../../../../src/bwi/functions";

describe("PersonalNotesAvatar", () => {
    let client: Mocked<MatrixClient>;
    beforeAll(() => {
        client = getMockClientWithEventEmitter({
            ...mockClientMethodsUser(),
            ...mockClientStore(),
            getEventMapper: () => (opts: Partial<IEvent>) => new MatrixEvent(opts),
            getRoom: jest.fn(),
        });
        client.getAccountData.mockReturnValue({
            getContent: () => ({}) as any,
        } as MatrixEvent);
        DMRoomMap.makeShared(client);
    });

    afterEach(cleanup);

    afterAll(() => {
        unmockClientPeg();
    });

    describe("Personal Notes", () => {
        let room: Room;

        beforeAll(() => {
            room = mockRoomWithEvents(
                {
                    partials: {
                        tags: { [DefaultTagID.Favourite]: {} },
                        name: "Personal Notes Room",
                    },
                },
                client,
            );

            const event = TestUtils.mkEvent({
                type: NOTES_DATA,
                event: true,
                content: {
                    room_id: room.roomId,
                },
                user: "@userId:matrix.rog",
                room: "",
            });
            client.getRoom.mockImplementation((roomid) => (roomid === room.roomId ? room : null));
            client.getAccountData.mockImplementation((eventType: string) => event);
        });

        afterAll(() => {
            client.getRoom.mockRestore();
        });

        it("should render the avatar as an image if no click handler supplied", async () => {
            const { container } = render(<RoomAvatar room={room} />);

            const avatarEl = container.querySelector("img");
            expect(avatarEl?.getAttribute("src")).toBe(PersonalNotesAvatarUrl);
        });

        it("should render the avatar as a button if click handler supplied", async () => {
            const onClick = jest.fn();

            const { container } = render(<RoomAvatar room={room} onClick={onClick} />);

            const avatarEl = container.querySelector("img");
            expect(avatarEl).toBeTruthy();
            expect(avatarEl?.getAttribute("src")).toBe(PersonalNotesAvatarUrl);

            act(() => {
                fireEvent.click(avatarEl!);
            });

            expect(onClick).toHaveBeenCalled();
        });

        it("avatarUrlForRoom should return the personal notes room avatar", () => {
            const result = avatarUrlForRoom(room, 24, 24);

            expect(result).toEqual(PersonalNotesAvatarUrl);
        });

        it("should be unable to change topic and avatar", () => {
            const roomState = room.currentState;
            jest.spyOn(roomState, "maySendEvent").mockReturnValueOnce(true);
            jest.spyOn(roomState, "getStateEvents").mockImplementationOnce(
                () =>
                    ({
                        getContent: () => ({}) as any,
                    }) as MatrixEvent,
            );

            const { getByLabelText } = render(<RoomProfileSettings roomId={room.roomId} />);

            const topicEl = getByLabelText("Room Topic") as HTMLTextAreaElement;
            expect(topicEl).toBeDisabled();
        });
    });

    // FIXME: Currently we add some extra tests for the default room behaviour, because NV does'nt have some.
    // When they create some tests, we should remove the tests below
    describe("Default Room", () => {
        let room: Room;
        beforeAll(() => {
            room = mockRoomWithEvents(
                {
                    partials: {
                        name: "Some personal Room",
                    },
                },
                client,
            );
            client.getRoom.mockImplementation((roomid) => (roomid === room.roomId ? room : null));
        });

        afterAll(() => {
            client.getRoom.mockRestore();
        });

        it("should render the regular avatar", async () => {
            const { container } = render(<RoomProfileSettings roomId={room.roomId} />);

            const avatarEl = container.querySelector("img");
            expect(avatarEl?.getAttribute("src")).not.toBe(PersonalNotesAvatarUrl);
        });

        it("avatarUrlForRoom should not return personal notes room avatar", () => {
            const result = avatarUrlForRoom(room, 24, 24);

            expect(result).not.toEqual(PersonalNotesAvatarUrl);
        });

        it("should be able to change topic and avatar", () => {
            const roomState = room.currentState;
            jest.spyOn(roomState, "maySendEvent").mockReturnValueOnce(true);
            jest.spyOn(roomState, "getStateEvents").mockImplementationOnce(
                () =>
                    ({
                        getContent: () => ({}) as any,
                    }) as MatrixEvent,
            );

            const { getByLabelText } = render(<RoomProfileSettings roomId={room.roomId} />);

            const topicEl = getByLabelText("Room Topic") as HTMLTextAreaElement;
            expect(topicEl).toBeEnabled();
        });
    });
});
