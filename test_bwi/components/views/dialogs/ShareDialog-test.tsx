// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, cleanup, fireEvent, render, screen } from "@testing-library/react";
import { EventType, MatrixEvent, MsgType, Room, User } from "matrix-js-sdk/src/matrix";
import React from "react";
import ShareDialog from "../../../../src/components/views/dialogs/ShareDialog";
import SettingsStore from "../../../../src/settings/SettingsStore";
import { UIFeature } from "../../../../src/settings/UIFeature";
import { RoomPermalinkCreator } from "../../../../src/utils/permalinks/Permalinks";
import { getMockClientWithEventEmitter, mockClientMethodsUser, unmockClientPeg } from "../../../../test/test-utils";
import { mockRoomWithEvents } from "../../../mock/MockRoom";

describe("ShareDialog", () => {
    const userId = "@alice:server.org";
    const client = getMockClientWithEventEmitter({
        ...mockClientMethodsUser(userId),
    });
    const room = mockRoomWithEvents(
        {
            partials: {
                roomId: "!room:server.org",
            },
        },
        client,
    );
    const message = new MatrixEvent({
        type: EventType.RoomMessage,
        sender: userId,
        room_id: room.roomId,
        content: {
            msgtype: MsgType.Text,
            body: "Hello",
        },
        event_id: "$alices_message",
    });
    const user = new User(userId);

    beforeAll(() => {
        Object.defineProperty(window.navigator, "clipboard", {
            value: {
                write: () => Promise.resolve(),
            },
        });
        window.ClipboardItem = class ClipboardItem {} as any;
    });

    afterEach(() => {
        cleanup();
    });
    afterAll(() => {
        unmockClientPeg();
        window.ClipboardItem = undefined as any;
        Object.assign(window.navigator, "clipboard", {
            value: undefined,
        });
    });

    async function renderComponent(target: MatrixEvent | Room | User) {
        await act(async () => {
            render(
                <ShareDialog
                    target={target as any}
                    permalinkCreator={new RoomPermalinkCreator(room)}
                    onFinished={() => {}}
                />,
            );
        });
    }

    describe("Hide checkbox", () => {
        it("should be hidden if a room is shared", async () => {
            await renderComponent(room);
            expect(screen.queryByText("Link to selected message")).not.toBeInTheDocument();
        });
        it("should be hidden if a user is shared", async () => {
            await renderComponent(user);
            expect(screen.queryByText("Link to selected message")).not.toBeInTheDocument();
        });
        it("should be hidden if a message is shared", async () => {
            await renderComponent(message);
            expect(screen.queryByText("Link to selected message")).not.toBeInTheDocument();
        });
    });

    it("should be able to copy the qr code", async () => {
        await renderComponent(room);

        const copyButton = screen.getByText("Copy QR Code");

        const writeSpy = jest.spyOn(window.navigator.clipboard, "write");
        await act(async () => {
            fireEvent.click(copyButton);
        });

        expect(writeSpy).toHaveBeenCalled();
    });

    it("should not change the ui after a base update", async () => {
        const settingsSpy = jest.spyOn(SettingsStore, "getValue").mockImplementation(
            (key) =>
                ({
                    [UIFeature.ShareQRCode]: true,
                    [UIFeature.ShareSocial]: false,
                })[key],
        );

        await renderComponent(room);

        expect(document.body).toMatchSnapshot();

        settingsSpy.mockReset();
    });
});
