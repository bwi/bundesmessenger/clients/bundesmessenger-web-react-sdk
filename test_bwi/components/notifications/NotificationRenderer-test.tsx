// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, cleanup, fireEvent, render, screen, waitFor } from "@testing-library/react";
import { mocked } from "jest-mock";
import React from "react";
import Modal from "../../../src/Modal";
import { getBwiAnalyticsDialog } from "../../../src/bwi/components/notifications/BwiAnalyticsDialog";
import {
    BwiFederationOnboarding,
    openBwiFederationOnboarding,
} from "../../../src/bwi/components/notifications/BwiFederationOnboarding";
import {
    BwiNotificationDialog as BwiNotificationDialogType,
    BwiNotifications,
} from "../../../src/bwi/components/notifications/BwiNotificationRenderer";
import { BwiNotificationTypes, NOTIFICATION_DATA } from "../../../src/bwi/components/notifications/BwiNotifications";
import { getBwiReleaseNotes } from "../../../src/bwi/components/notifications/BwiReleaseNotes";
import BwiNotificationDialog from "../../../src/bwi/components/views/dialogs/BwiNotificationDialog";
import { BWI_NOTIFICATION_SETTINGS } from "../../../src/bwi/functions";
import * as NotificationConfig from "../../../src/bwi/functions/NotificationConfig";
import { BwiNotificationManager } from "../../../src/bwi/managers/BwiNotificationManager";
import Confetti from "../../../src/effects/confetti";
import { mockClientMethodsUser, wrapInMatrixClientContext } from "../../../test/test-utils";
import { getBaseClient } from "../../mock/MockClient";
import { NothingSeenMock, getBwiAnalyticsDialogMockNonCustom, showPrivacyMock } from "../../mock/MockNotification";
import { MockUser } from "../../mock/MockUser";
import { mockClientHttp } from "../../test-utils/client";
import { CryptoApi } from "matrix-js-sdk/src/crypto-api";

// Jest can not auto mock the Confetti class
// because the important functions like 'start' are not contained in the prototype
jest.mock("../../../src/effects/confetti", () =>
    jest.fn().mockImplementation(() => {
        return {
            start: () => {},
        };
    }),
);

const getNotificationOfExpectation = (current: number, of: number) => `Notification ${current} of ${of}`;

const WrappedNotificationRenderer = wrapInMatrixClientContext(BwiNotifications);

describe("NotificationRenderer", () => {
    const client = getBaseClient({
        getKeyBackupEnabled: jest.fn(),
        ...mockClientMethodsUser(MockUser.userId),
        ...mockClientHttp(),
        setAccountData: jest.fn().mockResolvedValue({}),
        getCrypto() {
            return {
                async getActiveSessionBackupVersion() {
                    return null;
                },
            };
        },
        getHomeserverUrl() {
            return "";
        },
    });

    const http = mocked(client.http);

    afterEach(() => {
        jest.clearAllMocks();
        BwiNotificationManager.resetManager();
        cleanup();
    });

    async function setupTest(settings: {
        keyBackupEnabled: boolean;
        notificationMock: any;
        dialogs: BwiNotificationDialogType[];
    }) {
        jest.spyOn(client, "getCrypto").mockReturnValue({
            async getActiveSessionBackupVersion() {
                return settings.keyBackupEnabled ? "1" : null;
            },
        } as CryptoApi);
        http.authedRequest.mockResolvedValue(settings.notificationMock);

        const modalSpy = jest.spyOn(Modal, "createDialog");

        jest.spyOn(NotificationConfig, "getBwiDialogs").mockResolvedValue(settings.dialogs);

        await act(async () => {
            render(<WrappedNotificationRenderer />);
        });

        return modalSpy;
    }

    it("should show the error analytics dialog on page 1 of 1 if it's unread", async () => {
        const modalSpy = await setupTest({
            keyBackupEnabled: true,
            notificationMock: NothingSeenMock,
            dialogs: [getBwiAnalyticsDialog()],
        });

        expect(modalSpy).toHaveBeenCalledWith(
            BwiNotificationDialog,
            expect.objectContaining({
                title: "Error Analytics",
                footer: getNotificationOfExpectation(1, 1),
            }),
        );
    });

    it("should show the error analytics dialog on page 1 of 2 if all dialogs are unread", async () => {
        const releaseNotes = await getBwiReleaseNotes("mock");
        const modalSpy = await setupTest({
            keyBackupEnabled: true,
            notificationMock: NothingSeenMock,
            dialogs: [getBwiAnalyticsDialog(), releaseNotes!],
        });

        expect(modalSpy).toHaveBeenCalledWith(
            BwiNotificationDialog,
            expect.objectContaining({
                title: "Error Analytics",
                footer: getNotificationOfExpectation(1, 2),
            }),
        );
    });

    it("shouldn't show any dialogs if none are configured", async () => {
        const modalSpy = await setupTest({
            keyBackupEnabled: true,
            notificationMock: NothingSeenMock,
            dialogs: [],
        });

        expect(modalSpy).not.toHaveBeenCalled();
    });

    it("shouldn't show any dialogs if keybackup is disabled", async () => {
        const modalSpy = await setupTest({
            keyBackupEnabled: false,
            notificationMock: NothingSeenMock,
            dialogs: [getBwiAnalyticsDialogMockNonCustom()],
        });

        expect(modalSpy).not.toHaveBeenCalled();
    });

    it("shouldn't show the error analytics dialog if it's already seen", async () => {
        const modalSpy = await setupTest({
            keyBackupEnabled: true,
            notificationMock: showPrivacyMock(false),
            dialogs: [getBwiAnalyticsDialogMockNonCustom()],
        });

        expect(modalSpy).not.toHaveBeenCalled();
    });

    it("should trigger an effect on first dialog", async () => {
        const releaseNotes = await getBwiReleaseNotes("mock", "confetti");

        await setupTest({
            keyBackupEnabled: true,
            notificationMock: NothingSeenMock,
            dialogs: [releaseNotes!, getBwiAnalyticsDialogMockNonCustom()],
        });

        await waitFor(() => expect(Confetti).toHaveBeenCalled());
    });

    it("*TMP* should show federation announcement modal", async () => {
        const releaseNotes = await getBwiReleaseNotes("mock", "confetti");

        // when release notes and federation announcement

        const modalSpy = await setupTest({
            keyBackupEnabled: true,
            notificationMock: NothingSeenMock,
            dialogs: [
                releaseNotes!,
                {
                    notificationType: BwiNotificationTypes.HINT_FEDERATION_INTRODUCTION,
                    createModal: openBwiFederationOnboarding,
                },
            ],
        });

        // expect release notes to be the first modal and temp dialog to be last
        expect(modalSpy).toHaveBeenNthCalledWith(
            1,
            BwiFederationOnboarding,
            expect.anything(),
            undefined,
            false,
            false,
            expect.anything(),
        );
        expect(modalSpy).toHaveBeenNthCalledWith(2, BwiNotificationDialog, expect.anything());
    });

    it("close should trigger hasSeenNotification", async () => {
        BWI_NOTIFICATION_SETTINGS.showReleaseNotes = true;
        const notificationManagerSpy = jest.spyOn(BwiNotificationManager.prototype, "hasSeenNotification");
        const releaseNotes = await getBwiReleaseNotes("mock");

        await setupTest({
            keyBackupEnabled: true,
            notificationMock: NothingSeenMock,
            dialogs: [releaseNotes!],
        });

        expect(client.setAccountData).not.toHaveBeenCalled();
        await waitFor(() => screen.getByTestId("bwiReleaseNotificationDialogConfirm"));
        expect(screen.queryByRole("dialog")).toBeInTheDocument();
        await act(async () => {
            fireEvent.click(screen.getByTestId("bwiReleaseNotificationDialogConfirm"));
        });
        expect(notificationManagerSpy).toHaveBeenCalledTimes(1);
        expect(client.setAccountData).toHaveBeenCalledWith(
            NOTIFICATION_DATA,
            expect.objectContaining({ should_show_web_release_notes: { mock: false } }),
        );
    });
});
