// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { render } from "@testing-library/react";
import React, { ComponentProps } from "react";
import TabbedView, { NotificationTabInfo, Tab } from "../../../src/components/structures/TabbedView";
import { UserTab } from "../../../src/components/views/dialogs/UserTab";
import { _td } from "../../../src/languageHandler";
import { stubClient } from "../../../test/test-utils";
import { MaintenanceManager, useMaintenanceManager } from "../../../src/bwi/managers/MaintenanceManager";
import { RegularMaintenanceMock } from "../../../src/bwi/components/views/maintenance/MockMaintenanceInfo";
import { getRequestClientSpy } from "../../mock/MockClient";

jest.mock("../../../src/bwi/managers/MaintenanceManager", () => {
    const originalModule = jest.requireActual("../../../src/bwi/managers/MaintenanceManager");
    return {
        ...originalModule,
        useMaintenanceManager: jest.fn(),
    };
});

function mockHelper(returnVal: ReturnType<typeof useMaintenanceManager>) {
    (useMaintenanceManager as unknown as jest.MockedFn<typeof useMaintenanceManager>).mockReturnValue(returnVal);
}

describe("<TabbedView />", () => {
    // let client: MatrixClient;
    beforeEach(() => {
        jest.resetModules();
    });
    beforeAll(() => {
        stubClient();
    });

    afterAll(() => {
        MaintenanceManager.resetManager();
    });

    const activeTabId = UserTab.General;
    const notActiveTabId = UserTab.Help;
    const notificationTab: NotificationTabInfo = {
        id: activeTabId,
        banner: undefined,
        shouldShowNotification: true,
        shouldShowBanner: false,
    };
    const props: ComponentProps<typeof TabbedView> = {
        tabs: [
            new Tab(
                activeTabId,
                _td("common|general"),
                "mx_UserSettingsDialog_settingsIcon",
                <></>,
                "UserSettingsGeneral",
            ),
            new Tab(
                notActiveTabId,
                _td("setting|help_about|title"),
                "mx_UserSettingsDialog_helpIcon",
                <></>,
                "UserSettingsHelpAbout",
            ),
        ],
        activeTabId,
        onChange: () => {
            return;
        },
        notificationTab,
    };
    it("should render a <TabLabel /> with a non blocking maintance notification", () => {
        mockHelper({
            enabled: "",
            downtime: null,
            manager: MaintenanceManager.instance,
            showNotification: true,
            showBanner: false,
            isBlocking: true,
        });

        const { getByRole } = render(<TabbedView {...props} />);

        const notificationLiEl = getByRole("tab", { selected: true });
        expect(notificationLiEl).toHaveClass("mx_TabbedView_Notification_pulse");
        expect(notificationLiEl).toHaveClass("mx_BwiMaintenance_blocking");

        const noNotificationLiEl = getByRole("tab", { selected: false });
        expect(noNotificationLiEl).not.toHaveClass("mx_TabbedView_Notification_pulse");
        expect(noNotificationLiEl).toHaveClass("mx_BwiMaintenance_blocking");
    });

    it("should render a <TabLabel /> with a blocking maintance notification", () => {
        getRequestClientSpy(RegularMaintenanceMock);
        mockHelper({
            enabled: "",
            downtime: null,
            manager: MaintenanceManager.instance,
            showNotification: true,
            showBanner: false,
            isBlocking: false,
        });

        const { getByRole } = render(<TabbedView {...props} />);

        const notificationLiEl = getByRole("tab", { selected: true });
        expect(notificationLiEl).toHaveClass("mx_TabbedView_Notification_pulse");
        expect(notificationLiEl).not.toHaveClass("mx_BwiMaintenance_blocking");

        const noNotificationLiEl = getByRole("tab", { selected: false });
        expect(noNotificationLiEl).not.toHaveClass("mx_TabbedView_Notification_pulse");
        expect(noNotificationLiEl).not.toHaveClass("mx_BwiMaintenance_blocking");
    });

    it("should render a <TabLabel /> with no maintance notification", () => {
        mockHelper({
            enabled: "",
            downtime: null,
            manager: MaintenanceManager.instance,
            showNotification: false,
            showBanner: false,
            isBlocking: false,
        });

        const { getByRole } = render(<TabbedView {...props} />);

        const notificationLiEl = getByRole("tab", { selected: true });
        expect(notificationLiEl).not.toHaveClass("mx_TabbedView_Notification_pulse");
        expect(notificationLiEl).not.toHaveClass("mx_BwiMaintenance_blocking");

        const noNotificationLiEl = getByRole("tab", { selected: false });
        expect(noNotificationLiEl).not.toHaveClass("mx_TabbedView_Notification_pulse");
        expect(noNotificationLiEl).not.toHaveClass("mx_BwiMaintenance_blocking");
    });
});
