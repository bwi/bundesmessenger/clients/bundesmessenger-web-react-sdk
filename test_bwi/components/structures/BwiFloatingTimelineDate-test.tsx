// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, fireEvent, render, screen } from "@testing-library/react";
import EventEmitter from "events";
import { MatrixEvent, Room } from "matrix-js-sdk/src/matrix";
import React from "react";
import { bwiCreateFloatingDateScrollHandler } from "../../../src/bwi/components/structures/BwiFloatingTimelineDate";
import { BwiFeature } from "../../../src/bwi/settings/BwiFeature";
import MessagePanel from "../../../src/components/structures/MessagePanel";
import { IRoomState } from "../../../src/components/structures/RoomView";
import MatrixClientContext from "../../../src/contexts/MatrixClientContext";
import RoomContext, { TimelineRenderingType } from "../../../src/contexts/RoomContext";
import SettingsStore from "../../../src/settings/SettingsStore";
import ResizeNotifier from "../../../src/utils/ResizeNotifier";
import { mkMessage, unmockClientPeg } from "../../../test/test-utils";
import { getBaseClient, getClientUserMocks } from "../../mock/MockClient";
import { mockFeatureFlag } from "../../mock/MockSettingsStore";
import { mockUser } from "../../mock/MockUser";

describe("BwiFloatingTimelineDate", () => {
    function getScrollContainer({
        containerTop,
        eventTiles,
    }: {
        containerTop: number;
        eventTiles?: [number, number, string?][];
    }) {
        return {
            getBoundingClientRect() {
                return { top: containerTop };
            },
            querySelectorAll() {
                return (eventTiles || []).map(([clientHeight, top, id], i) => ({
                    dataset: { eventId: id || "" + (i + 1) },
                    clientHeight,
                    getBoundingClientRect() {
                        return {
                            top,
                        };
                    },
                }));
            },
        };
    }
    describe("scrollHandler", () => {
        const events = [
            {
                localTimestamp: +new Date(2011, 10, 11),
                getId() {
                    return "1";
                },
            },
            {
                localTimestamp: +new Date(2011, 10, 15),
                getId() {
                    return "2";
                },
            },
            {
                localTimestamp: +new Date(2011, 10, 19),
                getId() {
                    return "3";
                },
            },
        ] as MatrixEvent[];
        it("should show on scroll and hide after one second", async () => {
            // When: Feature enabled
            jest.spyOn(SettingsStore, "getValue").mockReturnValueOnce(true);

            const setState = jest.fn();
            const onScroll = bwiCreateFloatingDateScrollHandler({
                getEvents: () => events,
                setState,
            });

            onScroll({
                target: getScrollContainer({
                    containerTop: 0,
                    eventTiles: [
                        [-100, 50], // Not visible
                        [-20, 100], // Visible, Current Date
                        [300, 10], // Visible
                    ],
                }),
            } as unknown as Event);

            expect(setState).toHaveBeenCalledWith({ label: "Nov 15, 2011", show: true });
            setState.mockClear();

            await new Promise((done) => setTimeout(done, 1000));
            expect(setState).toHaveBeenCalledWith({ show: false });
        });

        it("should not show for the first event in the timeline", () => {
            // When: Feature enabled
            jest.spyOn(SettingsStore, "getValue").mockReturnValueOnce(true);

            const setState = jest.fn();
            const onScroll = bwiCreateFloatingDateScrollHandler({
                getEvents: () => events,
                setState,
            });

            onScroll({
                target: getScrollContainer({
                    containerTop: 0,
                    eventTiles: [
                        [200, 50], // Visible, Current Date and first event
                        [260, 40], // Visible
                        [310, 100], // Visible
                    ],
                }),
            } as unknown as Event);

            expect(setState).toHaveBeenCalledWith({ show: false });
        });

        it("should not show if no events in the timeline", () => {
            // When: Feature enabled
            jest.spyOn(SettingsStore, "getValue").mockReturnValueOnce(true);

            const setState = jest.fn();
            const onScroll = bwiCreateFloatingDateScrollHandler({
                getEvents: () => events,
                setState,
            });

            onScroll({
                target: getScrollContainer({
                    containerTop: 0,
                    eventTiles: [],
                }),
            } as unknown as Event);

            expect(setState).toHaveBeenCalledWith({ show: false });
        });

        it("should use the alternative config object", async () => {
            const hideAfterMs = 500;
            // When: Feature enabled
            jest.spyOn(SettingsStore, "getValue").mockReturnValueOnce({
                enabled: true,
                hideAfterMs,
            });

            const setState = jest.fn();
            const onScroll = bwiCreateFloatingDateScrollHandler({
                getEvents: () => events,
                setState,
            });

            onScroll({
                target: getScrollContainer({
                    containerTop: 0,
                    eventTiles: [
                        [-100, 50], // Not visible
                        [-20, 100], // Visible, Current Date
                        [300, 10], // Visible
                    ],
                }),
            } as unknown as Event);

            expect(setState).toHaveBeenCalledWith({ label: "Nov 15, 2011", show: true });
            setState.mockClear();

            await new Promise((done) => setTimeout(done, hideAfterMs));

            expect(setState).toHaveBeenCalledWith({ show: false });
        });

        it("should return a no-op function if feature is disabled", () => {
            jest.spyOn(SettingsStore, "getValue").mockReturnValueOnce(false);

            const setState = jest.fn();

            const onScroll = bwiCreateFloatingDateScrollHandler({
                getEvents: () => [],
                setState,
            });

            onScroll({
                target: getScrollContainer({
                    containerTop: 0,
                    eventTiles: [],
                }),
            } as unknown as Event);

            expect(setState).not.toHaveBeenCalled();
        });
    });
    describe("MessagePanel", () => {
        let room: Room | null = null;
        const client = getBaseClient({
            ...getClientUserMocks(mockUser),
            supportsThreads: () => false,
            getPushActionsForEvent: () => null,
            getRoom: () => room,
            isRoomEncrypted: () => false,
            decryptEventIfNeeded: () => Promise.resolve(),
        });
        const roomId = "!room:server.org";
        room = new Room(roomId, client, mockUser.userId);

        afterAll(() => {
            unmockClientPeg();
        });

        it("should attach the onScroll handler to the message panel", async () => {
            const featureMock = mockFeatureFlag(BwiFeature.FloatingDateIndicator, {
                enabled: true,
                hideAfterMs: 50,
            });
            const events = [
                mkMessage({
                    event: true,
                    room: roomId,
                    user: mockUser.userId,
                }),
                mkMessage({
                    event: true,
                    room: roomId,
                    user: mockUser.userId,
                }),
                mkMessage({
                    event: true,
                    room: roomId,
                    user: mockUser.userId,
                }),
            ];
            events.forEach((event) => {
                event.localTimestamp = +new Date(2011, 10, 15);
            });
            const { container } = render(
                <MatrixClientContext.Provider value={client}>
                    <RoomContext.Provider
                        value={
                            {
                                ...RoomContext,
                                timelineRenderingType: TimelineRenderingType.Room,
                                room,
                                roomId,
                                canReact: true,
                                canSendMessages: true,
                                showReadReceipts: true,
                                showRedactions: false,
                                showJoinLeaves: false,
                                showAvatarChanges: false,
                                showDisplaynameChanges: true,
                                showHiddenEvents: false,
                            } as unknown as IRoomState
                        }
                    >
                        <MessagePanel
                            events={events}
                            className=""
                            callEventGroupers={new Map()}
                            room={room!}
                            resizeNotifier={new EventEmitter() as unknown as ResizeNotifier}
                        />
                    </RoomContext.Provider>
                </MatrixClientContext.Provider>,
            );

            await act(async () => {
                fireEvent.scroll(container.querySelector(".mx_ScrollPanel")!, {
                    target: getScrollContainer({
                        containerTop: 0,
                        eventTiles: [
                            [-100, 50, events[0].getId()], // Not visible
                            [-20, 100, events[1].getId()], // Visible, Current Date
                            [300, 10, events[2].getId()], // Visible
                        ],
                    }),
                });
            });

            // Expect: DateHightlight is in the document
            expect(
                screen.queryByText("Nov 15, 2011", { selector: ".bwi_MessagePanel_DateHighlight_date" }),
            ).toBeInTheDocument();

            featureMock.mockRestore();
        });
    });
});
