// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
import { render, screen } from "@testing-library/react";
import React from "react";
import { MatrixClientPeg } from "../../../src/MatrixClientPeg";
import SdkConfig from "../../../src/SdkConfig";
import { LegalUserSettingsTab } from "../../../src/bwi/settings/tabs/user/LegalUserSettingsTab";
import { mockFeatureFlag } from "../../mock/MockSettingsStore";
import { BwiFeature } from "../../../src/bwi/settings/BwiFeature";
import { _t } from "../../../src/languageHandler";

describe("LegalUserSettingsTab", () => {
    it("should render all configured links", () => {
        SdkConfig.put({
            brand: "mock",
            terms_and_conditions_links: [
                {
                    text: "Copyright",
                    url: "https://mock.com/copyright",
                },
            ],
        });

        jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValueOnce({
            getClientWellKnown: () => ({
                "de.bwi": {
                    data_privacy_url: "https://mock.com/privacy",
                    imprint_url: "https://mock.com/imprint",
                },
            }),
        } as any);

        render(<LegalUserSettingsTab />);

        expect(
            screen.queryByText("Copyright", { selector: 'a[href="https://mock.com/copyright"]' }),
        ).toBeInTheDocument();
        expect(
            screen.queryByText("Privacy Policy", { selector: 'a[href="https://mock.com/privacy"]' }),
        ).toBeInTheDocument();
        expect(screen.queryByText("Imprint", { selector: 'a[href="https://mock.com/imprint"]' })).toBeInTheDocument();
    });

    it("should render nothing with empty config", () => {
        SdkConfig.put({
            brand: "mock",
            terms_and_conditions_links: [],
        });

        jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValueOnce({
            getClientWellKnown: () => ({
                "de.bwi": {},
            }),
        } as any);

        render(<LegalUserSettingsTab />);

        expect(screen.queryByText("Copyright")).not.toBeInTheDocument();
        expect(screen.queryByText("Privacy Policy")).not.toBeInTheDocument();
        expect(screen.queryByText("Imprint")).not.toBeInTheDocument();
    });

    it("should render nothing with invalid config", () => {
        SdkConfig.put({
            brand: "mock",
        });

        jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValueOnce({
            getClientWellKnown: () => ({}),
        } as any);

        render(<LegalUserSettingsTab />);

        expect(screen.queryByText("Copyright")).not.toBeInTheDocument();
        expect(screen.queryByText("Privacy Policy")).not.toBeInTheDocument();
        expect(screen.queryByText("Imprint")).not.toBeInTheDocument();
    });

    it("sould filter out duplicate values", () => {
        SdkConfig.put({
            brand: "mock",
            terms_and_conditions_links: [
                {
                    text: "Privacy Policy",
                    url: "https://mock.com/copyright",
                },
            ],
        });

        jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValueOnce({
            getClientWellKnown: () => ({
                "de.bwi": {
                    data_privacy_url: "https://mock.com/privacy",
                },
            }),
        } as any);

        render(<LegalUserSettingsTab />);

        expect(screen.queryAllByRole("link")).toHaveLength(1);
    });

    it("should render subsection", () => {
        SdkConfig.put({
            brand: "mock",
            terms_and_conditions_links: [
                {
                    text: "Privacy Policy",
                    url: "https://mock.com/copyright",
                },
            ],
        });
        jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValueOnce({
            getClientWellKnown: () => ({
                "de.bwi": {
                    data_privacy_url: "https://mock.com/privacy",
                },
            }),
        } as any);
        mockFeatureFlag(BwiFeature.DataVNumber, "12345 S");

        render(<LegalUserSettingsTab />);

        expect(screen.queryByText(_t("bwi|legal|data_v_number", { num: "12345 S" }))).toBeInTheDocument();
    });
});
