// Copyright 2024 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, render, screen } from "@testing-library/react";
import React from "react";
import { BwiNotificationDialog } from "../../../src/bwi/components/notifications/BwiNotificationRenderer";
import {
    BwiNewFeatureUserSettingsTab,
    SHOW_RELEASE_NOTES_FOR,
} from "../../../src/bwi/settings/tabs/user/NewFeaturesUserSettingsTab";

jest.mock("../../../src/bwi/components/notifications/BwiReleaseNotes", () => ({
    async getBwiReleaseNotes(version: string) {
        return {
            title: `Version ${version}`,
            notificationType: "should_show_web_release_notes",
        } as BwiNotificationDialog;
    },
}));

describe("BwiNewFeatureUserSettingsTab", () => {
    it("should render notes for each version", async () => {
        await act(async () => {
            render(<BwiNewFeatureUserSettingsTab />);
        });

        screen.debug();

        SHOW_RELEASE_NOTES_FOR.forEach((version) => {
            expect(screen.queryByText(`Version ${version}`)).toBeInTheDocument();
        });
    });
});
