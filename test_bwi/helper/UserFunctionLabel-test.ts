// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { EventType, MatrixEvent, RoomMember } from "matrix-js-sdk/src/matrix";

import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import * as TestUtils from "../../test/test-utils";

import {
    BwiFunctionLabelEvent,
    getUserFunctionLabelEvent,
    setUserFunctionLabel,
    textForUserFunctionLabelEvent,
    unsetUserFunctionLabel,
} from "../../src/bwi/helper/events/UserFunctionLabel";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { generateRoomId, mockRoomWithEvents } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
    },
}));

jest.mock("../../src/stores/spaces/SpaceStore", () => ({
    spacesEnabled: false,
}));

describe("UserFunctionLabel", () => {
    const createEvent = (content: BwiFunctionLabelEvent, user: string, room: string) => {
        return TestUtils.mkEvent({
            type: EventType.BwiUserFunctionLabels,
            event: true,
            content,
            room,
            user,
        });
    };

    TestUtils.stubClient();
    const client = MatrixClientPeg.safeGet();

    const mockRoom = TestUtils.mkStubRoom(generateRoomId(), "1111", client);
    const mockMember = new RoomMember(mockRoom.roomId, "test@localhost");
    mockMember.rawDisplayName = "test";

    const mockMember2 = new RoomMember(mockRoom.roomId, "test1@localhost");
    mockMember2.rawDisplayName = "test1";

    const mockLabel = "testLabel";
    const defaultEventContent = {
        [mockMember.userId]: mockLabel,
    };

    const multiUserEventContent = {
        [mockMember.userId]: mockLabel,
        [mockMember2.userId]: mockLabel,
    };

    const defaultEvent: MatrixEvent = createEvent(defaultEventContent, mockMember.userId, mockRoom.roomId);
    const multiUserEvent: MatrixEvent = createEvent(multiUserEventContent, mockMember.userId, mockRoom.roomId);

    const stateEventSpy = jest.spyOn(mockRoom.currentState, "getStateEvents").mockReturnValue(defaultEvent);

    beforeEach(() => {
        stateEventSpy.mockClear();
    });

    describe("getUserFunctionLabelEvent", () => {
        it("should properly unwrap the state event", () => {
            expect(getUserFunctionLabelEvent(mockRoom)).toStrictEqual(defaultEventContent);
        });

        it("should default to an empty object if there is no event", () => {
            stateEventSpy.mockReturnValueOnce(null);

            expect(getUserFunctionLabelEvent(mockRoom)).toStrictEqual({});
        });
    });

    describe("setUserFunctionLabel", () => {
        beforeEach(() => {
            jest.spyOn(client, "sendStateEvent").mockReset();
        });

        it("should send a state event with given content for a single user", () => {
            const sendStateEventSpy = jest.spyOn(client, "sendStateEvent");
            const testLabel = "testtesttesttest";

            setUserFunctionLabel(mockRoom, mockMember, testLabel);

            expect(sendStateEventSpy).toHaveBeenCalledTimes(1);
            expect(stateEventSpy).toHaveBeenCalledTimes(2);

            expect(sendStateEventSpy.mock.calls[0]).toEqual([
                mockRoom.roomId,
                EventType.BwiUserFunctionLabels,
                { [mockMember.userId]: testLabel },
            ]);
        });

        it("should send a state event with given content for multiple users", () => {
            const sendStateEventSpy = jest.spyOn(client, "sendStateEvent");
            const testLabel = "testtesttesttest";

            setUserFunctionLabel(mockRoom, [mockMember, mockMember2], testLabel);

            expect(sendStateEventSpy).toHaveBeenCalledTimes(1);
            expect(stateEventSpy).toHaveBeenCalledTimes(2);

            expect(sendStateEventSpy.mock.calls[0]).toEqual([
                mockRoom.roomId,
                EventType.BwiUserFunctionLabels,
                {
                    [mockMember.userId]: testLabel,
                    [mockMember2.userId]: testLabel,
                },
            ]);
        });

        it("should patch the power levels if below 100 to 100", () => {
            const sendStateEventSpy = jest.spyOn(client, "sendStateEvent");
            const roomId = generateRoomId();
            const room = mockRoomWithEvents({
                events: [
                    createEvent(defaultEventContent, mockMember.userId, roomId),
                    TestUtils.mkEvent({
                        event: true,
                        user: "",
                        content: {
                            events: {
                                [EventType.BwiUserFunctionLabels]: 50,
                            },
                        },
                        skey: "",
                        type: EventType.RoomPowerLevels,
                        room: roomId,
                    }),
                ],
                partials: {
                    roomId,
                },
            });
            setUserFunctionLabel(room, [mockMember, mockMember2], "testtesttesttest");

            expect(sendStateEventSpy.mock.calls[0]).toEqual([
                roomId,
                EventType.RoomPowerLevels,
                expect.objectContaining({
                    events: {
                        [EventType.BwiUserFunctionLabels]: 100,
                    },
                }),
            ]);
        });

        it("should patch the power level to 100 if not set", () => {
            const sendStateEventSpy = jest.spyOn(client, "sendStateEvent");
            const roomId = generateRoomId();
            const room = mockRoomWithEvents({
                events: [
                    createEvent(defaultEventContent, mockMember.userId, roomId),
                    TestUtils.mkEvent({
                        event: true,
                        user: "",
                        content: {
                            events: {},
                        },
                        skey: "",
                        type: EventType.RoomPowerLevels,
                        room: roomId,
                    }),
                ],
                partials: {
                    roomId,
                },
            });
            setUserFunctionLabel(room, [mockMember, mockMember2], "testtesttesttest");

            expect(sendStateEventSpy.mock.calls[0]).toEqual([
                roomId,
                EventType.RoomPowerLevels,
                expect.objectContaining({
                    events: {
                        [EventType.BwiUserFunctionLabels]: 100,
                    },
                }),
            ]);
        });

        it("should not patch the power level if power level event is empty", () => {
            const sendStateEventSpy = jest.spyOn(client, "sendStateEvent");
            const roomId = generateRoomId();
            const room = mockRoomWithEvents({
                events: [
                    createEvent(defaultEventContent, mockMember.userId, roomId),
                    TestUtils.mkEvent({
                        event: true,
                        user: "",
                        content: {},
                        skey: "",
                        type: EventType.RoomPowerLevels,
                        room: roomId,
                    }),
                ],
                partials: {
                    roomId,
                },
            });
            setUserFunctionLabel(room, [mockMember, mockMember2], "testtesttesttest");

            expect(sendStateEventSpy).toHaveBeenCalledTimes(1);
        });
    });

    describe("unsetUserFunctionLabel", () => {
        beforeEach(() => {
            jest.spyOn(client, "sendStateEvent").mockReset();
        });

        it("should delete a users key in the state event", () => {
            const sendStateEventSpy = jest.spyOn(client, "sendStateEvent");

            unsetUserFunctionLabel(mockRoom, mockMember);

            expect(sendStateEventSpy).toHaveBeenCalledTimes(1);
            expect(stateEventSpy).toHaveBeenCalledTimes(2);

            expect(sendStateEventSpy.mock.calls[0]).toEqual([mockRoom.roomId, EventType.BwiUserFunctionLabels, {}]);
        });

        it("should delete multiple users keys in the state event", () => {
            const sendStateEventSpy = jest.spyOn(client, "sendStateEvent");
            stateEventSpy.mockReturnValueOnce(multiUserEvent);
            unsetUserFunctionLabel(mockRoom, [mockMember, mockMember2]);

            expect(sendStateEventSpy).toHaveBeenCalledTimes(1);
            expect(stateEventSpy).toHaveBeenCalledTimes(2);

            expect(sendStateEventSpy.mock.calls[0]).toEqual([mockRoom.roomId, EventType.BwiUserFunctionLabels, {}]);
        });
    });

    describe("textForUserFunctionLabelEvent", () => {
        it("should return always null by default", () => {
            jest.spyOn(defaultEvent, "getPrevContent").mockReturnValueOnce({});

            const result = textForUserFunctionLabelEvent(defaultEvent);

            expect(result).toEqual(null);
        });

        it("should announce added function labels properly", () => {
            mockFeatureFlag(BwiFeature.UserFunctionLabels, true);

            jest.spyOn(defaultEvent, "getPrevContent").mockReturnValueOnce({});
            const result = textForUserFunctionLabelEvent(defaultEvent)?.();

            expect(result).toEqual(`${mockMember.userId} appointed Member to function: ${mockLabel}`);
        });

        it("should announce deleted function labels properly", () => {
            mockFeatureFlag(BwiFeature.UserFunctionLabels, true);

            const emptyEvent = createEvent({}, mockMember.userId, mockRoom.roomId);
            jest.spyOn(emptyEvent, "getPrevContent").mockReturnValueOnce(defaultEvent.getContent());

            const result = textForUserFunctionLabelEvent(emptyEvent)?.();

            expect(result).toEqual(`${mockMember.userId} has removed the function ${mockLabel} for Member`);
        });
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });
});
