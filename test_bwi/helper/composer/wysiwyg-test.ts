// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixClient, Room } from "matrix-js-sdk/src/matrix";
import { MatrixClientPeg } from "../../../src/MatrixClientPeg";
import SdkConfig from "../../../src/SdkConfig";
import { wysiwyg } from "../../../src/bwi/helper/composer/wysiwyg";

describe("wysiwyg helpers", () => {
    describe("validateMessageContent()", () => {
        beforeAll(() => {
            SdkConfig.put({
                permalink_prefix: "https://myapp.to",
            });
        });
        describe("should replace matrix.to permalinks with local permalinks", () => {
            beforeAll(() => {
                jest.spyOn(MatrixClientPeg, "safeGet").mockReturnValue({
                    getRoom(_: string | undefined): Room | null {
                        return null;
                    },
                } as MatrixClient);
            });
            it("for user", () => {
                expect(wysiwyg.validateMessageContent(`<a href="https://matrix.to/#/@myapp:user">User</a>`)).toEqual(
                    `<a href="https://myapp.to/#/user/@myapp:user">User</a>`,
                );
            });
            it("for room", () => {
                expect(wysiwyg.validateMessageContent(`<a href="https://matrix.to/#/#myapp:room">My room</a>`)).toEqual(
                    `<a href="https://myapp.to/#/room/#myapp:room">My room</a>`,
                );
            });
        });
    });
});
