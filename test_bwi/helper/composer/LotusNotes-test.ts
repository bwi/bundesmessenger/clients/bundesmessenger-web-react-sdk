// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { tryAndTransformLonoSnippetToUrl } from "../../../src/bwi/helper/composer/LotusNotes";

describe("LotusNotes", () => {
    it("should return null when input is not a proper lono snippet", () => {
        const invalidSnippet = `<NDL><REPLICAINVALID r1:r2><VIEW v1:v2-v3:v4><NOTE n1:n2-n3:n4></NDL>`;
        expect(tryAndTransformLonoSnippetToUrl(invalidSnippet)).toEqual(null);
    });

    it("should convert partial lono snippets to url", () => {
        const onlyReplica = `<NDL><REPLICA r1:r2></NDL>`;
        const withView = `<NDL><REPLICA r1:r2><VIEW OFv1:v2-ONv3:v4></NDL>`;

        expect(tryAndTransformLonoSnippetToUrl(onlyReplica)).toEqual("notes:///r1r2");
        expect(tryAndTransformLonoSnippetToUrl(withView)).toEqual("notes:///r1r2/v1v2v3v4");
    });

    it("should convert full lono snippet to url", () => {
        const full = `test abc <NDL><REPLICA r1:r2><VIEW OFv1:v2-ONv3:v4><NOTE OFn1:n2-ONn3:n4></NDL>`;

        expect(tryAndTransformLonoSnippetToUrl(full)).toEqual("notes:///r1r2/v1v2v3v4/n1n2n3n4");
    });
});
