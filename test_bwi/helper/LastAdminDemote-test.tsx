// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { fireEvent, render } from "@testing-library/react";
import * as TestUtils from "../../test/test-utils";
import { PowerLevelEditor } from "../../src/components/views/right_panel/UserInfo";
import { mkRoom, mockClientMethodsUser } from "../../test/test-utils";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import DMRoomMap from "../../src/utils/DMRoomMap";
import { mockPowerLevelEvent } from "../mock/MockPowerLevelEvent";
import Modal from "../../src/Modal";
import ErrorDialog from "../../src/components/views/dialogs/ErrorDialog";
import QuestionDialog from "../../src/components/views/dialogs/QuestionDialog";
import bwi from "../../src/bwi";
import { MatrixEvent, Room, RoomMember } from "matrix-js-sdk/src/matrix";

let setPowerLevelSpy: jest.Mock;
let modalSpy: jest.SpyInstance;
let testRoom: Room;

const user = mockClientMethodsUser("@userId:matrix.org");
const lastAdminEvent = mockPowerLevelEvent({
    sender: user,
    usersDefault: 20,
    prevDefault: 20,
    users: {
        ["@userId:matrix.org"]: 100,
    },
    prevUsers: {
        ["@userId:matrix.org"]: 100,
    },
});

const multipleAdminEvent = mockPowerLevelEvent({
    sender: user,
    usersDefault: 20,
    prevDefault: 20,
    users: {
        ["@userId:matrix.org"]: 100,
        ["@userId2:matrix.org"]: 100,
    },
    prevUsers: {
        ["@userId:matrix.org"]: 100,
        ["@userId2:matrix.org"]: 100,
    },
});

const setup = (powerLevelEvent: MatrixEvent) => {
    const MockedPowerLevelEditor = TestUtils.wrapInMatrixClientContext(PowerLevelEditor);
    testRoom = mkRoom(MatrixClientPeg.safeGet(), "!test23:example.com");
    const member = new RoomMember(testRoom.roomId, user.getUserId());
    member.powerLevel = 100;
    testRoom.getMember = jest.fn().mockReturnValue(member) as any;
    testRoom.currentState.getStateEvents = jest.fn().mockReturnValue(powerLevelEvent);
    const utils = render(
        <MockedPowerLevelEditor
            user={user.getUser()}
            room={testRoom}
            roomPermissions={{ canEdit: true, canInvite: true, modifyLevelMax: 100 }}
        />,
    );
    const input = utils.container.querySelector("#mx_Field_1");
    return {
        input,
        ...utils,
    };
};

beforeAll(() => {
    TestUtils.stubClient();
    const client = MatrixClientPeg.safeGet();
    client.setPowerLevel = setPowerLevelSpy;
    DMRoomMap.makeShared(client);
});

afterAll(() => {
    jest.clearAllMocks();
    jest.resetModules();
});

describe("LastAdminDemote", () => {
    beforeEach(() => {
        setPowerLevelSpy = jest.fn();
        modalSpy = jest.spyOn(Modal, "createDialog");
    });

    afterEach(() => {
        setPowerLevelSpy.mockClear();
        modalSpy.mockClear();
    });

    describe("UserInfo", () => {
        it("last admin in room, don't allow demoting self", () => {
            const { input } = setup(lastAdminEvent);
            if (input) {
                fireEvent.change(input, { target: { value: 50 } });
                // don't change powerlevel
                expect(setPowerLevelSpy).toHaveBeenCalledTimes(0);
                // show warning dialog
                expect(modalSpy).toHaveBeenCalledWith(ErrorDialog, expect.anything());
            }
        });

        it("multiple admins in room, allow demoting self but ask for confirmation", () => {
            const { input } = setup(multipleAdminEvent);
            if (input) {
                fireEvent.change(input, { target: { value: 50 } });
                // ask for confirmation but allow change
                expect(modalSpy).toHaveBeenCalledWith(QuestionDialog, expect.anything());
            }
        });
    });

    describe("isLastRoomAdmin", () => {
        it("last admin in room, isLastRoomAdmin is true", () => {
            const result = bwi.isLastRoomAdmin("100", testRoom, lastAdminEvent);
            expect(result).toBe(true);
        });
        it("multiple admins in room, isLastRoomAdmin is false", () => {
            const result = bwi.isLastRoomAdmin("100", testRoom, multipleAdminEvent);
            expect(result).toBe(false);
        });
        it("last admin in room, user is no admin, isLastRoomAdmin is false", () => {
            const result = bwi.isLastRoomAdmin("50", testRoom, lastAdminEvent);
            expect(result).toBe(false);
        });
    });
});
