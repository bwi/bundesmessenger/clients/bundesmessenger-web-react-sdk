// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixEvent } from "matrix-js-sdk/src/matrix";
import { BwiDecryptionFailureTracker } from "../../src/bwi/analytics/BwiDecryptionFailureTracker";
import { BwiAnalyticsClass } from "../../src/bwi/analytics/BwiAnalyticsManager";
import * as Sender from "../../src/bwi/analytics/BwiAnalyticsSender";
import { DecryptionFailureCode } from "matrix-js-sdk/src/crypto-api";

const originalTrackFn = BwiDecryptionFailureTracker.instance["fn"];

describe("BwiDecryptionFailureTracker", () => {
    describe("Instance", () => {
        const senderSpy = jest.spyOn(Sender, "sendAnalyticsEncryptionViewMessage").mockImplementation(jest.fn());

        beforeEach(() => {
            jest.useFakeTimers();
            jest.setSystemTime(0);
            senderSpy.mockClear();
        });
        afterEach(() => {
            jest.useRealTimers();
        });

        it("should send decryption failures to analytics", () => {
            const tracker = makeTracker(originalTrackFn);
            tracker.start(null!);

            const event1 = getDecryptionEvent("1");
            const event2 = getDecryptionEvent("2");

            // load in some failed events
            tracker.addVisibleEvent(event1);
            tracker.eventDecrypted(event1);

            tracker.addVisibleEvent(event2);
            tracker.eventDecrypted(event2);

            // advance time by 1 minute so the check interval processes the failed events and the track function was executed
            jest.advanceTimersByTime(60000);

            // expect the two events to be sent to analytics one by one
            expect(senderSpy).toHaveBeenCalledTimes(2);

            tracker.stop();
        });

        it("should send bundled decryption failures to analytics", () => {
            const tracker = makeTracker(originalTrackFn);
            tracker.start(null!);

            const event1 = getDecryptionEvent("1", false, DecryptionFailureCode.OLM_UNKNOWN_MESSAGE_INDEX);
            const event2 = getDecryptionEvent("2", false, DecryptionFailureCode.OLM_UNKNOWN_MESSAGE_INDEX);
            const event3 = getDecryptionEvent("3", false, DecryptionFailureCode.MEGOLM_UNKNOWN_INBOUND_SESSION_ID);

            // load in some failed events
            tracker.addVisibleEvent(event1);
            tracker.eventDecrypted(event1);

            tracker.addVisibleEvent(event2);
            tracker.eventDecrypted(event2);

            tracker.addVisibleEvent(event3);
            tracker.eventDecrypted(event3);

            // advance time by 1 minute so the check interval processes the failed events and the track function was executed
            jest.advanceTimersByTime(60000);

            // expect the two events to be sent to analytics one by one
            expect(senderSpy).toHaveBeenCalledTimes(2);

            tracker.stop();
        });

        it("should exclude events decrypted within the grace period", () => {
            const tracker = makeTracker(originalTrackFn);
            tracker.start(null!);

            const event1 = getDecryptionEvent("1");
            const event2 = getDecryptionEvent("2");

            // load in some failed events
            tracker.addVisibleEvent(event1);
            tracker.eventDecrypted(event1);

            tracker.addVisibleEvent(event2);
            tracker.eventDecrypted(event2);

            // advance by 20s
            jest.advanceTimersByTime(20000);

            // decrypt event 2
            tracker.eventDecrypted(getDecryptionEvent("2", true));

            // advance time to 1 minute so the check interval processes the failed events and the track function was executed
            jest.advanceTimersByTime(40000);

            // expect only event 1 to be tracked
            expect(senderSpy).toHaveBeenCalledTimes(1);

            tracker.stop();
        });
    });
    describe("E2EEDecryptionTime", () => {
        const senderSpy = jest.spyOn(Sender, "sendE2EEDecryptionTime");

        beforeEach(() => {
            jest.useFakeTimers();
            jest.setSystemTime(0);
            senderSpy.mockClear();
        });
        afterEach(() => {
            jest.useRealTimers();
        });
        it("track e2ee decryption times for failed events", () => {
            const tracker = makeTracker();

            // load in some failed events
            tracker.eventDecrypted(getDecryptionEvent("1"));
            tracker.eventDecrypted(getDecryptionEvent("2"));

            // resolve first event after 1 second
            jest.advanceTimersByTime(1000);
            tracker.eventDecrypted(getDecryptionEvent("1", true));

            // resolve second event after 3 seconds
            jest.advanceTimersByTime(2000);
            tracker.eventDecrypted(getDecryptionEvent("2", true));

            const senderSpy = jest.spyOn(Sender, "sendE2EEDecryptionTime");
            const trackEventSpy = jest.spyOn(BwiAnalyticsClass.sharedInstance, "trackEvent").mockResolvedValue();

            tracker.bwiTrackDecryption();

            // Expect the metrics send to the sender to be correct
            expect(senderSpy).toHaveBeenCalledWith(2, 3000, 2000);
            // Expect track function to have been called for each metric
            expect(trackEventSpy).toHaveBeenCalledTimes(3);
        });

        it("should emit tracking for each interval", () => {
            const tracker = makeTracker();

            tracker.start(null!);

            // load in some failed events
            tracker.eventDecrypted(getDecryptionEvent("1"));
            tracker.eventDecrypted(getDecryptionEvent("2"));
            tracker.eventDecrypted(getDecryptionEvent("3"));
            tracker.eventDecrypted(getDecryptionEvent("4"));

            jest.advanceTimersByTime(11000);

            // Expect no calls if no events are ready to be logged
            expect(senderSpy).not.toHaveBeenCalled();

            // Resolve event "1" after 12s
            jest.advanceTimersByTime(1000);
            tracker.eventDecrypted(getDecryptionEvent("1", true));

            // Resolve event "2" after 14s
            jest.advanceTimersByTime(2000);
            tracker.eventDecrypted(getDecryptionEvent("2", true));

            // Trigger next track interval
            jest.advanceTimersByTime(6000);

            // Expect event "1" and "2" to be logged
            expect(senderSpy).toHaveBeenLastCalledWith(2, 14000, 13000);

            // Resolve event "3" after 25s
            jest.advanceTimersByTime(5000);
            tracker.eventDecrypted(getDecryptionEvent("3", true));

            // Trigger next track interval
            jest.advanceTimersByTime(5000);

            // Expect event "3" to be logged
            expect(senderSpy).toHaveBeenLastCalledWith(1, 25000, 25000);

            // Stop tracking
            tracker.stop();

            // Resolve event "4" after 35s
            jest.advanceTimersByTime(5000);
            tracker.eventDecrypted(getDecryptionEvent("4", true));

            // Expect no more calls to sender after tracking has been stopped
            expect(senderSpy).toHaveBeenCalledTimes(2);
        });
    });
});

function getDecryptionEvent(
    eventId: string,
    resolved = false,
    code: DecryptionFailureCode = DecryptionFailureCode.HISTORICAL_MESSAGE_NO_KEY_BACKUP,
) {
    const event = new MatrixEvent({
        event_id: eventId,
        room_id: "!room",
        content: {
            algorithm: "m.megolm.v1.aes-sha2",
        },
    });

    if (!resolved) {
        jest.spyOn(event, "decryptionFailureReason", "get").mockReturnValue(code);
    }

    return event;
}

function makeTracker(fn: BwiDecryptionFailureTracker["fn"] = () => {}) {
    //@ts-ignore
    return new BwiDecryptionFailureTracker(fn, BwiDecryptionFailureTracker.instance.errorCodeMapFn);
}
