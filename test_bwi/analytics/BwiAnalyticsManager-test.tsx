// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import BwiAnalytics, {
    AnalyticsSessionInfo,
    BwiAnalyticsClass,
    BWI_ANALYTICS_MATRIX_EVENT_TYPE,
    getAnalyticsMetaData,
} from "../../src/bwi/analytics/BwiAnalyticsManager";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import SdkConfig from "../../src/SdkConfig";
import * as TestUtils from "../../test/test-utils";
import { getPlatformSpy } from "../mock/MockPlatform";
import { createRoom } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { MatrixClient, RoomMember } from "matrix-js-sdk/src/matrix";
import { IConfigOptions } from "../../src/IConfigOptions";
import {
    getDeviceDimensionCluster,
    getTimestamp,
    sendAnalyticsPerformanceSendMessage,
} from "../../src/bwi/analytics/BwiAnalyticsSender";

describe("BwiAnalytics", () => {
    let cli: MatrixClient;

    const getAnalyticsEvent = (isEnabled: boolean) =>
        TestUtils.mkEvent({
            type: BWI_ANALYTICS_MATRIX_EVENT_TYPE,
            event: true,
            content: {
                ABCDEFGHI: {
                    time: 1234656345,
                    platform: "mock",
                    consent: isEnabled,
                    app_version: "1.mock",
                },
            },
            user: "@userId:matrix.rog",
            room: "",
            // @ts-ignore
            getContent: jest.fn().mockReturnValue({
                enabled: true,
            }),
        });

    const matomoDefaultConfig: Pick<IConfigOptions, "matomo" | "bwi_setting_defaults" | "setting_defaults"> = {
        matomo: {
            siteId: "mock",
            url: "https://mock.com/",
        },
        setting_defaults: {},
        bwi_setting_defaults: {
            // @ts-ignore
            "BwiFeature.analytics": false,
        },
    };

    const matomoEnabledConfig: Pick<IConfigOptions, "matomo" | "bwi_setting_defaults" | "setting_defaults"> = {
        matomo: {
            siteId: "mock",
            url: "https://mock.com/",
        },
        setting_defaults: {},
        bwi_setting_defaults: {
            // @ts-ignore
            "BwiFeature.analytics": true,
        },
    };

    const matomoMockDimensionConfig: Pick<IConfigOptions, "matomo" | "bwi_setting_defaults" | "setting_defaults"> = {
        matomo: {
            siteId: "mock",
            url: "https://mock.com/",
            dimensions: {
                mockDimension: 1,
            },
        },
        setting_defaults: {},
        bwi_setting_defaults: {
            // @ts-ignore
            "BwiFeature.analytics": true,
        },
    };

    const matomoDevicesCountDimensionConfig: Pick<
        IConfigOptions,
        "matomo" | "bwi_setting_defaults" | "setting_defaults"
    > = {
        matomo: {
            siteId: "mock",
            url: "https://mock.com/",
            dimensions: {
                RoomDevicesCount: 1,
            },
        },
        setting_defaults: {},
        bwi_setting_defaults: {
            // @ts-ignore
            "BwiFeature.analytics": true,
        },
    };

    beforeAll(() => {
        TestUtils.stubClient();
        cli = MatrixClientPeg.safeGet();
        cli.getRoom = jest.fn().mockReturnValue(
            createRoom(cli, {
                findEventById: jest.fn().mockReturnValue({ mock: true }),
                getJoinedMembers: jest.fn().mockReturnValue([new RoomMember("#abc", "#mockUser")]),
            }),
        );
        cli.getStoredDevicesForUser = jest.fn().mockReturnValue([{}]);
        getPlatformSpy();
    });

    afterEach(() => {
        // @ts-ignore
        if (window.bwiAnalytics) {
            // @ts-ignore
            window.bwiAnalytics = undefined;
        }
        // @ts-ignore
        MatrixClientPeg.safeGet().getRoom.mockClear();
    });

    describe("enabled, disabled tests", () => {
        it("feature flag default disabled, config set, don't enable", async () => {
            const flag = mockFeatureFlag(BwiFeature.Analytics, false);
            SdkConfig.put({
                brand: "mock",
                ...matomoDefaultConfig,
            });
            const analytics: BwiAnalyticsClass = BwiAnalytics;
            const canEnable = analytics.canEnable();
            expect(canEnable).toBe(false);
            await analytics.enable();
            expect(analytics.disabled).toBe(true);
            const canSeeOption = analytics.canSeeOption();
            expect(canSeeOption).toBeTruthy();
            flag.mockRestore();
        });

        it("feature flag enabled, config not set, don't enable", async () => {
            const flag = mockFeatureFlag(BwiFeature.Analytics, true);
            SdkConfig.put({
                brand: "mock",
                matomo: undefined,
            });
            const analytics: BwiAnalyticsClass = BwiAnalytics;
            const canEnable = analytics.canEnable();
            expect(canEnable).toBe(false);
            await analytics.enable();
            expect(analytics.disabled).toBe(true);
            const canSeeOption = analytics.canSeeOption();
            expect(canSeeOption).toBeFalsy();
            flag.mockRestore();
        });

        it("feature flag enabled, config set, enable and disable again", async () => {
            const flag = mockFeatureFlag(BwiFeature.Analytics, true);
            SdkConfig.put({
                brand: "mock",
                ...matomoDefaultConfig,
            });
            const analytics: BwiAnalyticsClass = BwiAnalytics;
            const canEnable = analytics.canEnable();
            expect(canEnable).toBeTruthy();
            await analytics.enable();
            expect(analytics.disabled).toBe(false);

            await analytics.disable();
            expect(analytics.disabled).toBe(true);
            flag.mockRestore();
        });

        it("feature flag enabled in config, user turns off feature, canSeeOption true but enabled false", () => {
            const flag = mockFeatureFlag(BwiFeature.Analytics, false);
            SdkConfig.put({
                brand: "mock",
                ...matomoDefaultConfig,
            });
            const analytics: BwiAnalyticsClass = BwiAnalytics;
            const canEnable = analytics.canEnable();
            expect(canEnable).toBeFalsy();
            const canSeeOption = analytics.canSeeOption();
            expect(canSeeOption).toBeTruthy();
            flag.mockRestore();
        });

        it("analytics feature disabled, de.bwi.analytics event received to turn it on", async () => {
            const analytics: BwiAnalyticsClass = BwiAnalytics;
            SdkConfig.put({
                brand: "mock",
                ...matomoDefaultConfig,
            });
            const event = getAnalyticsEvent(true);
            const canEnable = analytics.canEnable();

            expect(canEnable).toBe(false);
            expect(analytics.disabled).toBe(true);
            const accountMock = jest.spyOn(cli, "getAccountData").mockReturnValue(event);

            // @ts-ignore
            cli.emit("accountData", event);
            const canEnableAfter = analytics.canEnable();
            expect(canEnableAfter).toBe(true);
            expect(analytics.disabled).toBe(false);

            accountMock.mockRestore();
        });

        it("analytics feature enabled, de.bwi.analytics event received to turn it off", async () => {
            const analytics: BwiAnalyticsClass = BwiAnalytics;
            let event = getAnalyticsEvent(true);
            let accountMock = jest.spyOn(cli, "getAccountData").mockReturnValue(event);
            SdkConfig.put({
                brand: "mock",
                ...matomoEnabledConfig,
            });
            const canEnable = analytics.canEnable();
            await analytics.enable();
            expect(canEnable).toBe(true);
            expect(analytics.disabled).toBe(false);

            // @ts-ignore
            cli.emit("accountData", event);
            accountMock.mockRestore();
            event = getAnalyticsEvent(false);
            accountMock = jest.spyOn(cli, "getAccountData").mockReturnValue(event);
            const canEnableAfter = analytics.canEnable();
            expect(canEnableAfter).toBe(false);
            expect(analytics.disabled).toBe(true);

            accountMock.mockRestore();
        });
    });

    describe("trackSendMessage", () => {
        it("tracking feature disabled, don't send", () => {
            const flag = mockFeatureFlag(BwiFeature.Analytics, false);
            SdkConfig.put({
                brand: "mock",
                ...matomoDefaultConfig,
            });
            sendAnalyticsPerformanceSendMessage(100, Promise.resolve({ event_id: "mock" }), "mockRoom");
            expect(MatrixClientPeg.safeGet().getRoom).toHaveBeenCalledTimes(0);
            flag.mockRestore();
        });

        it("tracking feature enabled, room exists but message not delayed, don't send", async () => {
            const flag = mockFeatureFlag(BwiFeature.Analytics, true);
            SdkConfig.put({
                brand: "mock",
                ...matomoDefaultConfig,
            });
            const analytics: BwiAnalyticsClass = BwiAnalytics;
            await analytics.enable();
            const sendEvent = jest.fn().mockResolvedValue({});
            window.fetch = sendEvent;
            const startTime = getTimestamp();
            await sendAnalyticsPerformanceSendMessage(startTime, Promise.resolve({ event_id: "mock" }), "mockRoom");
            expect(MatrixClientPeg.safeGet().getRoom).toHaveBeenCalled();
            expect(sendEvent).toHaveBeenCalledTimes(0);
            sendEvent.mockRestore();
            flag.mockRestore();
        });

        it("tracking feature enabled, room exists and message is delayed, send tracking message", async () => {
            const flag = mockFeatureFlag(BwiFeature.Analytics, true);
            SdkConfig.put({
                brand: "mock",
                ...matomoDefaultConfig,
            });
            const analytics: BwiAnalyticsClass = BwiAnalytics;
            await analytics.enable();
            const sendEvent = jest.fn().mockResolvedValue({});
            window.fetch = sendEvent;
            const startTime = getTimestamp() - 6000;
            sendEvent.mockClear();
            await sendAnalyticsPerformanceSendMessage(startTime, Promise.resolve({ event_id: "mock" }), "mockRoom");
            expect(MatrixClientPeg.safeGet().getRoom).toHaveBeenCalled();
            expect(sendEvent).toHaveBeenCalledTimes(1);
            sendEvent.mockRestore();
            flag.mockRestore();
        });
    });

    describe("trackSendMessage Dimension", () => {
        it("tracking feature enabled, other dimension is configured, send tracking message without dimension", async () => {
            const flag = mockFeatureFlag(BwiFeature.Analytics, true);
            SdkConfig.put({
                brand: "mock",
                ...matomoMockDimensionConfig,
            });
            const analytics: BwiAnalyticsClass = BwiAnalytics;
            await new Promise(process.nextTick);
            await analytics.enable();
            const sendEvent = jest.fn().mockResolvedValue({});
            window.fetch = sendEvent;
            const startTime = getTimestamp() - 6000;
            sendEvent.mockClear();
            await sendAnalyticsPerformanceSendMessage(startTime, Promise.resolve({ event_id: "mock" }), "mockRoom");
            expect(MatrixClientPeg.safeGet().getRoom).toHaveBeenCalled();
            expect(sendEvent).toHaveBeenCalledTimes(1);
            expect(sendEvent).not.toHaveBeenCalledWith(expect.stringContaining("&dimension1="), expect.anything());
            sendEvent.mockRestore();
            flag.mockRestore();
        });

        it("tracking feature enabled, devicesCount dimension is configured and attached, send tracking message with dimension", async () => {
            const flag = mockFeatureFlag(BwiFeature.Analytics, true);
            SdkConfig.put({
                brand: "mock",
                ...matomoDevicesCountDimensionConfig,
            });
            const analytics: BwiAnalyticsClass = BwiAnalytics;
            await new Promise(process.nextTick);
            await analytics.enable();
            const sendEvent = jest.fn().mockResolvedValue({});
            window.fetch = sendEvent;
            const startTime = getTimestamp() - 6000;
            sendEvent.mockClear();
            await sendAnalyticsPerformanceSendMessage(startTime, Promise.resolve({ event_id: "mock" }), "mockRoom");
            expect(MatrixClientPeg.safeGet().getRoom).toHaveBeenCalled();
            expect(sendEvent).toHaveBeenCalledTimes(1);
            expect(sendEvent).not.toHaveBeenCalledWith(expect.stringContaining("&dimension1="), expect.anything());
            sendEvent.mockRestore();
            flag.mockRestore();
        });

        const cases = [
            [NaN, "Unbekannt"],
            [0, "Undefiniert"],
            [1, "1 bis 10"],
            [11, "11 bis 100"],
            [101, "101 bis 200"],
            [201, "201 bis 500"],
            [501, "501 bis 1000"],
            [1001, "1001 bis 2500"],
            [2600, "mehr als 2500"],
        ];
        test.each(cases)(
            "given %p as input and %p as expected out, correctly cluster values",
            (count, expectedResult) => {
                const result = getDeviceDimensionCluster(count as number);
                expect(result).toEqual(expectedResult);
            },
        );
    });

    describe("getAnalyticsMetaData", () => {
        beforeAll(() => {
            Date.now = jest.fn(() => 1487076708000);
        });

        it("regular true input, receive proper AnalyticsSessionInfo", async () => {
            const newValue = true;
            const result = await getAnalyticsMetaData(newValue);
            const expected: AnalyticsSessionInfo = {
                consent: true,
                platform: "web",
                time: Date.now(),
                app_version: "0.0.1",
            };
            expect(result).toStrictEqual(expected);
        });
        it("regular false input, receive proper AnalyticsSessionInfo", async () => {
            const newValue = false;
            const result = await getAnalyticsMetaData(newValue);
            const expected: AnalyticsSessionInfo = {
                consent: false,
                platform: "web",
                time: Date.now(),
                app_version: "0.0.1",
            };
            expect(result).toStrictEqual(expected);
        });
    });
});
