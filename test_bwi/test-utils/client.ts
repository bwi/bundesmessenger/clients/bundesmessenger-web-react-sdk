// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixClient, Room, TypedEventEmitter } from "matrix-js-sdk/src/matrix";

export const mockClientHttp = (partial?: Partial<MatrixClient["http"]>) => ({
    http: {
        authedRequest: jest.fn(),
        fetch: jest.fn(),
        abort: jest.fn(),
        abortController: new AbortController(),
        cancelUpload: jest.fn(),
        eventEmitter: new TypedEventEmitter(),
        getContentUri: jest.fn(),
        getCurrentUploads: jest.fn(),
        getUrl: jest.fn(),
        idServerRequest: jest.fn(),
        opts: {} as MatrixClient["http"]["opts"],
        request: jest.fn(),
        requestOtherUrl: jest.fn(),
        setIdBaseUrl: jest.fn(),
        uploadContent: jest.fn(),
        uploads: [],
        ...partial,
    },
});

export const mockClientStore = (partial?: Partial<MatrixClient["store"]>) => ({
    store: {
        getPendingEvents: jest.fn().mockResolvedValue([]),
        setPendingEvents: jest.fn().mockResolvedValue(undefined),
        storeRoom: jest.fn(),
        removeRoom: jest.fn(),
        ...partial,
    },
});

export const mockClientRoomFunctions = (client: MatrixClient, rooms: Room[]) => {
    client.getRoom = (id) => {
        return rooms.find((room) => room.roomId === id || "") || null;
    };
};
