// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, waitFor } from "@testing-library/react";
import { RenderResult as RenderHookResult, renderHook } from "@testing-library/react-hooks";
import fetchMockJest from "fetch-mock-jest";
import { BwiWellKnownConfig, setCurrentHomeServer, useBwiWellKnownConfig } from "../../src/bwi/functions";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { getBaseClient } from "../mock/MockClient";
import { ClientEvent } from "matrix-js-sdk/src/matrix";
import { SettingLevel } from "../../src/settings/SettingLevel";

const EMPTY_WELLKNOWN = {
    data_privacy_url: "",
    imprint_url: "",
    federation: {},
};

const configResponse = {
    ["de.bwi"]: {
        ...EMPTY_WELLKNOWN,
        data_privacy_url: "https://privacy.com",
        imprint_url: "https://imprintg.com",
    },
};
const validConfig = {
    dataPrivacyUrl: "https://privacy.com",
    imprintUrl: "https://imprintg.com",
    federation: {},
};
const HOME_SERVER = "https://hs.de";

describe("Wellknown", () => {
    beforeEach(() => {
        fetchMockJest.mockReset();
    });

    describe("getBwiWellKnownConfig", () => {
        beforeEach(() => {
            jest.resetModules();
        });
        it("should fetch and cache the wellknown from the homeserver", async () => {
            const fetchSpy = fetchMockJest.get("https://hs.de/.well-known/matrix/client", configResponse);

            const { getWellKnownConfig } = await import("../../src/bwi/functions/WellKnown");

            expect(await getWellKnownConfig(HOME_SERVER)).toEqual(validConfig);
            expect(await getWellKnownConfig(HOME_SERVER)).toEqual(validConfig);
            expect(fetchSpy).toHaveBeenCalledTimes(1);
        });

        it("should return null if the requested failed", async () => {
            fetchMockJest.get("https://hs.de/.well-known/matrix/client", {
                status: 500,
            });

            const { getWellKnownConfig } = await import("../../src/bwi/functions/WellKnown");

            expect(await getWellKnownConfig(HOME_SERVER)).toBeNull();
        });

        it("should return null if the homeserver is invalid", async () => {
            const { getWellKnownConfig } = await import("../../src/bwi/functions/WellKnown");

            expect(await getWellKnownConfig("")).toBeNull();
        });

        it("should return null if the config is empty or invalid", async () => {
            fetchMockJest.get("https://hs.de/.well-known/matrix/client", {
                empty: {},
            });

            const { getWellKnownConfig } = await import("../../src/bwi/functions/WellKnown");

            expect(await getWellKnownConfig(HOME_SERVER)).toBeNull();
        });

        it("should load a partial config and set fallback values", async () => {
            fetchMockJest.getOnce("https://hs.de/.well-known/matrix/client", {
                ["de.bwi"]: {
                    data_privacy_url: "https://privacy.com",
                },
            });

            expect(
                await import("../../src/bwi/functions/WellKnown").then((module) =>
                    module.getWellKnownConfig(HOME_SERVER),
                ),
            ).toEqual({
                dataPrivacyUrl: "https://privacy.com",
                imprintUrl: "",
                federation: {},
            });

            jest.resetModules();

            fetchMockJest.getOnce("https://hs.de/.well-known/matrix/client", {
                ["de.bwi"]: {
                    imprint_url: "https://imprint.com",
                },
            });
            expect(
                await import("../../src/bwi/functions/WellKnown").then((module) =>
                    module.getWellKnownConfig(HOME_SERVER),
                ),
            ).toEqual({
                dataPrivacyUrl: "",
                imprintUrl: "https://imprint.com",
                federation: {},
            });
        });
    });

    describe("useBwiWellKnownConfig", () => {
        it("should set and load the config based on an event", async () => {
            fetchMockJest.getOnce("https://hs.de/.well-known/matrix/client", configResponse);

            const { result, unmount } = renderHook(() => useBwiWellKnownConfig());

            expect(result.current).toBeNull();

            await act(async () => {
                setCurrentHomeServer(HOME_SERVER);
            });

            expect(result.current).toEqual(validConfig);

            unmount();
        });

        it("should use the clients home server if available", async () => {
            getBaseClient({
                getHomeserverUrl() {
                    return "https://client.homeserver.org";
                },
            });

            fetchMockJest.getOnce("https://client.homeserver.org/.well-known/matrix/client", {
                ["de.bwi"]: {
                    data_privacy_url: "https://client.privacy.com",
                },
            });

            let result!: RenderHookResult<BwiWellKnownConfig | null>;
            await act(async () => {
                result = renderHook(() => useBwiWellKnownConfig()).result;
            });

            await waitFor(() => {
                expect(result.current?.dataPrivacyUrl).toEqual("https://client.privacy.com");
            });
        });
    });

    describe("WellknownSettingsHandler", () => {
        // Run test in isolated env to ensure the static classes are reloaded

        it("should return the correct value from manager", async () => {
            await jest.isolateModulesAsync(async () => {
                const SettingsStore = await import("../../src/settings/SettingsStore").then((m) => m.default);
                const MatrixClientBackedSettingsHandler = await import(
                    "../../src/settings/handlers/MatrixClientBackedSettingsHandler"
                ).then((m) => m.default);

                // load client
                MatrixClientBackedSettingsHandler.matrixClient = getBaseClient({
                    isGuest() {
                        return false;
                    },
                    getClientWellKnown() {
                        return {
                            "de.bwi": {
                                federation: {
                                    enable: true,
                                },
                            },
                        };
                    },
                });

                // get value
                expect(SettingsStore.getValue(BwiFeature.Federation, "")).toBe(true);
            });
        });

        it("should watch a setting", async () => {
            await jest.isolateModulesAsync(async () => {
                const SettingsStore = await import("../../src/settings/SettingsStore").then((m) => m.default);
                const MatrixClientBackedSettingsHandler = await import(
                    "../../src/settings/handlers/MatrixClientBackedSettingsHandler"
                ).then((m) => m.default);

                const testPromise = new Promise((done) => {
                    SettingsStore.watchSetting(
                        BwiFeature.Federation,
                        null,
                        (_setting, _room, _level, _levelValue, value) => {
                            expect(value).toBe(true);
                            done(undefined);
                        },
                    );
                });

                const wellKnown = {
                    "de.bwi": {
                        federation: {
                            enable: true,
                        },
                    },
                };
                // load client
                MatrixClientBackedSettingsHandler.matrixClient = getBaseClient({
                    isGuest() {
                        return false;
                    },
                    getClientWellKnown() {
                        return wellKnown;
                    },
                });

                // emit event
                MatrixClientBackedSettingsHandler["_matrixClient"].emit(ClientEvent.ClientWellKnown, wellKnown);

                await testPromise;
            });
        });

        it("should handle error cases and default values", async () => {
            await jest.isolateModulesAsync(async () => {
                const SettingsStore = await import("../../src/settings/SettingsStore").then((m) => m.default);
                const MatrixClientBackedSettingsHandler = await import(
                    "../../src/settings/handlers/MatrixClientBackedSettingsHandler"
                ).then((m) => m.default);

                // load client
                MatrixClientBackedSettingsHandler.matrixClient = getBaseClient({
                    isGuest() {
                        return false;
                    },
                    getClientWellKnown() {
                        return {
                            "de.bwi": {
                                federation: {
                                    enable: true,
                                },
                            },
                        };
                    },
                });

                const handler = SettingsStore["getHandler"](BwiFeature.Federation, SettingLevel.WELLKNOWN)!;

                // it should not support unknown settings
                expect(() => {
                    handler.getValue("random_feature", "");
                }).toThrow(`random_feature is not supported by ${SettingLevel.WELLKNOWN}`);

                expect(handler.canSetValue(BwiFeature.Federation, "")).toBe(false);
            });
        });
    });
});
