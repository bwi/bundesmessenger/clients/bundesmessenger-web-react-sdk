// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { BWI_NOTIFICATION_SETTINGS, getBwiDialogs } from "../../src/bwi/functions";
import { BwiNotificationTypes } from "../../src/bwi/components/notifications/BwiNotifications";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import * as ReleaseNotesUtils from "../../src/bwi/components/notifications/BwiReleaseNotes";
import BwiAnalyticsManager from "../../src/bwi/analytics/BwiAnalyticsManager";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import fetchMockJest from "fetch-mock-jest";

const NEW_FEATURE_DIALOG = {
    notificationType: BwiNotificationTypes.HINT_RELEASE_NOTES,
    title: "Title",
    version: "1.0.0",
    content: <div className="mx_BwiReleaseNotes">Content</div>,
    triggerEffect: "fireworks" as const,
    confirmButton: <div data-testid="bwiReleaseNotificationDialogConfirm">Ok</div>,
};

describe("prepareBwiDialogs", () => {
    BWI_NOTIFICATION_SETTINGS.showReleaseNotes = true;
    beforeAll(() => {
        fetchMockJest.get("https://hs.de/.well-known/matrix/client", {
            federation: {},
        });
        getBaseClient({
            ...getClientUserMocks(),
            getHomeserverUrl() {
                return "";
            },
        });
    });

    it("should add no dialogs if celebration is turned off", async () => {
        mockFeatureFlag(BwiFeature.Celebration, false);
        const dialogs = await getBwiDialogs();
        expect(dialogs).toHaveLength(0);
    });
    it("should add new features dialog", async () => {
        mockFeatureFlag(BwiFeature.Celebration, true);
        jest.spyOn(ReleaseNotesUtils, "getBwiReleaseNotes").mockResolvedValueOnce(NEW_FEATURE_DIALOG);

        const dialogs = await getBwiDialogs();
        expect(dialogs).toHaveLength(1);
        expect(dialogs[0].notificationType).toEqual(BwiNotificationTypes.HINT_RELEASE_NOTES);
    });
    it("should add analytics dialog and new features in the correct order", async () => {
        mockFeatureFlag(BwiFeature.Celebration, true);
        jest.spyOn(BwiAnalyticsManager, "canSeeOption").mockReturnValueOnce(true);
        jest.spyOn(ReleaseNotesUtils, "getBwiReleaseNotes").mockResolvedValueOnce(NEW_FEATURE_DIALOG);

        const dialogs = await getBwiDialogs();
        expect(dialogs).toHaveLength(2);
        expect(dialogs[0].notificationType).toEqual(BwiNotificationTypes.HINT_ANALYTICS_NOTIFICATION);
        expect(dialogs[1].notificationType).toEqual(BwiNotificationTypes.HINT_RELEASE_NOTES);
    });
    it("should not add new feature dialog if release notes are invalid", async () => {
        mockFeatureFlag(BwiFeature.Celebration, true);
        jest.spyOn(ReleaseNotesUtils, "getBwiReleaseNotes").mockResolvedValueOnce(null);

        const dialogs = await getBwiDialogs();
        expect(dialogs).toHaveLength(0);
    });
    it("should not add new feature dialog if release notes throw an error but add analytics dialog anyway", async () => {
        mockFeatureFlag(BwiFeature.Celebration, true);
        jest.spyOn(BwiAnalyticsManager, "canSeeOption").mockReturnValueOnce(true);
        jest.spyOn(ReleaseNotesUtils, "getBwiReleaseNotes").mockRejectedValueOnce(null);

        const dialogs = await getBwiDialogs();
        expect(dialogs).toHaveLength(1);
        expect(dialogs[0].notificationType).toEqual(BwiNotificationTypes.HINT_ANALYTICS_NOTIFICATION);
    });
});
