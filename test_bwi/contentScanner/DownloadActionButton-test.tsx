// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import DownloadActionButton from "../../src/components/views/messages/DownloadActionButton";
import { enableContentScanner } from "../mock/ContentScanner";
import { FileDownloader } from "../../src/utils/FileDownloader";
import { act } from "react-dom/test-utils"; // eslint-disable-line deprecate/import
import { fireEvent, render, screen } from "@testing-library/react";

const scanSourceMock = jest.fn();
const scanThumbnailMock = jest.fn();
const MediaMock = jest.fn().mockImplementation(() => {
    return {
        media: {
            scanSource: scanSourceMock,
            scanThumbnail: scanThumbnailMock,
        },
        sourceBlob: {
            value: "thisisablob",
        },
    };
});

describe("ContentScanner: DownloadActionButton", () => {
    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    it("should mark file as safe as default", () => {
        render(<DownloadActionButton mediaEventHelperGet={MediaMock} mxEvent={undefined!} />);
        // Expect: Button should have 'Download' is text, if the file is safe
        expect(screen.queryByRole("button", { name: "Download" })).toBeInTheDocument();
    });

    it("should mark the file as unsafe after trying to download unsafe content", async () => {
        enableContentScanner();
        const downloadSpy = jest.spyOn(FileDownloader.prototype, "download").mockResolvedValue();

        scanSourceMock.mockResolvedValue("unsafe");

        await act(async () => {
            render(<DownloadActionButton mediaEventHelperGet={MediaMock} mxEvent={undefined!} />);
        });

        await act(async () => {
            fireEvent.click(screen.getByRole("button"));
        });

        expect(scanSourceMock).toHaveBeenCalled();
        // download should have not been called on unsafe content
        expect(downloadSpy.mock.calls.length).toBe(0);
        expect(screen.getByRole("button").hasAttribute("disabled")).toBeTruthy();
        expect(screen.getByLabelText("Blocked")).toBeInTheDocument();
    });

    it("should mark the file as request failed when request to scanner fails", async () => {
        enableContentScanner();
        const downloadSpy = jest.spyOn(FileDownloader.prototype, "download").mockResolvedValue();

        scanSourceMock.mockResolvedValue("request-failed");

        await act(async () => {
            render(<DownloadActionButton mediaEventHelperGet={MediaMock} mxEvent={undefined!} />);
        });
        expect(screen.getByRole("button").hasAttribute("disabled")).toBeTruthy();
        await act(async () => {
            fireEvent.click(screen.getByRole("button"));
        });

        expect(scanSourceMock).toHaveBeenCalled();
        // download should have not been called on unsafe content
        expect(downloadSpy.mock.calls.length).toBe(0);
        expect(screen.getByLabelText("Connection error. Please try again later.")).toBeInTheDocument();
    });

    it("should mark the file as not found when request to scanner fails", async () => {
        enableContentScanner();
        const downloadSpy = jest.spyOn(FileDownloader.prototype, "download").mockResolvedValue();

        scanSourceMock.mockResolvedValue("not-found");

        await act(async () => {
            render(<DownloadActionButton mediaEventHelperGet={MediaMock} mxEvent={undefined!} />);
        });
        expect(screen.getByRole("button").hasAttribute("disabled")).toBeTruthy();
        await act(async () => {
            fireEvent.click(screen.getByRole("button"));
        });

        expect(scanSourceMock).toHaveBeenCalled();
        // download should have not been called on unsafe content
        expect(downloadSpy.mock.calls.length).toBe(0);
        expect(screen.getByLabelText("File is unavailable")).toBeInTheDocument();
    });

    it("should not revalidate a file that was already marked as safe", async () => {
        enableContentScanner();

        scanSourceMock.mockResolvedValue("safe");

        await act(async () => {
            render(<DownloadActionButton mediaEventHelperGet={MediaMock} mxEvent={undefined!} />);
        });

        expect(scanSourceMock).toHaveBeenCalled();
        expect(screen.getByRole("button").hasAttribute("disabled")).toBeFalsy();
        // When: Source was already downloaded and marked as safe

        scanSourceMock.mockClear();
        scanThumbnailMock.mockClear();

        await act(async () => {
            fireEvent.click(screen.getByRole("button"));
        });

        // Expect: Content scanning should not be emitted again
        expect(scanSourceMock).not.toHaveBeenCalled();
    });
});
