// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, render, screen } from "@testing-library/react";
import React from "react";
import { Playback } from "../../src/audio/Playback";
import { PlaybackManager } from "../../src/audio/PlaybackManager";
import MAudioBody from "../../src/components/views/messages/MAudioBody";
import * as TestUtils from "../../test/test-utils";
import { enableContentScanner } from "../mock/ContentScanner";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { ScanResult } from "../../src/bwi/content/ContentScanner";
import { getMediaMock } from "./ContentScannerHelper";

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
    },
}));

jest.mock("../../src/stores/spaces/SpaceStore", () => ({
    spacesEnabled: false,
}));

jest.mock("../../src/components/views/messages/MVoiceMessageBody", () => <>mvoicemessage</>);

jest.mock("../../src/components/views/audio_messages/AudioPlayer", () => (props: unknown) => (
    <div className="audioPlayer">audioplayer</div>
));

const ev = TestUtils.mkEvent({
    type: "m.room.message",
    room: "room_id",
    user: "sender",
    content: {
        body: "test.mp3",
        info: {
            size: 1,
            mimetype: "audio/mpeg",
        },
        msgtype: "m.audio",
        file: {
            url: "mxc://localhost/ABC",
            mimetype: "audio/mpeg",
        },
    },
    event: true,
});

describe("ContentScanner: MAudioBody", () => {
    getBaseClient({
        ...getClientUserMocks(),
        getHomeserverUrl: () => "",
    });

    const audioPlaybackSpy = jest.spyOn(PlaybackManager.instance, "createPlaybackInstance").mockReturnValue({
        on: () => false,
        clockInfo: {
            populatePlaceholdersFrom: () => false,
        },
        destroy: () => true,
    } as unknown as Playback);

    afterAll(() => {
        audioPlaybackSpy.mockRestore();
        jest.clearAllMocks();
        jest.resetModules();
    });

    async function renderComponent(scanResult: ScanResult) {
        await act(async () => {
            render(
                <MAudioBody
                    mxEvent={ev}
                    mediaEventHelper={getMediaMock(scanResult)}
                    onHeightChanged={() => {}}
                    onMessageAllowed={() => {}}
                    permalinkCreator={undefined}
                />,
            );
        });
    }

    it("should render audioplayer by default", async () => {
        await renderComponent("safe");

        expect(screen.queryByText("audioplayer")).toBeInTheDocument();
    });

    it("should not render audioplayer when unsafe, and default to unsafe file body", async () => {
        enableContentScanner();

        await renderComponent("unsafe");

        expect(screen.queryByText("Blocked")).toBeInTheDocument();
    });

    it("should render filebody with scan error when contentscanner doesn't find file", async () => {
        enableContentScanner();

        await renderComponent("not-found");

        expect(screen.queryByText("File is unavailable")).toBeInTheDocument();
    });

    it("should render filebody with scan error when contentscanner has network issues", async () => {
        enableContentScanner();

        await renderComponent("request-failed");

        expect(screen.queryByText("Connection error. Please try again later.")).toBeInTheDocument();
    });

    it("should render audioplayer when file is scanned and safe", async () => {
        enableContentScanner();

        await renderComponent("safe");

        expect(screen.queryByText("audioplayer")).toBeInTheDocument();
    });
});
