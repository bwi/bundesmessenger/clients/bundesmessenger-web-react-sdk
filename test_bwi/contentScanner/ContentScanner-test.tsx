// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import fetchMock from "fetch-mock-jest";
import { EncryptedFile } from "matrix-js-sdk/src/types";
import SdkConfig from "../../src/SdkConfig";
import ContentScanner, { ContentScanError, ContentScannerClass, ScanError } from "../../src/bwi/content/ContentScanner";
import { enableContentScanner } from "../mock/ContentScanner";
import { getBaseClient } from "../mock/MockClient";

const setRecipientKeyMock = jest.fn((key) => !!key);
const PkEncryptionMock = jest.fn().mockImplementation(() => {
    return {
        set_recipient_key: setRecipientKeyMock,
        encrypt: jest.fn((file) => file),
    };
});
const olmInitMock = jest.fn().mockResolvedValue(true);

const MxcMock = "12345";

const cleanFileMock = {
    url: "mxc://scanner/cleanfile",
    key: {
        alg: "a",
        key_ops: [],
        kty: "",
        k: "",
        ext: false,
    },
    iv: "",
    hashes: {
        a: "a",
    },
    v: "",
} as EncryptedFile;

const publicKeyResponse = {
    public_key: "a",
};

function mockScanResponse(status: number, body: any) {
    fetchMock.post(/^https?:\/\/scanner.*scan_encrypted$/, { status, body });
}
function mockScanError(status: number, error: ScanError) {
    mockScanResponse(status, { reason: error, info: "Error" });
}
function mockDownloadResponse(status: number, body: any) {
    fetchMock.post(/^https?:\/\/scanner.*download_encrypted$/, { status, body });
}
function mockDownloadError(status: number, error: ScanError) {
    mockDownloadResponse(status, { reason: error, info: "Error" });
}

describe("ContentScanner", () => {
    beforeAll(() => {
        // @ts-ignore
        global.Olm = {};
        global.Olm.PkEncryption = PkEncryptionMock;
        global.Olm.init = olmInitMock;

        getBaseClient({
            getHomeserverUrl() {
                return "https://scanner";
            },
        });
    });

    beforeEach(async () => {
        localStorage.clear();
        fetchMock.mockReset();
        fetchMock.get(/^https?:\/\/scanner.*$/, (url) => {
            if (url.endsWith("/media_proxy/unstable/public_key")) {
                return publicKeyResponse;
            }

            return {};
        });
        enableContentScanner();
    });

    describe("instance", () => {
        it("should have a global instance", () => {
            expect(ContentScanner.instance).toBeInstanceOf(ContentScannerClass);
        });
        it("should be disabled by default", () => {
            SdkConfig.put({});
            expect(ContentScanner.instance.canEnable).toBe(false);
        });
        it("should be enabled by config", () => {
            expect(ContentScanner.instance.canEnable).toBe(true);
        });
    });

    describe("setup", () => {
        it("should load public key before any request", async () => {
            const scanner = new ContentScannerClass();

            mockScanResponse(200, { clean: true });

            await scanner.scan(MxcMock, cleanFileMock);

            expect(scanner["key"]).toBeTruthy();
        });
    });

    describe("scan", () => {
        let scanner: ContentScannerClass;
        beforeEach(() => {
            scanner = new ContentScannerClass();
        });
        it("safe", async () => {
            mockScanResponse(200, { clean: true });
            const scanResult = await scanner.scan(MxcMock, cleanFileMock);
            expect(scanResult).toBe("safe");
        });
        it("unsafe", async () => {
            mockScanResponse(200, { clean: false });
            const scanResult = await scanner.scan(MxcMock, cleanFileMock);
            expect(scanResult).toBe("unsafe");
        });
        it("not-found", async () => {
            mockScanError(404, ScanError.NotFound);
            const scanResult = await scanner.scan(MxcMock, cleanFileMock);
            expect(scanResult).toBe("not-found");
        });
        it("unknown if unknown response", async () => {
            mockScanError(505, "unknown reason" as any);
            const scanResult = await scanner.scan(MxcMock, cleanFileMock);
            expect(scanResult).toBe("unknown");
        });
        it.each([
            [400, ScanError.Malformed],
            [400, ScanError.DecryptFailed],
            [403, ScanError.BadDecryption],
            [502, ScanError.RequestFailed],
        ])("request-failed when %s %s", async (status, reason) => {
            mockScanError(status, reason);
            const scanResult = await scanner.scan(MxcMock, cleanFileMock);
            expect(scanResult).toBe("request-failed");
        });
    });

    describe("download", () => {
        let scanner: ContentScannerClass;
        beforeEach(() => {
            scanner = new ContentScannerClass();
        });

        it("should return the response if not issues occurred during scanning", async () => {
            mockDownloadResponse(200, "file content");

            const scanResult = await scanner.download(MxcMock, cleanFileMock);

            expect(await scanResult.text()).toEqual("file content");
        });

        it("should throw a ContentScanError if content scanner response unsuccessfully", async () => {
            mockDownloadError(404, ScanError.NotFound);

            await expect(scanner.download(MxcMock, cleanFileMock)).rejects.toThrow(ContentScanError);
        });
    });
});
