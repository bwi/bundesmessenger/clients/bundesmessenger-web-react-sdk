// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, render, screen } from "@testing-library/react";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import RoomSummaryCard from "../../src/components/views/right_panel/RoomSummaryCard";
import React from "react";
import { PendingEventOrdering, Room, RoomMember } from "matrix-js-sdk/src/matrix";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { UserVerificationStatus } from "matrix-js-sdk/src/crypto-api";
import { mockDevice, mockDeviceMap, mockDeviceVerificationStatus } from "../mock/MockDevice";
import { mockRoomWithEvents } from "../mock/MockRoom";
import DMRoomMap from "../../src/utils/DMRoomMap";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import UserInfo from "../../src/components/views/right_panel/UserInfo";
import { RightPanelPhases } from "../../src/stores/right-panel/RightPanelStorePhases";
import UserProfileSettings from "../../src/components/views/settings/UserProfileSettings";
import { RoomGeneralContextMenu } from "../../src/components/views/context_menus/RoomGeneralContextMenu";
import RoomContextMenu from "../../src/components/views/context_menus/RoomContextMenu";

const ROOM_ID = "#room:server.org";
const USER_ID = "@alice:server.org";

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
        off: () => true,
        getTagsForRoom: () => [],
    },
}));

describe("BwiFeature.hidePermalinkCreation", () => {
    afterAll(jest.clearAllMocks);

    const { client, room, member } = mockTest();

    describe("RoomSummaryCard", () => {
        const renderSummary = () => {
            render(
                <MatrixClientContext.Provider value={client}>
                    <RoomSummaryCard room={room} permalinkCreator={undefined!} />
                </MatrixClientContext.Provider>,
            );
        };

        it("ff not set, render permalink", () => {
            renderSummary();
            screen.debug();
            expect(screen.queryByText("Share room")).toBeInTheDocument();
        });

        it("ff set, don't render permalink", () => {
            const flag = mockFeatureFlag(BwiFeature.HidePermalinkCreation, true);
            renderSummary();
            expect(screen.queryByText("Share room")).not.toBeInTheDocument();
            flag.mockRestore();
        });
    });

    describe("UserInfo", () => {
        const renderUserInfo = async () => {
            await act(async () => {
                render(
                    <MatrixClientContext.Provider value={client}>
                        <UserInfo
                            room={room}
                            phase={RightPanelPhases.RoomMemberInfo}
                            user={member}
                            onClose={() => {}}
                        />
                    </MatrixClientContext.Provider>,
                );
            });
        };

        it("ff not set, render permalink", async () => {
            await renderUserInfo();
            screen.debug();
            expect(screen.queryByText("Share profile")).toBeInTheDocument();
        });

        it("ff set, don't render permalink", async () => {
            const flag = mockFeatureFlag(BwiFeature.HidePermalinkCreation, true);
            await renderUserInfo();
            expect(screen.queryByText("Share profile")).not.toBeInTheDocument();
            flag.mockRestore();
        });
    });

    describe("ProfileSettings", () => {
        const renderProfileSetitngs = async () => {
            await act(async () => {
                render(
                    <MatrixClientContext.Provider value={client}>
                        <UserProfileSettings canSetAvatar={false} canSetDisplayName={false} />
                    </MatrixClientContext.Provider>,
                );
            });
        };

        it("ff not set, render permalink", async () => {
            await renderProfileSetitngs();
            expect(screen.queryByText("Share link to profile")).toBeInTheDocument();
        });

        it("ff set, don't render permalink", async () => {
            const flag = mockFeatureFlag(BwiFeature.HidePermalinkCreation, true);
            await renderProfileSetitngs();
            expect(screen.queryByText("Share link to profile")).not.toBeInTheDocument();
            flag.mockRestore();
        });
    });

    describe("RoomGeneralContextMenu", () => {
        const renderRoomGeneralContextMenu = async () => {
            await act(async () => {
                render(
                    <MatrixClientContext.Provider value={client}>
                        <RoomGeneralContextMenu room={room!} onFinished={() => {}} />
                    </MatrixClientContext.Provider>,
                );
            });
        };

        it("ff not set, render permalink", async () => {
            await renderRoomGeneralContextMenu();
            expect(screen.queryByText("Share link to Room")).toBeInTheDocument();
        });

        it("ff set, don't render permalink", async () => {
            const flag = mockFeatureFlag(BwiFeature.HidePermalinkCreation, true);
            await renderRoomGeneralContextMenu();
            expect(screen.queryByText("Share link to Room")).not.toBeInTheDocument();
            flag.mockRestore();
        });
    });

    describe("RoomContextMenu", () => {
        const renderRoomContextMenu = async () => {
            await act(async () => {
                render(
                    <MatrixClientContext.Provider value={client}>
                        <RoomContextMenu room={room!} onFinished={() => {}} />
                    </MatrixClientContext.Provider>,
                );
            });
        };

        it("ff not set, render permalink", async () => {
            await renderRoomContextMenu();
            expect(screen.queryByText("Share link to Room")).toBeInTheDocument();
        });

        it("ff set, don't render permalink", async () => {
            const flag = mockFeatureFlag(BwiFeature.HidePermalinkCreation, true);
            await renderRoomContextMenu();
            expect(screen.queryByText("Share link to Room")).not.toBeInTheDocument();
            flag.mockRestore();
        });
    });

    function mockTest() {
        // eslint-disable-next-line prefer-const
        let room: Room;
        const client = getBaseClient({
            ...getClientUserMocks(),
            isRoomEncrypted() {
                return true;
            },
            checkUserTrust() {
                return new UserVerificationStatus(true, true, true);
            },
            getRoom() {
                return room;
            },
            getCrypto() {
                return {
                    async getUserDeviceInfo() {
                        return mockDeviceMap(USER_ID, mockDevice(USER_ID));
                    },
                    async getDeviceVerificationStatus() {
                        return mockDeviceVerificationStatus(true);
                    },
                    async getUserVerificationStatus() {
                        return new UserVerificationStatus(true, true, true);
                    },
                };
            },
            store: {
                async getPendingEvents() {
                    return [];
                },
            },
            getEventMapper() {
                return {};
            },
        });
        room = mockRoomWithEvents(
            {
                partials: { roomId: ROOM_ID, getJoinRule: jest.fn().mockReturnValue("public") },
                opts: {
                    pendingEventOrdering: PendingEventOrdering.Detached,
                },
            },
            client,
        );
        DMRoomMap.makeShared(client);

        return { client, room, member: new RoomMember(ROOM_ID, "@alice:server.org") };
    }
});
