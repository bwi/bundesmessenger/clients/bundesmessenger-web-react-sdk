// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { render, screen } from "@testing-library/react";
import { Room } from "matrix-js-sdk/src/matrix";
import React from "react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import SecurityRoomSettingsTab from "../../src/components/views/settings/tabs/room/SecurityRoomSettingsTab";
import DMRoomMap from "../../src/utils/DMRoomMap";
import * as testUtils from "../../test/test-utils";
import { getBaseClient } from "../mock/MockClient";
import { mockRoomWithEvents } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";

jest.mock("../../src/components/views/settings/JoinRuleSettings", () => () => <div>Mock</div>);

describe("BwiFeature.hideRoomAccessSettings", () => {
    let room: Room | null = null;

    const client = getBaseClient({
        ...testUtils.mockClientMethodsUser(),
        doesServerSupportUnstableFeature: () => Promise.resolve(false),
        getLocalAliases: () => Promise.resolve([]),
        getRoom: () => room,
        isRoomEncrypted: () => false,
    });
    room = mockRoomWithEvents(
        {
            partials: {
                name: "My Room",
                currentState: {
                    mayClientSendStateEvent: () => false,
                },
                canInvite: () => false,
            },
        },
        client,
    );

    beforeEach(function () {
        DMRoomMap.makeShared(client);
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    function renderComponent() {
        render(
            <MatrixClientContext.Provider value={client}>
                <SecurityRoomSettingsTab room={room!} closeSettingsFn={jest.fn} />
            </MatrixClientContext.Provider>,
        );
    }

    it("should show room join settings if feature is disabled", () => {
        const featureFlag = mockFeatureFlag(BwiFeature.HideRoomAccessSettings, false);

        renderComponent();

        expect(screen.queryByText("Decide who can join My Room.")).toBeInTheDocument();

        featureFlag.mockRestore();
    });

    it("should hide room join settings if feature is enabled", () => {
        const featureFlag = mockFeatureFlag(BwiFeature.HideRoomAccessSettings, true);

        renderComponent();

        expect(screen.queryByText("Decide who can join My Room.")).not.toBeInTheDocument();

        featureFlag.mockRestore();
    });
});
