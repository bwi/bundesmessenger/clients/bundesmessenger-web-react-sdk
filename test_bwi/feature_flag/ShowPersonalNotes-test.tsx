// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, render, screen, cleanup, fireEvent } from "@testing-library/react";
import { MatrixClient, PendingEventOrdering, Room } from "matrix-js-sdk/src/matrix";
import React from "react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import _RoomList from "../../src/components/views/rooms/RoomList";
import { SDKContext, SdkContextClass } from "../../src/contexts/SDKContext";
import { ListAlgorithm, SortAlgorithm } from "../../src/stores/room-list/algorithms/models";
import { SpaceFilterCondition } from "../../src/stores/room-list/filters/SpaceFilterCondition";
import RoomListStore, { RoomListStoreClass } from "../../src/stores/room-list/RoomListStore";
import DMRoomMap from "../../src/utils/DMRoomMap";
import ResizeNotifier from "../../src/utils/ResizeNotifier";
import * as testUtils from "../../test/test-utils";
import { getMockClientWithEventEmitter, mockClientMethodsUser, unmockClientPeg } from "../../test/test-utils";
import { mockRoomWithEvents } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { mockClientRoomFunctions } from "../test-utils/client";
import { DefaultTagID } from "../../src/stores/room-list/models";
import { RoomGeneralContextMenu } from "../../src/components/views/context_menus/RoomGeneralContextMenu";
import SettingsStore from "../../src/settings/SettingsStore";
function TestComponent({ stores }: { stores: SdkContextClass }) {
    const Wrapped = testUtils.wrapInMatrixClientContext(_RoomList);
    return (
        <SDKContext.Provider value={stores}>
            <Wrapped
                onResize={() => {}}
                onKeyDown={jest.fn}
                onFocus={jest.fn}
                onBlur={jest.fn}
                resizeNotifier={new ResizeNotifier()}
                isMinimized={false}
                activeSpace=""
            />
        </SDKContext.Provider>
    );
}

async function setupRoomListStore(client: MatrixClient) {
    const roomListStore = RoomListStore.instance as RoomListStoreClass;

    jest.spyOn(roomListStore, "getTagSorting").mockImplementation(() => SortAlgorithm.Alphabetic);
    jest.spyOn(roomListStore, "setTagSorting").mockImplementation(() => {});

    jest.spyOn(roomListStore, "getListOrder").mockImplementation(() => ListAlgorithm.Natural);
    jest.spyOn(roomListStore, "setListOrder").mockImplementation(() => {});

    await roomListStore.makeReady(client);
    return roomListStore;
}

describe("BwiFeature.showPersonalNotes default is to hide notes room", () => {
    let client: MatrixClient;
    let rooms: Room[];

    beforeAll(function () {
        client = getMockClientWithEventEmitter({
            ...mockClientMethodsUser("@me:domain"),
            getEventMapper: jest.fn().mockReturnValue(() => {}),
            getUserId: jest.fn(),
        });
        client.store = { getPendingEvents: jest.fn().mockResolvedValue([]) } as unknown as (typeof client)["store"];
        DMRoomMap.makeShared(client);

        rooms = [
            mockRoomWithEvents(
                {
                    opts: {
                        pendingEventOrdering: PendingEventOrdering.Detached,
                    },
                },
                client,
            ),
        ];
        rooms[0].tags = {
            [DefaultTagID.Favourite]: {},
        };
        rooms[0].getMyMembership = jest.fn().mockReturnValue("join");
        rooms[0].name = "Personal Notes Room";
        client.getVisibleRooms = jest.fn().mockReturnValue(rooms);
        client.getAccountData = jest.fn().mockReturnValue({
            getContent: () => ({
                room_id: rooms[0].roomId,
            }),
        });
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
        unmockClientPeg();
    });

    describe("RoomList", () => {
        jest.spyOn(SpaceFilterCondition.prototype, "isVisible").mockReturnValue(true);

        async function renderComponent() {
            await act(async () => {
                const stores = new SdkContextClass();
                stores.client = client;

                render(<TestComponent stores={stores} />);
            });
        }

        beforeEach(() => {
            mockClientRoomFunctions(client, rooms);
        });

        afterEach(() => {
            cleanup();
        });

        it("should hide the personal notes room in Room List if feature disabled", async () => {
            await setupRoomListStore(client);
            await renderComponent();
            expect(screen.queryByText("Personal Notes Room")).not.toBeInTheDocument();
        });

        it("should show the personal notes room in RoomList if feature enabled", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.ShowPersonalNotes, true);
            await setupRoomListStore(client);
            await renderComponent();
            expect(screen.getByText("Personal Notes Room")).toBeInTheDocument();

            featureFlag.mockRestore();
        });
    });

    describe("RoomGeneralContextMenu", () => {
        beforeEach(() => {
            SettingsStore.setValue = jest.fn();
            mockClientRoomFunctions(client, rooms);
        });

        async function renderComponent() {
            const Wrapped = testUtils.wrapInMatrixClientContext(RoomGeneralContextMenu);

            await act(async () => {
                render(<Wrapped room={rooms[0]} onFinished={jest.fn()} />);
            });
        }

        it("personal notes room, leave button should trigger show feature flag", async () => {
            await renderComponent();
            const leaveButton = screen.getByText("Hide personal notes room");

            expect(leaveButton).toBeInTheDocument();

            fireEvent.click(leaveButton);
            expect(SettingsStore.setValue).toHaveBeenCalledWith("BwiFeature.showPersonalNotes", null, "account", false);
        });

        it("no personal notes room, leave button should leave room", async () => {
            client.getAccountData = jest.fn().mockReturnValue({
                getContent: () => ({
                    room_id: "#somethingelse",
                }),
            });

            await renderComponent();
            const leaveButton = screen.getByText("Leave");
            expect(leaveButton).toBeInTheDocument();
            fireEvent.click(leaveButton);
            expect(SettingsStore.setValue).not.toHaveBeenCalled();
        });
    });
});
