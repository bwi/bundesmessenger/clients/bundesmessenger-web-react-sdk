// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { render, screen } from "@testing-library/react";
import React from "react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import PreferencesUserSettingsTab from "../../src/components/views/settings/tabs/user/PreferencesUserSettingsTab";
import SecurityUserSettingsTab from "../../src/components/views/settings/tabs/user/SecurityUserSettingsTab";
import PlatformSettingsHandler from "../../src/settings/handlers/PlatformSettingsHandler";
import * as testUtils from "../../test/test-utils";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { Mocked } from "jest-mock";
import { TestSdkContext } from "../../test/TestSdkContext";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import { SDKContext } from "../../src/contexts/SDKContext";
import { CryptoApi } from "matrix-js-sdk/src/crypto-api";
import { mockPlatformPeg } from "../../test/test-utils";

jest.mock("../../src/stores/spaces/SpaceStore", () => ({
    spacesEnabled: false,
}));

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
    },
}));

jest.mock("../../src/PlatformPeg", () => ({
    get: () => ({
        getEventIndexingManager: jest.fn().mockReturnValue(undefined),
        supportsAutoLaunch: jest.fn().mockResolvedValue(false),
        supportsWarnBeforeExit: jest.fn().mockResolvedValue(false),
        supportsAutoHideMenuBar: jest.fn().mockResolvedValue(false),
        supportsMinimizeToTray: jest.fn().mockResolvedValue(false),
    }),
}));

const SecurityTab = testUtils.wrapInMatrixClientContext(SecurityUserSettingsTab);

describe("BwiFeature.disableKeyExport", () => {
    describe("SecurityUserSettingsTab", () => {
        beforeAll(() => {
            const mockCrypto = {
                getDeviceVerificationStatus: jest.fn().mockResolvedValue({
                    crossSigningVerified: false,
                }),
                getCrossSigningKeyId: jest.fn(),
                getUserDeviceInfo: jest.fn().mockResolvedValue(new Map()),
                isCrossSigningReady: jest.fn().mockResolvedValue(true),
                isSecretStorageReady: jest.fn().mockResolvedValue(false),
                signedByOwner: true,
                getCrossSigningStatus: jest.fn().mockResolvedValue({
                    publicKeysOnDevice: true,
                    privateKeysInSecretStorage: true,
                    privateKeysCachedLocally: {
                        masterKey: true,
                        selfSigningKey: true,
                        userSigningKey: true,
                    },
                }),
                hasKey: () => false,
                getSessionBackupPrivateKey: () => Promise.resolve(null),
            } as unknown as Mocked<CryptoApi>;

            getBaseClient({
                ...getClientUserMocks(),
                getIgnoredUsers: () => [],
                getDeviceEd25519Key: () => null,
                isCryptoEnabled: () => false,
                getDeviceId: () => "123456",
                getVersions: () =>
                    Promise.resolve({
                        versions: [],
                        unstable_features: {},
                    }),
                getRooms: () => [],
                secretStorage: { hasKey: jest.fn() },
                getCrossSigningCacheCallbacks: () => {},
                isSecretStorageReady: () => false,
                isKeyBackupKeyStored: () => false,
                isCrossSigningReady: () => false,
                getDevices: () =>
                    Promise.resolve({
                        devices: [],
                    }),
                checkKeyBackup: () => ({
                    trustInfo: {
                        sigs: [],
                        trusted_locally: true,
                    },
                }),
                getCapabilities: () => Promise.resolve({}),
                getCrypto: () => mockCrypto,
                doesServerSupportUnstableFeature: jest.fn().mockResolvedValue(false),
            });
        });
        afterAll(() => {
            testUtils.unmockClientPeg();
        });

        const headings = ["Secure Backup", "Message search", "Cross-signing", "Cryptography"];

        const renderSecurityTab = () => {
            const context = new TestSdkContext();
            context.client = MatrixClientPeg.safeGet();
            render(
                <SDKContext.Provider value={context}>
                    <SecurityTab closeSettingsFn={jest.fn()} />
                </SDKContext.Provider>,
            );
        };

        it("should render the key export section by default", () => {
            const flag = mockFeatureFlag(BwiFeature.HideNerdyUserSettings, false);
            renderSecurityTab();

            expect(screen.queryByText("Encryption", { selector: ".mx_Heading_h3" })).toBeInTheDocument();
            headings.forEach((heading) => {
                expect(
                    screen.queryByText(heading, { selector: ".mx_SettingsSubsectionHeading_heading" }),
                ).toBeInTheDocument();
            });

            flag.mockRestore();
        });

        it("should not render the key export section with feature flag enabled", () => {
            const flag = mockFeatureFlag(BwiFeature.HideNerdyUserSettings, true);
            renderSecurityTab();

            expect(screen.queryByText("Encryption", { selector: ".mx_Heading_h3" })).not.toBeInTheDocument();
            headings.forEach((heading) => {
                expect(
                    screen.queryByText(heading, { selector: ".mx_SettingsSubsectionHeading_heading" }),
                ).not.toBeInTheDocument();
            });

            flag.mockRestore();
        });
    });

    describe("PreferencesUserSettingsTab", () => {
        beforeEach(() => {
            getBaseClient({
                doesServerSupportUnstableFeature: () => Promise.resolve(true),
                isGuest: () => false,
            });
            window.matchMedia = () =>
                ({
                    matches: false,
                }) as any;
            mockPlatformPeg();
        });

        it("should render keyboard shortcut section by default", () => {
            jest.spyOn(PlatformSettingsHandler.prototype, "isSupported").mockReturnValue(false);
            mockFeatureFlag("showCommunitiesInsteadOfSpaces", true);

            render(<PreferencesUserSettingsTab closeSettingsFn={jest.fn()} />);

            expect(
                screen.queryByText("Keyboard shortcuts", { selector: ".mx_SettingsSubsectionHeading_heading" }),
            ).toBeInTheDocument();
            expect(screen.queryByText("Show NSFW content")).toBeInTheDocument();
        });

        it("should not render keyboard shortcut section with feature flag enabled", () => {
            const ff = mockFeatureFlag(
                new Map([
                    ["showCommunitiesInsteadOfSpaces", true],
                    [BwiFeature.HideNerdyUserSettings, true],
                ]),
            );

            render(<PreferencesUserSettingsTab closeSettingsFn={jest.fn()} />);

            expect(
                screen.queryByText("Keyboard shortcuts", { selector: ".mx_SettingsSubsectionHeading_heading" }),
            ).not.toBeInTheDocument();
            expect(screen.queryByText("Show NSFW content")).not.toBeInTheDocument();
            ff.mockRestore();
        });
    });
});
