// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import {
    Room,
    Beacon,
    BeaconIdentifier,
    EventStatus,
    PendingEventOrdering,
    MatrixEvent,
} from "matrix-js-sdk/src/matrix";
import React from "react";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { IRoomState } from "../../src/components/structures/RoomView";
import MessageContextMenu from "../../src/components/views/context_menus/MessageContextMenu";
import RoomContext from "../../src/contexts/RoomContext";

import { render, screen } from "@testing-library/react";
import { makeLocationEvent } from "../../test/test-utils/location";
import { stubClient } from "../../test/test-utils/test-utils";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

describe("BwiFeature.hideOSMLink", () => {
    function renderMenu(
        mxEvent: MatrixEvent,
        props?: Partial<React.ComponentProps<typeof MessageContextMenu>>,
        context: Partial<IRoomState> = {},
        beacons: Map<BeaconIdentifier, Beacon> = new Map(),
        room: Room = makeDefaultRoom(),
    ) {
        const client = MatrixClientPeg.safeGet();

        mxEvent.setStatus(EventStatus.SENT);

        client.getUserId = jest.fn().mockReturnValue("@user:example.com");
        client.getRoom = jest.fn().mockReturnValue(room);

        return render(
            <RoomContext.Provider value={context as IRoomState}>
                <MessageContextMenu chevronFace={undefined} mxEvent={mxEvent} onFinished={jest.fn()} {...props} />,
            </RoomContext.Provider>,
        );
    }

    function makeDefaultRoom(): Room {
        return new Room("#mock", MatrixClientPeg.safeGet(), "@user:example.com", {
            pendingEventOrdering: PendingEventOrdering.Detached,
        });
    }

    const location = makeLocationEvent("geo:50,50");

    beforeEach(() => {
        jest.resetAllMocks();
        stubClient();
    });

    describe("MessageContextMenu", () => {
        it("should show OSM link by default", async () => {
            renderMenu(location);

            // exists with a href with the lat/lon from the location event
            const anchor = screen.queryByLabelText<HTMLAnchorElement>("Open in OpenStreetMap");
            expect(anchor).toBeInTheDocument();
            expect(anchor?.href).toEqual("https://www.openstreetmap.org/?mlat=50&mlon=50#map=16/50/50");
        });

        it("should hide OSM link if feature is enabled", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.HideOSMLink, true);

            renderMenu(location);
            expect(screen.queryByLabelText<HTMLAnchorElement>("Open in OpenStreetMap")).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });
    });
});
