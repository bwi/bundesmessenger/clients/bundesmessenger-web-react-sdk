// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import CreateRoomDialog from "../../src/components/views/dialogs/CreateRoomDialog";
import RoomSettingsDialog from "../../src/components/views/dialogs/RoomSettingsDialog";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { render, screen } from "@testing-library/react";
import { mockRoomWithEvents } from "../mock/MockRoom";
import { Room } from "matrix-js-sdk/src/matrix";

jest.mock("../../src/components/views/settings/tabs/room/GeneralRoomSettingsTab.tsx", () => () => <>mock</>);

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
    },
}));

jest.mock("../../src/stores/spaces/SpaceStore", () => ({
    spacesEnabled: false,
}));

describe("BwiFeature.roomSecuritySettings", () => {
    describe("CreateRoomDialog", () => {
        beforeAll(() => {
            getBaseClient({
                ...getClientUserMocks(),
                getDomain: () => "example",
                doesServerForceEncryptionForPreset: () => Promise.resolve(true),
            });
        });

        it("should render security settings by default", () => {
            render(<CreateRoomDialog defaultEncrypted={true} onFinished={jest.fn()} />);
            screen.debug();
            expect(screen.queryByText("Enable end-to-end encryption")).toBeInTheDocument();
        });

        it("should not render security settings if feature flag is enabled", () => {
            const featureFlag = mockFeatureFlag(BwiFeature.RoomSecuritySettings, false);
            render(<CreateRoomDialog defaultEncrypted={true} onFinished={jest.fn()} />);
            expect(screen.queryByText("Enable end-to-end encryption")).not.toBeInTheDocument();
            featureFlag.mockRestore();
        });
    });

    describe("RoomSettingsDialog", () => {
        let room: Room | null = null;

        beforeEach(() => {
            getBaseClient({
                ...getClientUserMocks(),
                doesServerForceEncryptionForPreset: () => Promise.resolve(true),
                doesServerSupportUnstableFeature: () => Promise.resolve(false),
                getRoomDirectoryVisibility: () => Promise.resolve({ visibility: "public" }),
                getRoom: () => room,
            });
            room = mockRoomWithEvents({});
        });

        afterEach(() => {
            jest.clearAllMocks();
            jest.resetModules();
        });

        it("should render security settings tab by default", async () => {
            render(<RoomSettingsDialog roomId={room!.roomId} onFinished={jest.fn()} />);
            expect(screen.queryByText("Security & Privacy")).toBeInTheDocument();
        });

        it("should not render security settings tab if feature flag is enabled", async () => {
            const flag = mockFeatureFlag(BwiFeature.RoomSecuritySettings, false);
            render(<RoomSettingsDialog roomId={room!.roomId} onFinished={jest.fn()} />);
            expect(screen.queryByText("Security & Privacy")).not.toBeInTheDocument();
            flag.mockRestore();
        });
    });
});
