// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";

import { act, render, screen } from "@testing-library/react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { IRoomState } from "../../src/components/structures/RoomView";
import MessageComposerButtons from "../../src/components/views/rooms/MessageComposerButtons";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import RoomContext, { TimelineRenderingType } from "../../src/contexts/RoomContext";
import { Layout } from "../../src/settings/enums/Layout";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { generateRoomId, mockRoomWithEvents } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { Room, RoomMember } from "matrix-js-sdk/src/matrix";

describe("MessageComposerButtons", () => {
    let room: Room | null = null;
    const roomId = generateRoomId();
    const client = getBaseClient({
        getRoom: () => room,
        ...getClientUserMocks(),
    });
    room = mockRoomWithEvents({
        partials: {
            getMember: (userId: string) => {
                return new RoomMember(roomId, userId);
            },
        },
    });

    const roomState = createRoomState(room);

    async function renderComponent(props?: Partial<Parameters<typeof MessageComposerButtons>[0]>) {
        await act(async () => {
            render(
                <MatrixClientContext.Provider value={client}>
                    <RoomContext.Provider value={roomState}>
                        <MessageComposerButtons
                            isMenuOpen={true}
                            toggleButtonMenu={() => {}}
                            showPollsButton={true}
                            addEmoji={jest.fn()}
                            haveRecording={false}
                            isStickerPickerOpen={false}
                            menuPosition={undefined}
                            onRecordStartEndClick={jest.fn()}
                            setStickerPickerOpen={jest.fn()}
                            showLocationButton={false}
                            showStickersButton={false}
                            showVoiceBroadcastButton={false}
                            isRichTextEnabled={false}
                            onComposerModeClick={() => {}}
                            onStartVoiceBroadcastClick={() => {}}
                            {...props}
                        />
                    </RoomContext.Provider>
                </MatrixClientContext.Provider>,
            );
        });
    }
    describe("BwiFeature.disablePoll", () => {
        it("renders poll button by default", async () => {
            await renderComponent();
            expect(screen.queryByRole("menuitem", { name: "Poll" })).toBeInTheDocument();
        });

        it("doesnt render poll button while feature flag is enabled", async () => {
            const ff = mockFeatureFlag(BwiFeature.DisablePoll, true);

            await renderComponent();

            expect(screen.queryByRole("menuitem", { name: "Poll" })).not.toBeInTheDocument();

            ff.mockRestore();
        });
    });

    describe("BwiFeature.disableLocationSharing", () => {
        it("renders location button by default", async () => {
            await renderComponent({ showLocationButton: true });
            expect(screen.queryByRole("menuitem", { name: "Location" })).toBeInTheDocument();
        });

        it("doesnt render location button while feature flag is enabled", async () => {
            const ff = mockFeatureFlag(BwiFeature.LocationShareTypesEnabled, []);

            await renderComponent({ showLocationButton: true });

            expect(screen.queryByRole("menuitem", { name: "Location" })).not.toBeInTheDocument();

            ff.mockRestore();
        });
    });
});

function createRoomState(room: Room): IRoomState {
    return {
        room: room,
        roomId: room.roomId,
        roomLoading: true,
        peekLoading: false,
        shouldPeek: true,
        membersLoaded: false,
        numUnreadMessages: 0,
        canPeek: false,
        showApps: false,
        isPeeking: false,
        showRightPanel: true,
        joining: false,
        atEndOfLiveTimeline: true,
        showTopUnreadMessagesBar: false,
        statusBarVisible: false,
        canReact: false,
        layout: Layout.Group,
        lowBandwidth: false,
        alwaysShowTimestamps: false,
        showTwelveHourTimestamps: false,
        readMarkerInViewThresholdMs: 3000,
        readMarkerOutOfViewThresholdMs: 30000,
        showReadReceipts: true,
        showRedactions: true,
        showJoinLeaves: true,
        showAvatarChanges: true,
        showDisplaynameChanges: true,
        matrixClientIsReady: false,
        timelineRenderingType: TimelineRenderingType.Room,
        liveTimeline: undefined,
        canSendMessages: true,
        narrow: true,
        resizing: false,
        showHiddenEvents: false,
        activeCall: null,
        canSelfRedact: false,
        msc3946ProcessDynamicPredecessor: false,
        threadRightPanel: false,
        canAskToJoin: false,
        promptAskToJoin: false,
        viewRoomOpts: { buttons: [] },
    };
}
