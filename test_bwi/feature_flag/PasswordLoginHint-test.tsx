// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, render, screen } from "@testing-library/react";
import fetchMockJest from "fetch-mock-jest";
import React from "react";
import SdkConfig from "../../src/SdkConfig";
import Login from "../../src/components/structures/auth/Login";
import { mkServerConfig } from "../../test/test-utils";

describe("BwiFeature.PasswordLoginHint", () => {
    beforeEach(() => {
        fetchMockJest.mockClear();

        fetchMockJest.get("https://matrix.org/_matrix/client/versions", {
            unstable_features: {},
            versions: ["v1.1"],
        });

        fetchMockJest.get("https://vector.im/_matrix/identity/v2", {});

        fetchMockJest.get("https://matrix.org/_matrix/client/v3/login", {
            flows: [
                {
                    type: "m.login.password",
                },
            ],
        });

        fetchMockJest.get("https://loltrix.org/.well-known/matrix/client", {
            status: 500,
        });
    });

    async function renderComponent() {
        await act(async () => {
            render(
                <Login
                    serverConfig={mkServerConfig("https://matrix.org", "https://vector.im")}
                    onLoggedIn={() => {}}
                    onRegisterClick={() => {}}
                    onServerConfigChange={() => {}}
                />,
            );
        });
    }

    describe("enabled", () => {
        it("should show a login hint", async () => {
            SdkConfig.put({
                brand: "brand",
                bwi_password_login_hint: {
                    en: "123 is not a good password",
                },
            });

            await renderComponent();

            expect(screen.queryByText("123 is not a good password")).toBeInTheDocument();
        });

        it("should not show a login hint if no value for the current language was configured", async () => {
            SdkConfig.put({
                brand: "brand",
                bwi_password_login_hint: {
                    de: "123 is not a good password",
                },
            });

            await renderComponent();

            expect(screen.queryByText("123 is not a good password")).not.toBeInTheDocument();
        });
    });

    describe("disabled", () => {
        it("should not show a login hint", async () => {
            await renderComponent();

            expect(screen.queryByText("123 is not a good password")).not.toBeInTheDocument();
        });
    });
});
