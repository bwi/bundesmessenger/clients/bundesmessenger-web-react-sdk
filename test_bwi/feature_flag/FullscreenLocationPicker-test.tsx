// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { mocked } from "jest-mock";
import SdkConfig from "../../src/SdkConfig";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { LocationShareType } from "../../src/components/views/location/shareLocation";
import { getWellKnownSpy } from "../mock/MockClient";
import { createRoom } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import * as TestUtils from "../../test/test-utils";
import maplibregl from "maplibre-gl";
import _LocationPicker from "../../src/components/views/location/LocationPicker";
import { render } from "@testing-library/react";
import React from "react";
import { RoomMember } from "matrix-js-sdk/src/matrix";

describe("BwiFeature.preferLocationSharingConfig", () => {
    const client = getWellKnownSpy();

    beforeAll(() => {
        SdkConfig.put({
            map_style_url: "mock_from_config",
            brand: "MockBrand",
        });
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    describe("LocationPicker", () => {
        const renderLocationPicker = (shouldBeThere: boolean) => {
            const mockMap = new maplibregl.Map(null!);
            mocked(mockMap);
            const controlMock = jest.fn().mockImplementation(() => {
                return {};
            });
            maplibregl.FullscreenControl = controlMock;
            const WrappedLocationPicker = TestUtils.wrapInMatrixClientContext(_LocationPicker);
            render(
                <WrappedLocationPicker
                    sender={new RoomMember(createRoom(client, {}).roomId, "@me:domain")}
                    shareType={LocationShareType.Own}
                    onChoose={jest.fn()}
                    onFinished={jest.fn()}
                />,
            );
            if (shouldBeThere) expect(controlMock).toHaveBeenCalled();
            else expect(controlMock).not.toHaveBeenCalled();
        };

        it("feature flag off, prefer well known config in updateStyleUrl", async () => {
            renderLocationPicker(false);
        });

        it("feature flag on, prefer config.json config in updateStyleUrl", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.FullscreenLocationPicker, true);
            renderLocationPicker(true);
            featureFlag.mockRestore();
        });
    });
});
