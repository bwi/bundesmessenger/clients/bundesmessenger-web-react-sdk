// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import ImageView from "../../src/components/views/elements/ImageView";
import { render } from "@testing-library/react";

jest.mock("../../src/components/views/avatars/MemberAvatar", () => (props: unknown) => {
    return () => <>avatar</>;
});

jest.mock("../../src/components/views/context_menus/MessageContextMenu", () => (props: unknown) => {
    return () => <>msgctx</>;
});

describe("BwiFeature.CloseImageViewOnNavigate", () => {
    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    it("should fire onclose when browser navigates", () => {
        const featureFlag = mockFeatureFlag(BwiFeature.CloseImageViewOnNavigate, true);
        const onFinished = jest.fn();
        render(<ImageView onFinished={onFinished} />);
        window.dispatchEvent(new HashChangeEvent("hashchange"));
        expect(onFinished).toHaveBeenCalledTimes(1);
        window.dispatchEvent(new HashChangeEvent("hashchange"));
        expect(onFinished).toHaveBeenCalledTimes(2);
        featureFlag.mockRestore();
    });
});
