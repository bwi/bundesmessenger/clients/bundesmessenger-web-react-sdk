// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { render, screen } from "@testing-library/react";
import { EventType, PendingEventOrdering, Room } from "matrix-js-sdk/src/matrix";
import React from "react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import RoomContextMenu from "../../src/components/views/context_menus/RoomContextMenu";
import { RoomGeneralContextMenu } from "../../src/components/views/context_menus/RoomGeneralContextMenu";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import DMRoomMap from "../../src/utils/DMRoomMap";
import * as TestUtils from "../../test/test-utils";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { generateRoomId, mockRoomWithEvents } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { mockUser } from "../mock/MockUser";

jest.mock("../../src/voice-broadcast/hooks/useHasRoomLiveVoiceBroadcast", () => ({
    useHasRoomLiveVoiceBroadcast: () => false,
}));

jest.mock("../../src/utils/DMRoomMap"),
    () => ({
        shared: () => ({
            getUserIdForRoomId: () => null,
        }),
    });

jest.mock("../../src/components/views/avatars/DecoratedRoomAvatar", () => () => {
    return () => <>avatar</>;
});

jest.mock("../../src/stores/spaces/SpaceStore", () => ({
    spacesEnabled: false,
}));

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
        off: () => true,
        getTagsForRoom: () => [],
    },
}));

describe("BwiFeature.hideLowPriorityRoomOption", () => {
    const user = mockUser;
    let room: Room | null = null;
    const client = getBaseClient({
        ...getClientUserMocks(user),
        getRoom: () => room,
        store: {
            getPendingEvents: () => Promise.resolve([]),
        },
        getEventMapper: () => ({}),
    });

    const roomId = generateRoomId();
    room = mockRoomWithEvents({
        partials: {
            canInvite: () => false,
            getMyMembership: () => "join",
        },
        events: [
            TestUtils.mkEvent({
                event: true,
                type: EventType.RoomPredecessor,
                content: {
                    predecessor: {
                        room_id: "trash",
                    },
                },
                room: roomId,
                user: user.userId,
                skey: "",
            }),
        ],
        opts: {
            pendingEventOrdering: PendingEventOrdering.Detached,
        },
    });

    beforeEach(() => {
        DMRoomMap.makeShared(client);
    });

    describe("RoomGeneralContextMenu", () => {
        function renderComponent() {
            render(
                <MatrixClientContext.Provider value={client}>
                    <RoomGeneralContextMenu room={room!} onFinished={() => {}} />
                </MatrixClientContext.Provider>,
            );
        }

        it("should show the option to mark a room is low prio by default", () => {
            renderComponent();

            expect(screen.queryByRole("menuitemcheckbox", { name: "Low Priority" })).toBeInTheDocument();
        });

        it("should hide the option to mark a rooom is low prio if feature is enabled", () => {
            const featureFlag = mockFeatureFlag(BwiFeature.HideLowPriorityRoomOption, true);

            renderComponent();

            expect(screen.queryByRole("menuitemcheckbox", { name: "Low Priority" })).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });
    });

    describe("RoomContextMenu", () => {
        function renderComponent() {
            render(
                <MatrixClientContext.Provider value={client}>
                    <RoomContextMenu room={room!} onFinished={() => {}} />
                </MatrixClientContext.Provider>,
            );
        }

        it("should render the option to mark rooms as low priority in context menu", () => {
            renderComponent();

            expect(screen.queryByRole("menuitemcheckbox", { name: "Low priority" })).toBeInTheDocument();
        });

        it("should not render the option to mark rooms as low priority in context menu when flag is enabled", () => {
            const featureFlag = mockFeatureFlag(BwiFeature.HideLowPriorityRoomOption, true);

            renderComponent();

            expect(screen.queryByRole("menuitemcheckbox", { name: "Low priority" })).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });
    });
});
