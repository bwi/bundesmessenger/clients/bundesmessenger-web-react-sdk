// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { render, screen } from "@testing-library/react";
import { JoinRule, PendingEventOrdering, Room, RoomMember, Visibility } from "matrix-js-sdk/src/matrix";
import React from "react";
import WrappedRoomSettingsDialog from "../../src/components/views/dialogs/RoomSettingsDialog";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { mockRoomWithEvents } from "../mock/MockRoom";
import { stubClient, unmockClientPeg } from "../../test/test-utils";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import DMRoomMap from "../../src/utils/DMRoomMap";
import SdkConfig from "../../src/SdkConfig";
import { renderHook } from "@testing-library/react-hooks";
import { useRoomCall } from "../../src/hooks/room/useRoomCall";
import * as BwiClient from "../../src/bwi/client";

describe("Huddle", () => {
    const roomId = "!room:server.org";
    let room: Room | null = null;
    let client;

    beforeAll(() => {
        mockFeatureFlag("feature_group_calls", true);

        client = getBaseClient({
            getRoom: () => room,
            ...getClientUserMocks("@me:server.org"),
            isRoomEncrypted: () => false,
            getDomain: () => "server.org",
            getLocalAliases: () => Promise.resolve({ aliases: [] }),
            getRoomDirectoryVisibility: () => Promise.resolve({ visibility: Visibility.Private }),
            getOrCreateFilter: () => Promise.resolve(""),
            getEventMapper: () => ({}),
            store: {
                getPendingEvents: () => Promise.resolve([]),
            },
        });

        DMRoomMap.makeShared(client);

        SdkConfig.put({
            brand: "My App",
            element_call: {
                url: "https://call.me",
                use_exclusively: true,
            },
        });
    });

    afterAll(() => {
        unmockClientPeg();
    });

    describe("RoomSettingsDialog", () => {
        it("should hide Voice and Video Tab from RoomSettings", () => {
            room = mockRoomWithEvents({
                partials: {
                    roomId,
                },
            });

            render(<WrappedRoomSettingsDialog roomId={roomId} onFinished={() => {}} />);

            expect(screen.queryByText("Voice & Video")).not.toBeInTheDocument();
        });
    });

    describe("useRoomCall", () => {
        stubClient();
        const member = new RoomMember(roomId, "@1337:server.org");
        const room = mockRoomWithEvents({
            partials: {
                roomId,
                currentState: {
                    getJoinRule: () => JoinRule.Invite,
                },
                getMember: () => member,
                getJoinedMemberCount: () => 3,
            },
            opts: {
                pendingEventOrdering: PendingEventOrdering.Detached,
            },
        });

        it("new vector client, valid room should show video call button", () => {
            const bwiClientMock = jest.spyOn(BwiClient, "isBwiClient").mockReturnValue(false);
            const { result } = renderHook(() => useRoomCall(room));
            bwiClientMock.mockRestore();
            expect(result.current.showVideoCallButton).toBeTruthy();
        });

        it("bwi client, no config set so fallback, should not show video button", () => {
            const bwiClientMock = jest.spyOn(BwiClient, "isBwiClient").mockReturnValue(true);
            const { result } = renderHook(() => useRoomCall(room));
            bwiClientMock.mockRestore();
            expect(result.current.showVideoCallButton).toBeFalsy();
            expect(result.current.showVoiceCallButton).toBeFalsy();
        });

        it("bwi client, own config set so no fallback, should show video button", () => {
            const bwiClientMock = jest.spyOn(BwiClient, "isBwiClient").mockReturnValue(true);
            SdkConfig.put({
                element_call: {
                    url: "https://mockety.mock.io",
                    use_exclusively: true,
                    participant_limit: 8,
                    brand: "Mock Call Call",
                },
            });
            const { result } = renderHook(() => useRoomCall(room));
            bwiClientMock.mockRestore();
            expect(result.current.showVideoCallButton).toBeTruthy();
            expect(result.current.showVoiceCallButton).toBeFalsy();
            SdkConfig.reset();
        });
    });
});
