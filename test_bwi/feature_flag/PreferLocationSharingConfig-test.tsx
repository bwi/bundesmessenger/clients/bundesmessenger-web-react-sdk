// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, render } from "@testing-library/react";
import { mocked } from "jest-mock";
import maplibregl from "maplibre-gl";
import { ClientEvent, RoomMember } from "matrix-js-sdk/src/matrix";
import React from "react";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import SdkConfig from "../../src/SdkConfig";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import LocationPicker from "../../src/components/views/location/LocationPicker";
import Map from "../../src/components/views/location/Map";
import { LocationShareType } from "../../src/components/views/location/shareLocation";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import { findMapStyleUrl } from "../../src/utils/location";
import { getBaseClient, getClientUserMocks, locationWellKnown } from "../mock/MockClient";
import { createRoom } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { mockClientStore } from "../test-utils/client";

describe("BwiFeature.preferLocationSharingConfig", () => {
    const client = getBaseClient({
        ...getClientUserMocks(),
        ...mockClientStore({}),
        getEventMapper: jest.fn(),
        getClientWellKnown: jest.fn().mockReturnValue(locationWellKnown),
    });

    SdkConfig.put({
        map_style_url: "mock_from_config",
        brand: "MockBrand",
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    describe("findMapStyleUrl", () => {
        it("feature flag off, prefer well known config", () => {
            const result = findMapStyleUrl(client);
            expect(result).toBe("mock_from_wellknown");
        });

        it("feature flag on, prefer well known config", () => {
            const featureFlag = mockFeatureFlag(BwiFeature.PreferLocationSharingConfig, true);
            const result = findMapStyleUrl(client);
            expect(result).toBe("mock_from_config");
            featureFlag.mockRestore();
        });
    });

    describe("LocationPicker", () => {
        function renderComponent() {
            render(
                <MatrixClientContext.Provider value={client}>
                    <LocationPicker
                        sender={new RoomMember(createRoom(client, {}).roomId, "@me:domain")}
                        shareType={LocationShareType.Own}
                        onChoose={jest.fn()}
                        onFinished={jest.fn()}
                    />
                </MatrixClientContext.Provider>,
            );
        }

        it("feature flag off, prefer well known config in updateStyleUrl", async () => {
            const client = MatrixClientPeg.safeGet();
            const mockMap = new maplibregl.Map(null!);
            const mockedMap = mocked(mockMap);

            renderComponent();

            await act(async () => {
                client.emit(ClientEvent.ClientWellKnown, locationWellKnown);
            });

            expect(mockedMap.setStyle).toHaveBeenLastCalledWith("mock_from_wellknown");
        });

        it("feature flag on, prefer config.json config in updateStyleUrl", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.PreferLocationSharingConfig, true);
            const client = MatrixClientPeg.safeGet();
            const mockMap = new maplibregl.Map(null!);
            const mockedMap = mocked(mockMap);

            renderComponent();

            await act(async () => {
                client.emit(ClientEvent.ClientWellKnown, locationWellKnown);
            });

            expect(mockedMap.setStyle).toHaveBeenLastCalledWith("mock_from_config");
            featureFlag.mockRestore();
        });
    });

    describe("Map", () => {
        async function renderComponent() {
            await act(async () => {
                render(
                    <MatrixClientContext.Provider value={client}>
                        <Map id="MockMap" />
                    </MatrixClientContext.Provider>,
                );
            });
        }
        it("feature flag off, prefer well known config in updateStyleUrl", async () => {
            const client = MatrixClientPeg.safeGet();
            const mockMap = new maplibregl.Map(null!);
            const mockedMap = mocked(mockMap);

            await renderComponent();

            await act(async () => {
                client.emit(ClientEvent.ClientWellKnown, locationWellKnown);
            });

            expect(mockedMap.setStyle).toHaveBeenLastCalledWith("mock_from_wellknown");
        });

        it("feature flag on, prefer config.json config in updateStyleUrl", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.PreferLocationSharingConfig, true);
            const client = MatrixClientPeg.safeGet();
            const mockMap = new maplibregl.Map(null!);
            const mockedMap = mocked(mockMap);

            await renderComponent();

            await act(async () => {
                client.emit(ClientEvent.ClientWellKnown, locationWellKnown);
            });

            expect(mockedMap.setStyle).toHaveBeenLastCalledWith("mock_from_config");
            featureFlag.mockRestore();
        });
    });
});
