// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import fetchMockJest from "fetch-mock-jest";
import { act, cleanup, render, screen } from "@testing-library/react";
import LoginComponent from "../../src/components/structures/auth/Login";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import SdkConfig from "../../src/SdkConfig";
import { purgeWellKnownCache } from "../../src/bwi/functions";

describe("MandatoryDataPrivacyStatement", () => {
    beforeAll(() => {
        SdkConfig.put({
            brand: "My Messenger",
        });
        fetchMockJest.get("https://hs.de/_matrix/client/versions", {
            unstable_features: {},
            versions: ["v1.1"],
        });

        fetchMockJest.get("https://is.de/_matrix/identity/v2", {});

        fetchMockJest.get("https://hs.de/_matrix/client/v3/login", {
            flows: [
                {
                    type: "m.login.password",
                },
            ],
        });
    });

    describe("<Login />", () => {
        beforeEach(async () => {
            cleanup();
            mockFeatureFlag(BwiFeature.MandatoryDataPrivacyStatement, true);
            purgeWellKnownCache();
        });

        async function renderComponent() {
            await act(async () => {
                render(
                    <LoginComponent
                        onLoggedIn={() => {}}
                        onRegisterClick={() => {}}
                        onServerConfigChange={() => {}}
                        serverConfig={{
                            hsUrl: "https://hs.de",
                            hsName: "Home server",
                            hsNameIsDifferent: false,
                            isDefault: true,
                            isNameResolvable: false,
                            isUrl: "https://is.de",
                            warning: "",
                        }}
                    />,
                );
            });
        }

        it("should render password login if wellknown is valid", async () => {
            fetchMockJest.getOnce("https://hs.de/.well-known/matrix/client", {
                "de.bwi": {
                    data_privacy_url: "https://privacy.de",
                },
            });

            await renderComponent();

            expect(screen.queryByPlaceholderText("Username")).toBeInTheDocument();
        });

        it("should disable login and show error if wellknown is invalid", async () => {
            fetchMockJest.getOnce("https://hs.de/.well-known/matrix/client", {
                invalid: {},
            });

            await renderComponent();

            expect(
                screen.queryByText("You cannot login to My Messenger because the privacy policy is missing."),
            ).toBeInTheDocument();
            expect(screen.queryByPlaceholderText("Username")).not.toBeInTheDocument();
        });
    });
});
