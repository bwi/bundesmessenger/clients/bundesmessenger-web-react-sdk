// Copyright 2023 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import SdkConfig from "../../src/SdkConfig";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { showAppDownloadDialogPrompt } from "../../src/components/views/dialogs/AppDownloadDialog";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

describe("SkipDownloadForManagedDevices", () => {
    beforeEach(() => {
        SdkConfig.put({
            mobile_builds: {
                ios: "https://appstore.de",
                android: null,
                fdroid: null,
            },
        });
    });

    it("should disable app download if feature flag is enabled", () => {
        mockFeatureFlag(BwiFeature.SkipDownloadForManagedDevices, true);

        const appDownloadEnabled = showAppDownloadDialogPrompt();

        expect(appDownloadEnabled).toBe(false);
    });
    it("should allow app download if feature is disabled", () => {
        mockFeatureFlag(BwiFeature.SkipDownloadForManagedDevices, false);

        const appDownloadEnabled = showAppDownloadDialogPrompt();

        expect(appDownloadEnabled).toBe(true);
    });
    it("should allow app download if feature is null", () => {
        mockFeatureFlag(BwiFeature.SkipDownloadForManagedDevices, null);

        const appDownloadEnabled = showAppDownloadDialogPrompt();

        expect(appDownloadEnabled).toBe(true);
    });
});
