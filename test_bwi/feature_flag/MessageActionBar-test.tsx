// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { MatrixEvent, Room, EventType, MsgType } from "matrix-js-sdk/src/matrix";
import ResizeNotifier from "../../src/utils/ResizeNotifier";
import RoomContext, { TimelineRenderingType } from "../../src/contexts/RoomContext";
import { IRoomState } from "../../src/components/structures/RoomView";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import React from "react";
import MessagePanel from "../../src/components/structures/MessagePanel";
import SettingsStore from "../../src/settings/SettingsStore";
import DMRoomMap from "../../src/utils/DMRoomMap";
import {
    getMockClientWithEventEmitter,
    mkEvent,
    mockClientMethodsEvents,
    mockClientMethodsUser,
    mockPlatformPeg,
    unmockPlatformPeg,
} from "../../test/test-utils";
import { RoomPermalinkCreator } from "../../src/utils/permalinks/Permalinks";
import EventEmitter from "events";
import { act, cleanup, fireEvent, render, screen } from "@testing-library/react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import EventTile from "../../src/components/views/rooms/EventTile";

describe("MessageActionBar", () => {
    const roomId = "!roomId:server_name";
    const userId = "@alice:server.org";

    const messageEvent = new MatrixEvent({
        type: EventType.RoomMessage,
        sender: userId,
        room_id: roomId,
        content: {
            msgtype: MsgType.Text,
            body: "Hello",
        },
        event_id: "$alices_message",
    });
    const redactedEvent = new MatrixEvent({
        type: EventType.RoomMessage,
        sender: userId,
        room_id: roomId,
        content: {
            msgtype: MsgType.Text,
            body: "Hello Redacted Message",
        },
        event_id: "$alice_redacted_message",
        origin_server_ts: Date.now(),
    });

    const client = getMockClientWithEventEmitter({
        ...mockClientMethodsUser(userId),
        ...mockClientMethodsEvents(),
        getRoom: jest.fn(),
        getAccountData: jest.fn(),
        isUserIgnored: jest.fn().mockReturnValue(false),
        isRoomEncrypted: jest.fn().mockReturnValue(false),
        getClientWellKnown: jest.fn().mockReturnValue({}),
        supportsThreads: () => true,
    });

    const room = new Room(roomId, client, userId);
    redactedEvent.makeRedacted(redactedEvent, room);
    jest.spyOn(room, "getPendingEvents").mockReturnValue([]);
    client.getRoom.mockReturnValue(room);

    const defaultProps = {
        resizeNotifier: new EventEmitter() as unknown as ResizeNotifier,
        getTile: jest.fn(),
        getReplyChain: jest.fn(),
        toggleThreadExpanded: jest.fn(),
        mxEvent: messageEvent,
        permalinkCreator: new RoomPermalinkCreator(room),
        callEventGroupers: new Map(),
        room,
        className: "cls",
        events: [],
    };
    const defaultRoomContext = {
        ...RoomContext,
        room,
        roomId: room.roomId,
        timelineRenderingType: TimelineRenderingType.Room,
        canReact: true,
        canSendMessages: true,
        showReadReceipts: true,
        showRedactions: true,
        showJoinLeaves: false,
        showAvatarChanges: false,
        showDisplaynameChanges: true,
        showHiddenEvents: false,
    } as unknown as IRoomState;

    const renderMessagePanel = (props = {}, roomContext: Partial<IRoomState> = {}) =>
        render(
            <MatrixClientContext.Provider value={client}>
                <RoomContext.Provider value={{ ...defaultRoomContext, ...roomContext }}>
                    <MessagePanel {...defaultProps} {...props} />
                </RoomContext.Provider>
            </MatrixClientContext.Provider>,
        );

    beforeEach(function () {
        jest.clearAllMocks();
        DMRoomMap.makeShared(client);
    });

    afterAll(() => {
        jest.resetAllMocks();
    });

    function setupFeatures(features: Record<string, boolean>) {
        jest.spyOn(SettingsStore, "getValue").mockImplementation((arg) => {
            if (arg in features) return features[arg];
        });
    }

    function openMessageActionBar() {
        const { container } = renderMessagePanel({ events: [redactedEvent] });
        const redactedMessage = container.querySelector(".mx_EventTile")!;
        act(() => {
            fireEvent.mouseOver(redactedMessage);
        });
        return screen.queryByRole("toolbar");
    }

    describe("Event Tile: Redacted Message", () => {
        afterEach(cleanup);

        it("should show MessageActionBar if any feature is disabled", () => {
            setupFeatures({
                [BwiFeature.HideRedactedMessageSharing]: false,
                [BwiFeature.HideDebug]: true,
                [BwiFeature.HideOSMLink]: true,
            });

            const actionBar = openMessageActionBar();
            expect(actionBar).toBeTruthy();
        });

        it("should hide MessageActionBar if all features are enabled and message is redacted", () => {
            setupFeatures({
                [BwiFeature.HideRedactedMessageSharing]: true,
                [BwiFeature.HideDebug]: true,
                [BwiFeature.HideOSMLink]: true,
            });

            const actionBar = openMessageActionBar();
            expect(actionBar).toBeNull();
        });
    });

    describe("Message Context Menu", () => {
        afterEach(cleanup);

        it("should show share button for redacted message", () => {
            setupFeatures({
                [BwiFeature.HideRedactedMessageSharing]: false,
            });

            const actionBar = openMessageActionBar();
            expect(actionBar).toBeTruthy();

            act(() => {
                fireEvent.contextMenu(screen.getByLabelText("Options"));
            });

            expect(screen.getByText("Share message")).toBeTruthy();
        });

        it("should hide share button for redacted message", () => {
            setupFeatures({
                [BwiFeature.HideRedactedMessageSharing]: true,
            });

            const actionBar = openMessageActionBar();
            expect(actionBar).toBeTruthy();

            act(() => {
                fireEvent.contextMenu(screen.getByLabelText("Options"));
            });

            expect(screen.queryByText("Share message")).toBeNull();
        });
    });

    describe("Hide for non message events", () => {
        const nameChangeEvent = mkEvent({
            type: EventType.RoomName,
            user: userId,
            content: { name: "new room" },
            prev_content: { name: "old room" },
            event: true,
        });

        const messageEvent = mkEvent({
            type: EventType.RoomMessage,
            user: userId,
            content: { body: "my message" },
            event: true,
        });

        beforeEach(() => {
            mockPlatformPeg();
        });

        afterEach(() => {
            unmockPlatformPeg();
            cleanup();
        });

        it("should hide the action bar if event is not a message", () => {
            render(<EventTile mxEvent={nameChangeEvent} />);

            const eventTile = screen.getByText(/.*new room.*/);
            act(() => {
                fireEvent.mouseEnter(eventTile);
            });

            expect(screen.queryByRole("toolbar")).not.toBeInTheDocument();
        });

        it("should hide the context menu if event is not a message", async () => {
            render(<EventTile mxEvent={nameChangeEvent} />);

            const eventTile = screen.getByText(/.*new room.*/);
            act(() => {
                fireEvent.contextMenu(eventTile);
            });

            expect(screen.queryByRole("menu")).not.toBeInTheDocument();
        });

        it("should show the action bar if event is a message", () => {
            render(<EventTile mxEvent={messageEvent} />);

            const eventTile = screen.getByText("my message");
            act(() => {
                fireEvent.mouseEnter(eventTile);
            });

            expect(screen.queryByRole("toolbar")).toBeInTheDocument();
        });

        it("should show the context menu if event is a message", () => {
            render(<EventTile mxEvent={messageEvent} />);

            const eventTile = screen.getByText("my message");
            act(() => {
                fireEvent.contextMenu(eventTile);
            });

            expect(screen.queryByRole("menu")).toBeInTheDocument();
        });
    });
});
