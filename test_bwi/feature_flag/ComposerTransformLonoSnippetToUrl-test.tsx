// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";
import { IRoomState } from "../../src/components/structures/RoomView";
import SendMessageComposer from "../../src/components/views/rooms/SendMessageComposer";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import RoomContext, { TimelineRenderingType } from "../../src/contexts/RoomContext";
import { Layout } from "../../src/settings/enums/Layout";
import { RoomPermalinkCreator } from "../../src/utils/permalinks/Permalinks";
import { createTestClient, mkStubRoom } from "../../test/test-utils";

import { fireEvent, render, screen, act } from "@testing-library/react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

describe("ComposerTransformLonoSnippetToUrl", () => {
    const defaultRoomContext = {
        roomLoading: true,
        peekLoading: false,
        shouldPeek: true,
        membersLoaded: false,
        numUnreadMessages: 0,
        searching: false,
        guestsCanJoin: false,
        canPeek: false,
        showApps: false,
        isPeeking: false,
        showRightPanel: true,
        joining: false,
        atEndOfLiveTimeline: true,
        atEndOfLiveTimelineInit: false,
        showTopUnreadMessagesBar: false,
        statusBarVisible: false,
        canReact: false,
        canSendMessages: false,
        layout: Layout.Group,
        lowBandwidth: false,
        alwaysShowTimestamps: false,
        showTwelveHourTimestamps: false,
        readMarkerInViewThresholdMs: 3000,
        readMarkerOutOfViewThresholdMs: 30000,
        showHiddenEventsInTimeline: false,
        showReadReceipts: true,
        showRedactions: true,
        showJoinLeaves: true,
        showAvatarChanges: true,
        showDisplaynameChanges: true,
        matrixClientIsReady: false,
        timelineRenderingType: TimelineRenderingType.Room,
        liveTimeline: undefined,
    };

    const mockClient = createTestClient();
    jest.spyOn(MatrixClientPeg, "get").mockReturnValue(mockClient);
    const mockRoom = mkStubRoom("myfakeroom", "myfakeroom", mockClient) as any;

    beforeEach(() => {
        localStorage.clear();
    });

    const addTextToComposer = (text: string) =>
        act(() => {
            fireEvent.paste(screen.getByRole("textbox"), {
                clipboardData: {
                    types: [],
                    files: [],
                    getData: (type: string) => (type === "text/plain" ? text : undefined),
                },
            });
        });

    const defaultProps = {
        room: mockRoom,
        toggleStickerPickerOpen: jest.fn(),
        permalinkCreator: new RoomPermalinkCreator(mockRoom),
    };
    const renderComponent = (props = {}, roomContext = defaultRoomContext, client = mockClient) => {
        render(
            <MatrixClientContext.Provider value={client}>
                <RoomContext.Provider value={roomContext as unknown as IRoomState}>
                    <SendMessageComposer {...defaultProps} {...props} />
                </RoomContext.Provider>
            </MatrixClientContext.Provider>,
        );
    };

    const lonoSnippet = `test abc <NDL><REPLICA r1:r2><VIEW OFv1:v2-ONv3:v4><NOTE OFn1:n2-ONn3:n4></NDL>`;

    it("should paste snippet text by default", () => {
        renderComponent();

        addTextToComposer(lonoSnippet);

        expect(screen.getByText(lonoSnippet)).toBeInTheDocument();
    });

    it("should transform snippet to url with feature flag enabled", () => {
        const featureFlag = mockFeatureFlag(BwiFeature.ComposerTransformLonoSnippetToUrl, true);

        renderComponent();

        addTextToComposer(lonoSnippet);

        expect(screen.getByText("notes:///r1r2/v1v2v3v4/n1n2n3n4")).toBeInTheDocument();

        featureFlag.mockRestore();
    });
});
