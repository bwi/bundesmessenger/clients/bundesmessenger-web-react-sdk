// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { showUserOnboardingPage } from "../../src/components/views/user-onboarding/UserOnboardingPage";
import { UseCase } from "../../src/settings/enums/UseCase";

describe("BwiFeature.DisplayOnBoardingViaAccountDataFlag", () => {
    it('Should return content of "FTUE.userOnboardingButton" when feature flag is enabled', async () => {
        const flags = new Map();
        flags.set(BwiFeature.DisplayOnBoardingViaAccountDataFlag, true);
        flags.set("FTUE.userOnboardingButton", true);
        mockFeatureFlag(flags);

        expect(showUserOnboardingPage(UseCase.Skip)).toBe(true);
    });

    it("Should return value calculated by element when feature flag is disabled", async () => {
        const flags = new Map();
        flags.set(BwiFeature.DisplayOnBoardingViaAccountDataFlag, false);
        flags.set("FTUE.userOnboardingButton", false);
        mockFeatureFlag(flags);

        expect(showUserOnboardingPage(UseCase.Skip)).toBe(true);
    });

    afterEach(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });
});
