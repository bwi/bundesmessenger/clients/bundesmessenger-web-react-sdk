// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, cleanup, fireEvent, render, screen } from "@testing-library/react";
import { User } from "matrix-js-sdk/src/matrix";
import React from "react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import ImageView from "../../src/components/views/elements/ImageView";
import UserInfo from "../../src/components/views/right_panel/UserInfo";
import Modal from "../../src/Modal";
import { RightPanelPhases } from "../../src/stores/right-panel/RightPanelStorePhases";
import { unmockClientPeg, wrapInMatrixClientContext } from "../../test/test-utils";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import { userToRoomMember } from "../test-utils/user";
import { UserVerificationStatus } from "matrix-js-sdk/src/crypto-api";
import { mockDevice, mockDeviceMap } from "../mock/MockDevice";

jest.mock("../../src/components/views/settings/JoinRuleSettings", () => () => <div>Mock</div>);

const USER_ID = "@user:server.org";

describe("BwiFeature.disableProfileAvatarDl", () => {
    const user = new User(USER_ID);
    user.avatarUrl = "some/img.png";
    user.displayName = "User";
    const member = userToRoomMember(user);

    beforeAll(() => {
        getBaseClient({
            ...getClientUserMocks(user),
            getCrypto() {
                return {
                    async getUserDeviceInfo() {
                        return mockDeviceMap(USER_ID, mockDevice(USER_ID));
                    },
                    async getUserVerificationStatus() {
                        return new UserVerificationStatus(false, false, false);
                    },
                    async getDeviceVerificationStatus() {
                        return new UserVerificationStatus(false, false, false);
                    },
                    async userHasCrossSigningKeys() {
                        return true;
                    },
                };
            },
        });
    });

    afterAll(() => {
        unmockClientPeg();
    });

    // Tested speratly so we dont have to create a real dialog to check if the download button is there
    describe("ImageView", () => {
        afterEach(() => {
            cleanup();
        });

        it("should hide the download button", () => {
            render(<ImageView allowDownload={false} />);

            expect(screen.queryByLabelText("Download")).not.toBeInTheDocument();
        });

        it("should render the download button", () => {
            render(<ImageView allowDownload={true} />);

            expect(screen.queryByLabelText("Download")).toBeInTheDocument();
        });
    });

    describe("pass disable download to image view dialog", () => {
        const WrappedUserInfo = wrapInMatrixClientContext(UserInfo);

        const modalSpy = jest.spyOn(Modal, "createDialog");

        beforeEach(() => modalSpy.mockReset());
        afterEach(() => {
            modalSpy.mockRestore();
            cleanup();
        });

        it("should disable download", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.DisableProfileAvatarDl, true);

            render(<WrappedUserInfo user={member} phase={RightPanelPhases.RoomMemberInfo} onClose={() => {}} />);

            await act(async () => {
                fireEvent.click(screen.getByTestId("avatar-img"));
            });

            const call = modalSpy.mock.calls[0];
            expect(modalSpy.mock.calls).toHaveLength(1);
            expect(call).toBeTruthy();
            expect(call[1]).toEqual(expect.objectContaining({ allowDownload: false }));

            featureFlag.mockRestore();
        });
    });
});
