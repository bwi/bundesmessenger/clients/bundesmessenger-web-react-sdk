// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { act, render, screen } from "@testing-library/react";
import React from "react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import DevicesPanelEntry from "../../src/components/views/settings/DevicesPanelEntry";
import DeviceDetails from "../../src/components/views/settings/devices/DeviceDetails";
import { DeviceMetaData } from "../../src/components/views/settings/devices/DeviceMetaData";
import { ExtendedDevice } from "../../src/components/views/settings/devices/types";
import { DeviceType } from "../../src/utils/device/parseUserAgent";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

const mockDevice: ExtendedDevice = {
    device_id: "testdeviceid",
    display_name: "web_test",
    last_seen_ip: "127.0.0.1",
    last_seen_ts: 1633065589,
    appName: "Element Web",
    client: "Firefox 100",
    deviceModel: "Iphone X",
    deviceOperatingSystem: "Windows 95",
    isVerified: true,
    deviceType: DeviceType.Unknown,
};

jest.mock("../../src/MatrixClientPeg", () => ({
    MatrixClientPeg: {
        get: () => ({
            getDeviceId: () => mockDevice.last_seen_ip,
        }),
    },
}));

jest.mock("../../src/stores/room-list/RoomListStore", () => ({
    instance: {
        on: () => true,
    },
}));

jest.mock("../../src/stores/widgets/WidgetLayoutStore", () => ({
    instance: {
        updateAllRooms: () => true,
    },
}));

jest.mock("../../src/stores/spaces/SpaceStore", () => ({
    spacesEnabled: false,
}));

jest.mock("../../src/components/views/dialogs/security/SetupEncryptionDialog", () => () => {
    return () => <>ffs</>;
});

jest.mock("../../src/components/views/dialogs/VerificationRequestDialog", () => () => {
    return () => <>ffs</>;
});

describe("BwiFeature.hideIpAdresses", () => {
    describe("DevicesPanelEntry", () => {
        async function renderComponent() {
            await act(async () => {
                render(
                    <DevicesPanelEntry
                        device={mockDevice}
                        isOwnDevice={true}
                        verified={true}
                        canBeVerified={false}
                        selected={false}
                        onDeviceChange={jest.fn()}
                        onDeviceToggled={jest.fn()}
                    />,
                );
            });
        }

        it("should render ip adress by default", async () => {
            await renderComponent();

            expect(screen.queryByText(mockDevice.last_seen_ip!)).toBeInTheDocument();
        });

        it("should not render ip adress if flag is enabled", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.HideIpAdresses, true);

            await renderComponent();

            expect(screen.queryByText(mockDevice.last_seen_ip!)).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });
    });

    describe("DeviceMetaData", () => {
        it("should render ip adress by default", async () => {
            render(<DeviceMetaData device={{ ...mockDevice }} />);

            expect(screen.queryByText(mockDevice.last_seen_ip!)).toBeInTheDocument();
        });

        it("should not render ip adress if flag is enabled", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.HideIpAdresses, true);

            render(<DeviceMetaData device={{ ...mockDevice }} />);
            expect(screen.queryByText(mockDevice.last_seen_ip!)).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });
    });

    describe("DeviceDetails", () => {
        function renderComponent() {
            render(
                <DeviceDetails
                    device={{ ...mockDevice }}
                    isSigningOut={false}
                    onSignOutDevice={jest.fn()}
                    saveDeviceName={jest.fn()}
                />,
            );
        }

        it("should render ip adress by default", async () => {
            renderComponent();

            expect(screen.queryByText("IP address")).toBeInTheDocument();
            expect(screen.queryByText(mockDevice.last_seen_ip!)).toBeInTheDocument();
        });

        it("should not render ip adress if flag is enabled", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.HideIpAdresses, true);

            renderComponent();

            expect(screen.queryByText("IP address")).not.toBeInTheDocument();
            expect(screen.queryByText(mockDevice.last_seen_ip!)).not.toBeInTheDocument();

            featureFlag.mockRestore();
        });
    });
});
