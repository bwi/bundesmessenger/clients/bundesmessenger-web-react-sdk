// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { render, screen } from "@testing-library/react";
import { PendingEventOrdering, Room } from "matrix-js-sdk/src/matrix";
import React from "react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import RoomHeader from "../../src/components/views/rooms/LegacyRoomHeader";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import DMRoomMap from "../../src/utils/DMRoomMap";
import { E2EStatus } from "../../src/utils/ShieldUtils";
import { getBaseClient, getClientUserMocks } from "../mock/MockClient";
import { mockRoomWithEvents } from "../mock/MockRoom";
import { mockFeatureFlag } from "../mock/MockSettingsStore";

describe("BwiFeature.HideSearchButton", () => {
    let room: Room;

    const client = getBaseClient({
        ...getClientUserMocks(),
        getRoom: () => room,
        store: {
            getPendingEvents: () => Promise.resolve([]),
        },
        getEventMapper: () => ({}),
    });

    beforeAll(function () {
        room = mockRoomWithEvents({
            partials: {
                name: "NeedSomething",
                canInvite: () => false,
            },
            opts: {
                pendingEventOrdering: PendingEventOrdering.Detached,
            },
        });
        DMRoomMap.makeShared(client);
    });

    function renderComponent() {
        render(
            <MatrixClientContext.Provider value={client}>
                <RoomHeader
                    room={room}
                    onSearchClick={jest.fn().mockReturnValue(true)}
                    inRoom={true}
                    viewingCall={false}
                    onInviteClick={jest.fn}
                    onForgetClick={jest.fn}
                    onAppsClick={jest.fn}
                    e2eStatus={E2EStatus.Warning}
                    appsShown={false}
                    searchInfo={undefined}
                    activeCall={null}
                />
            </MatrixClientContext.Provider>,
        );
    }

    it("should render the search button by default", () => {
        const featureFlag = mockFeatureFlag(BwiFeature.HideSearchButton, false);

        renderComponent();

        expect(screen.queryByRole("button", { name: "Search" })).toBeInTheDocument();

        featureFlag.mockRestore();
    });

    it("should not render the search button if the feature is enabled", () => {
        const featureFlag = mockFeatureFlag(BwiFeature.HideSearchButton, true);

        renderComponent();

        expect(screen.queryByRole("button", { name: "Search" })).not.toBeInTheDocument();

        featureFlag.mockRestore();
    });
});
