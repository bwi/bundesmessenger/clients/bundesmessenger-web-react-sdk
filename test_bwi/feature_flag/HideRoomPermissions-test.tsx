// Copyright 2022 BWI GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React from "react";
import { BwiFeature } from "../../src/bwi/settings/BwiFeature";
import { mockFeatureFlag } from "../mock/MockSettingsStore";
import * as testUtils from "../../test/test-utils";
import { MockUser } from "../mock/MockUser";
import DMRoomMap from "../../src/utils/DMRoomMap";
import RolesRoomSettingsTab, {
    plEventsToShow,
} from "../../src/components/views/settings/tabs/room/RolesRoomSettingsTab";
import { Room } from "matrix-js-sdk/src/matrix";
import { generateRoomId } from "../mock/MockRoom";
import { render, screen } from "@testing-library/react";
import MatrixClientContext from "../../src/contexts/MatrixClientContext";
import { MatrixClientPeg } from "../../src/MatrixClientPeg";

describe("BwiFeature.hideRoomPermissions", () => {
    let room: Room;

    beforeAll(() => {
        const client = testUtils.stubClient();
        client.getUser = jest.fn().mockReturnValue(MockUser);
        client.getUserId = jest.fn().mockReturnValue(MockUser.userId);
        client.doesServerSupportUnstableFeature = jest.fn().mockReturnValue(false);
        client.isSynapseAdministrator = jest.fn().mockResolvedValue(false);
        DMRoomMap.makeShared(client);
        const newRoom = client.getRoom(generateRoomId());
        if (newRoom) room = newRoom;
    });

    afterAll(() => {
        jest.clearAllMocks();
        jest.resetModules();
    });

    describe("check if switch changes amount of selectors", () => {
        function renderComponent() {
            render(<RolesRoomSettingsTab room={room} />, {
                wrapper: ({ children }) => (
                    <MatrixClientContext.Provider value={MatrixClientPeg.safeGet()}>
                        {children}
                    </MatrixClientContext.Provider>
                ),
            });
        }

        it("feature flag off, all switches shown", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.HideRoomPermissions, false);
            renderComponent();
            const maxSelectors = Object.keys(plEventsToShow).length;
            const powerSelector = await screen.findAllByTestId("plEventsToShow");
            expect(maxSelectors).toEqual(powerSelector.length);
            featureFlag.mockRestore();
        });

        it("feature flag on, switches with hide tag or includedin are hidden", async () => {
            const featureFlag = mockFeatureFlag(BwiFeature.HideRoomPermissions, true);
            renderComponent();
            const maxSelectors = Object.keys(plEventsToShow).length;
            const hiddenSelectors = Object.keys(plEventsToShow).filter(
                (key) => plEventsToShow[key].bwiHide === true || plEventsToShow[key].bwiIncludedIn,
            ).length;
            const powerSelector = await screen.findAllByTestId("plEventsToShow");
            expect(maxSelectors).toBeGreaterThan(powerSelector.length);
            expect(hiddenSelectors).toBeDefined();
            expect(maxSelectors - hiddenSelectors).toBe(powerSelector.length);
            featureFlag.mockRestore();
        });
    });
});
