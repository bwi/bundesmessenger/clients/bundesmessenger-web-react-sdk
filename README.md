<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/logo.png?inline=false" alt="BundesMessenger Logo" width="128" height="128">
</div>
=======
[![npm](https://img.shields.io/npm/v/matrix-react-sdk)](https://www.npmjs.com/package/matrix-react-sdk)
![Tests](https://github.com/matrix-org/matrix-react-sdk/actions/workflows/tests.yml/badge.svg)
[![Playwright](https://img.shields.io/badge/Playwright-end_to_end_tests-blue)](https://e2e-develop--matrix-react-sdk.netlify.app/)
![Static Analysis](https://github.com/matrix-org/matrix-react-sdk/actions/workflows/static_analysis.yaml/badge.svg)
[![Localazy](https://img.shields.io/endpoint?url=https%3A%2F%2Fconnect.localazy.com%2Fstatus%2Felement-web%2Fdata%3Fcontent%3Dall%26title%3Dlocalazy%26logo%3Dtrue)](https://localazy.com/p/element-web)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=matrix-react-sdk&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=matrix-react-sdk)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=matrix-react-sdk&metric=coverage)](https://sonarcloud.io/summary/new_code?id=matrix-react-sdk)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=matrix-react-sdk&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=matrix-react-sdk)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=matrix-react-sdk&metric=bugs)](https://sonarcloud.io/summary/new_code?id=matrix-react-sdk)
>>>>>>> cfd5801

<div align="center">
  <h2 align="center">BundesMessenger-Web React SDK</h2>
</div>

---

Wir freuen uns, dass Du Dich für den BundesMessenger interessierst.

## Grundsätzliches

BundesMessenger-Web React SDK ist ein unterstützendes Repository für BundesMessenger Web.
Im React SDK wird der Großteil aller Komponenten des Web Clients entwickelt.

## Repo

https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-react-sdk

## Fehler und Verbesserungsvorschläge

https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-react-sdk/-/issues

## Abhängigkeiten

[matrix-org/matrix-react-sdk](https://github.com/matrix-org/matrix-react-sdk)

[BundesMessenger Web - Config and Assets](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-config-and-assets)

[BundesMessenger Web - Matrix SDK](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web-matrix-sdk)

[BundesMessenger Web](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web)

## Nutzung

Das Matrix React SDK dient als Library für React Komponenten und wird nicht eigenständig ausgeliefert.
Weitere Informationen können auch dem [matrix-org/matrix-react-sdk](https://github.com/matrix-org/matrix-react-sdk) Github Repo entnommen werden.

## Weitere Infos

Allgemeine Infos zum Thema BundesMessenger und was dahinter steckt findest Du [hier](https://gitlab.opencode.de/bwi/bundesmessenger/info) und alles weitere zur Web App findest Du unter [BundesMessenger Web](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web).

### Copyright

-   [BWI GmbH](https://messenger.bwi.de/copyright)
-   [Element](https://element.io/copyright)
