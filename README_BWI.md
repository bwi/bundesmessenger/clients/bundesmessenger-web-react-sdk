<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/logo.png?inline=false" alt="BundesMessenger Logo" width="128" height="128">
</div>

<div align="center">
  <h2 align="center">BundesMessenger-Web React SDK</h2>
</div>

---

Wir freuen uns, dass Du Dich für den BundesMessenger interessierst.

BundesMessenger-Web React SDK ist das Matrix React SDK basierend auf [Matrix React SDK](https://github.com/matrix-org/matrix-react-sdk)
von [Element Software](https://element.io/).

Allgemeine Infos zum Thema BundesMessenger und was dahinter steckt findest Du [hier](https://gitlab.opencode.de/bwi/bundesmessenger/info) und alles weitere zur Web App findest Du unter [BundesMessenger Web](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-web).

### Copyright

-   [BWI GmbH](https://messenger.bwi.de/copyright)
-   [Element](https://element.io/copyright)
